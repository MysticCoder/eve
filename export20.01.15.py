import bpy
import struct
import os
import os.path
import math
import xml.dom.minidom
def getbonegroup(bg, obj, arm):
    groupname = obj.vertex_groups[bg.group].name
    #if arm.pose.bones.find(groupname) == -1:
        #print("alarm")
    #else:
        #print("ok")
    return(arm.pose.bones.find(groupname))
def getbind(bone):
    bone_matrix = bone.bone.matrix_local
    if bone.parent:
        bone_matrix = bone.bone.matrix_local.inverted() *   bone.bone.matrix_local
    return(bone_matrix)    
def getmatrix(bone):
    return(bone.matrix_channel)
    #return(bone.matrix)
    bone_matrix = bone.matrix
    if bone.parent:
        bone_matrix = bone.matrix_channel
        #bone_matrix = bone.parent.matrix_channel
        # * bone.matrix
    return(bone_matrix)
    #else:
    #    bone_matrix = bone.bone.matrix_local    
    return(getbind(bone) * bone_matrix)
    if bone.parent != None:
        return(getmatrix(bone.parent).inverted() * bone.matrix)
    else:
        return(bone.matrix)
    
def SaveBones(arm, f):
    f.write(struct.pack('i', len(arm.pose.bones)))
    for bone in arm.pose.bones:
        matrix = getmatrix(bone)
        
        for matrix_i in matrix:
            for matrix_ij in matrix_i:
                f.write(struct.pack('f', matrix_ij))
def SaveVertexs(obj, f):
    polycount = len(obj.data.polygons)
    f.write(struct.pack("i", polycount))
    
    for poly in obj.data.polygons:
        for vert in poly.vertices:
            f.write(struct.pack("f", obj.data.vertices[vert].co[0]))
            f.write(struct.pack("f", obj.data.vertices[vert].co[1]))
            f.write(struct.pack("f", obj.data.vertices[vert].co[2]))

    for poly in obj.data.polygons:
        for vert in poly.vertices:
            f.write(struct.pack("f", obj.data.vertices[vert].normal[0]))
            f.write(struct.pack("f", obj.data.vertices[vert].normal[1]))
            f.write(struct.pack("f", obj.data.vertices[vert].normal[2]))

    for poly in obj.data.polygons:
        f.write(struct.pack("b", poly.material_index))
    
    xInd = 0    
    for poly in obj.data.polygons:
        for vert in poly.vertices:
            f.write(struct.pack("f", obj.data.uv_layers[0].data[xInd].uv[0]))
            f.write(struct.pack("f", obj.data.uv_layers[0].data[xInd].uv[1]))
            xInd = xInd + 1

    for poly in obj.data.polygons:
        for vertind in poly.vertices:
            vert = obj.data.vertices[vertind]
            count = 0
            for bonegroup in vert.groups:
                if getbonegroup(bonegroup, obj, obj.parent) == -1:
                    continue
                else:
                    count = count + 1
            #f.write(struct.pack('i', len(vert.groups)))
            f.write(struct.pack('i', count))
            for bonegroup in vert.groups:
                
                if getbonegroup(bonegroup, obj, obj.parent) == -1:
                    continue
                f.write(struct.pack('i', getbonegroup(bonegroup, obj, obj.parent)))
                #f.write(struct.pack('i', bonegroup.group))
            AllWeight = 0    
            for bonegroup in vert.groups:
                if getbonegroup(bonegroup, obj, obj.parent) == -1:
                    continue
                AllWeight = AllWeight + bonegroup.weight
            
            for bonegroup in vert.groups:
                if getbonegroup(bonegroup, obj, obj.parent) == -1:
                    continue
                if AllWeight == 0:
                    f.write(struct.pack('f', 0))
                else:
                    f.write(struct.pack('f', bonegroup.weight/AllWeight))        

                
def SaveMaterials(obj, f):
    f.write(struct.pack('b', len(obj.data.materials)))
    for mat in obj.data.materials:
        
        
        f.write(struct.pack('b', len(mat.texture_slots.items())))
        for tex in mat.texture_slots:
            if not tex:
                continue
            if (tex.use_map_color_diffuse) and (tex.texture.image):
                f.write(struct.pack('b', 1))
                f.write(struct.pack('i', len(tex.texture.image.name)))
                f.write(bytes(tex.texture.image.name, encoding = 'ascii'))
                texpath = (os.path.dirname(f.name) + os.path.sep + tex.texture.image.name)
                bpy.context.scene.render.image_settings.file_format = 'TARGA'
                bpy.context.scene.render.image_settings.color_depth = '8'
                bpy.context.scene.render.image_settings.color_mode = 'RGBA'
                tex.texture.image.save_render(texpath)
            else:
                f.write(struct.pack('b', 0))
                        
def SaveMesh(obj, f):
    if obj.parent == None:
        return()
    print(obj.name)
    obj.parent.data.pose_position = 'REST'
    SaveMaterials(obj, f)
    SaveVertexs(obj, f)
    #Rest Pose
    SaveBones(obj.parent, f)


    obj.parent.data.pose_position = 'POSE'

    frame_start = bpy.data.scenes[0].frame_start
    frame_end   = bpy.data.scenes[0].frame_end
    frame_ind   = 0    
    markers = bpy.context.scene.timeline_markers
    animcount = len(markers)
    f.write(struct.pack('i', animcount))
    for i in range(0, animcount):
        f.write(struct.pack('i', len(markers[i].name)))
        f.write(bytes(markers[i].name, encoding = 'ascii'))

        if i<animcount-1:
            framecount = markers[i+1].frame-markers[i].frame
        else:
            framecount = frame_end-markers[i].frame
        f.write(struct.pack('i', framecount))
        for frame_ind in range(markers[i].frame, markers[i].frame + framecount):
            bpy.data.scenes[0].frame_set(frame_ind)
            print(frame_ind)
            SaveBones(obj.parent, f)
            #SaveVertexs(obj, f)
    
print("Start")    
f = open('/mnt/data/creative/Eve/data/model.txt','wb')    
for obj in bpy.data.scenes[0].objects:
    if obj.type!='MESH':
        continue
    if obj.select == False:
        continue
    SaveMesh(obj, f)
f.flush()
f.close()    
print('Done')

 #for uv_co in mesh.uv_textures.active.data[face.index].uv:
#                uv.append(uv_co[0])
#                uv.append(-uv_co[1])
#Текстурные координаты:

#bpy.context.object.active_material.texture_slots[0].texture_coords

#Пути к файлам текстур:

#bpy.data.images["имя текстуры"].filepath