unit _apoc_map;

{$mode delphi}

interface

uses
  Classes, SysUtils, _usual, gl,  glext, math, _timer;

type

  R_UnitPos = RVector3f;

  T_MapCell = record
    _type : Byte;
    Passable : boolean;
    LightPassPower : Single;
    Height : Single;
    Units : TObjectList;
  end;

  { T_Map }
  T_Map = class
    Timer : T_Timer;
    Width,
    Height : integer;
    Data : array of array of T_MapCell;
    Units : TObjectList;
    UnitIDCounter : integer;
    constructor Create(ATimer : T_Timer);
    function AddUnit(AUnit : TObject; ADoGenerateID : boolean = True) : TObject;
    procedure GenSimpleMap;
    procedure Draw;
    procedure Update;
    function FindUnitByID(AID : integer) : TObject;
    function LightPassPower(APos : R_UnitPos) : Single;
    function IsPlacePass(APos : R_UnitPos) : boolean; overload;
    function IsPlacePass(AX, AY : Single) : boolean; overload;
    function GetUnitsOnRadius(APos: R_UnitPos; ARadius: Single; AExceptUnits : Array of Pointer): TObjectList;
  end;

const
  MAP_NONE = 0;
  MAP_BLOCK = 1;
  MAP_GRASS = 2;
implementation

  uses
    _scene_apoc, _apoc_unit;
{ T_Map }

constructor T_Map.Create(ATimer: T_Timer);
begin
  Units := TObjectList.create(True);
  Timer := ATimer;
end;

function T_Map.AddUnit(AUnit: TObject; ADoGenerateID: boolean): TObject;
begin
  if T_Unit(AUnit).Logic = nil then
    writeln;
  T_Unit(AUnit).Map := Self;
  T_Unit(AUnit).Timer := Timer;
  T_Unit(AUnit).Init;
  if ADoGenerateID then
    begin
      T_Unit(AUnit).ID := UnitIDCounter;
      inc(UnitIDCounter);
    end;
  writeln('AddUnit ', UnitIDCounter);
  Result := T_Unit(Units[Units.Add(AUnit)]);
end;

procedure T_Map.GenSimpleMap;
var
  i, j : integer;
  xTree : T_unit;
begin
  Width := 30;
  Height := 20;

  SetLength(Data, Width, Height);
  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
      begin
        Data[i][j]._type := MAP_NONE;
        Data[i][j].Height := 0;
        Data[i][j].Passable := True;
        Data[i][j].Units := TObjectList.create(False);
      end;

  for i := 0 to trunc(Width*Height*0.02) do
    begin
      Data[Random(Width)][Random(Height)]._type := MAP_BLOCK;
      Data[Random(Width)][Random(Height)]._type := MAP_GRASS;
    end;

  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
      Case Data[i][j]._type of
        MAP_NONE:
          begin
            Data[i][j].LightPassPower := 0;
            Data[i][j].Passable := True;
            Data[i][j].Height := 0;
          end;
        MAP_BLOCK:
          begin
            Data[i][j].LightPassPower := 1;
            Data[i][j].Passable := False;
            Data[i][j].Height := 0;
          end;
        MAP_GRASS:
          begin
            Data[i][j].LightPassPower := 0.6;
            Data[i][j].Passable := True;
            Data[i][j].Height := 0;
          end;
      end;
  {for i := 0 to 10 do
    begin
      xTree := T_Unit.Create([T_PhysicUnit, T_LogicUnit, T_GraphUnit]);
      AddUnit(xTree);
      xTree.Physic.Radius:=0.5;
      xTree.Physic.Pos[0] := Random(Width);
      xTree.Physic.Pos[1] := Random(Height);
    end;}
end;

var
  xdt : single = 0.5;
procedure T_Map.Draw;
var
  i, j : integer;
begin
  //glColor3f(0, 1, 0);
  //glDisable(GL_BLEND);
  //glDisable(GL_DEPTH_TEST);
  //glEnable(GL_BLEND);
  //glBlendFunc(GL_DST_COLOR, GL_ZERO);
  glEnable(GL_DEPTH_TEST);
  glPolygonMode(GL_FRONT, GL_QUADS);
  glBegin(GL_QUADS);
  for i := 0 to Min(50000, Width - 2) do
    for j := 0 to Min(50000, Height - 2) do
      begin
        Case Data[i][j]._type of
          MAP_NONE:
            glColor3f(0.5, 0.5, 0.5);
          MAP_BLOCK:
            glColor3f(1, 0, 0);
          MAP_GRASS:
            glColor3f(0, 0.5, 0);
          else
            writeln;
        end;
{        if Data[i][j].Passable then
          glColor3f(0.5, 0.5, 0.5)
        else
          glColor3f(1, 0, 0);}
        glVertex3f(i, j, Data[i][j].Height);
        glVertex3f(i + 1, j, Data[i + 1][j].Height);
        glVertex3f(i + 1, j + 1, Data[i + 1][j + 1].Height);
        glVertex3f(i, j + 1, Data[i][j + 1].Height);
      end;
  glEnd;
  glDisable(GL_DEPTH_TEST);
  for i := 0 to Units.Count - 1 do
    T_Unit(Units[i]).Draw;

end;

procedure T_Map.Update;
var
  i : integer;
begin
  for i := Units.Count - 1 downto 0 do
    T_Unit(Units[i]).Update;
end;

function T_Map.FindUnitByID(AID: integer): TObject;
var
  i : integer;
  xUnit : T_Unit;
begin
  Result := nil;
  for i := 0 to Units.Count - 1 do
    begin
      xUnit := T_Unit(Units[i]);
      if xUnit.ID = AID then
        begin
          Result := xUnit;
          Exit;
        end;
    end;
end;

function T_Map.LightPassPower(APos: R_UnitPos): Single;
begin
  if (APos[0] < 0) or (APos[1] < 0) or (APos[0] > Width - 1) or (APos[1] > Height - 1) then
    begin
      Result := 1;
      Exit;
    end;
  Result := Data[Trunc(APos[0])][Trunc(APos[1])].LightPassPower;
end;

function T_Map.IsPlacePass(APos: R_UnitPos): boolean;
begin
  Result := False;
  if (APos[0] < 0) or (APos[1] < 0) then
    Exit;
  if (APos[0] > Width - 1) or (APos[1] > Height - 1) then
    Exit;
  if Data[Trunc(APos[0])][Trunc(APos[1])].Passable then
    Result := True
  else
    Result := False;
end;

function T_Map.IsPlacePass(AX, AY: Single): boolean;
var
  xPos : R_UnitPos;
begin
  xPos[0] := AX;
  xPos[1] := AY;
  Result := IsPlacePass(xPos);
end;


function T_Map.GetUnitsOnRadius(APos: R_UnitPos; ARadius: Single;
  AExceptUnits: array of Pointer): TObjectList;
var
  i, j : integer;
  k, m : integer;
  xUnit : T_Unit;
  xUnits : TObjectList;
  xFlag : boolean;
begin
  Result := TObjectList.create(False);
  for i := Trunc(APos[0] - ARadius) to Trunc(APos[0] + ARadius) do
    for j := Trunc(APos[1] - ARadius) to Trunc(APos[1] + ARadius) do
      if (i >= 0) and (j >= 0) and (i <= Width - 1) and (j <= Height - 1) then
      begin
        xUnits := Data[i][j].Units;
        for k := 0 to xUnits.Count - 1 do
          begin
            xUnit := T_Unit(Data[i][j].Units[k]);
            xFlag := False;
            for m := Low(AExceptUnits) to High(AExceptUnits) do
              if xUnit = AExceptUnits[m] then
                begin
                  xFlag := True;
                  break;
                end;

            if not xFlag then
              if GetDistance(xUnit.Physic.Pos, APos) <= ARadius + xUnit.Physic.Radius then
                Result.Add(xUnit);
          end;
      end;
end;



end.

