unit _deep;

{$mode delphi}

interface

uses
  Classes, SysUtils, _window, _usual, gl, glu;

type
  R_UnitPos = RVector2f;

  { T_Unit }

  T_Unit = class
    Pos : R_UnitPos;
    Speed : Single;
    procedure Draw;
  end;

  T_MapCell = record
    isPassable : boolean;
    SpriteType : byte;
  end;

  { T_Map }

  T_Map = class
    Width,
    Height : integer;
    CellWidth,
    CellHeight : integer;
    Cells : array of array of T_MapCell;
    constructor Create(AWidth, AHeight : integer);
    procedure Draw;
  end;

  { T_Scene_Deep }

  T_Scene_Deep = class(T_Scene)
    Units : TList; // of T_Unit;
    Map : T_Map;
    procedure InitScene; override;
    procedure DrawScene; override;
  end;


implementation

{ T_Unit }

procedure T_Unit.Draw;
var
  q : PGLUquadric;
begin
  glTranslatef(Pos[0], Pos[1], 0);

  q := gluNewQuadric;
  gluSphere(q, 0.3, 5, 5);
  gluDeleteQuadric(q);

  glTranslatef(-Pos[0], -Pos[1], 0);

end;

{ T_Scene_Deep }

procedure T_Scene_Deep.InitScene;
begin
  inherited InitScene;
  Map := T_Map.Create(40, 30);
  Units := TList.Create;
  with T_Unit(Units[Units.Add(T_Unit.Create)]) do
    begin
      Pos[0] := 20;
      Pos[1] := 25;
    end;
end;

procedure T_Scene_Deep.DrawScene;
var
  i : integer;
begin
  inherited DrawScene;
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  glOrtho(0, Map.Width, 0, Map.Height, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  Map.Draw;
  for i := 0 to Units.Count - 1 do
    T_Unit(Units[i]).Draw;
end;

{ T_Map }

constructor T_Map.Create(AWidth, AHeight: integer);
var
  i : integer;
begin
  Width := AWidth;
  Height := AHeight;
  CellWidth := 1;
  CellHeight := 1;
  SetLength(Cells, Width, Height);
  for i := 0 to Width - 1 do
    FillChar(Cells[i][0], SizeOf(Cells[i][0]) * Length(Cells[i]), 0);
end;

procedure T_Map.Draw;
var
  i, j : integer;
begin
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glBegin(GL_QUADS);
  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
      begin
        Case Cells[i][j].SpriteType of
          0 :
            glColor3f(0, 1, 0);
          1 :
            glColor3f(1, 0, 0);
        end;
        glVertex2f(i - CellWidth/2, j - CellHeight/2);
        glVertex2f(i + CellWidth/2, j - CellHeight/2);
        glVertex2f(i + CellWidth/2, j + CellHeight/2);
        glVertex2f(i - CellWidth/2, j + CellHeight/2);
      end;
  glEnd;
  glPopAttrib;
end;

end.

