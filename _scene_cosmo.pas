unit _scene_cosmo;

{$mode delphi}

interface

uses
  inifiles, math, Classes, SysUtils, _window, _usual, gl, glu, _textures, _model;

type

  { T_CosmoObject }

  T_CosmoObject = class
    Pos : RVector3f;
    Speed : RVector3f;
    Accel : RVector3f;
    Orientation : RVector3f;
    Mass : Single;
    procedure Update; virtual;
    procedure Draw; virtual;
  end;

  { T_Planet }

  T_Planet = class(T_CosmoObject)
    Radius : Single;
    Distance : Single;
    Angle : Single;
    AngleSpeed : Single;
    Name : AnsiString;
    Tex : Cardinal;
    TexturePath : AnsiString;
    constructor Create;
    procedure Update; override;
    procedure Draw; override;
  end;

  { T_Scene_Cosmo }

  T_Scene_Cosmo = class(T_Scene)
    CamPos : RVector3f;
    CamAngle : RVector3f;
    Planets : TList; // of T_Planet
    PlanetCount : integer;
    Objects : TList; // of T_CosmoObject
    Control : TS_Control;
    procedure OnMouseDown(AX, AY : integer; AButton : T_MouseButton);
    procedure OnMouseMove(AX, AY : integer);
    procedure OnMouseWheel(AValue : integer);
    procedure PrepareDraw;
    procedure SetCamera;
    procedure DrawPlanets;
    procedure LoadPlanets;
    procedure InitScene; override;
    procedure UpdateScene; override;
    procedure DrawScene; override;
  end;

const
  Earth_radius = 6.371;
  G = 100;
var
  Scene : T_Scene_Cosmo;
implementation

{ T_Planet }

constructor T_Planet.Create;
begin
  Radius := Earth_radius;
  if random < 0.5 then
  Tex:= TextureManager.LoadTexture('data/earth.tga')
  else
    Tex:= TextureManager.LoadTexture('data/cloud.tga')
end;

procedure T_Planet.Update;
begin
  Angle := Angle + AngleSpeed * Window.Scene.dt;
  While Angle >= 2 * Pi do
    Angle := Angle - 2 * Pi;
  While Angle < 0 do
    Angle := Angle + 2 * Pi;
  Pos[0] := Distance * cos(Angle);
  Pos[1] := Distance * sin(Angle);
  Pos[2] := -radius;
end;

procedure T_Planet.Draw;
var
  q : PGLUquadric;
begin
  glPushMatrix;
  glTranslatef(Pos[0], Pos[1], Pos[2]);
  glRotatef(-70, 1, 0, 0);
  glRotatef(23.5, 0, 1, 0);
  glRotatef(Angle * 180 / Pi, 0, 0, 1);
  TextureManager.SetTexture(Tex);

  q := gluNewQuadric;
  gluQuadricTexture(q, GL_TRUE);
  gluSphere(q, Radius, 32, 32);
  gluDeleteQuadric(q);

  TextureManager.UnSetTexture;

  glTranslatef(-Pos[0], -Pos[1], -Pos[2]);
  glPopMatrix;
end;

{ T_CosmoObject }

procedure T_CosmoObject.Update;
var
  i : integer;
  F : Single;
  a : Single;
  xPos : RVector3f;
  xSum : Single;
  xPlanet : T_Planet;
begin
  Accel[0] := 0;
  Accel[1] := 0;
  Accel[2] := 0;
  //AddVector(Accel, Accel, -1);
  for i := 0 to Scene.PlanetCount - 1 do
    begin
      xPlanet := Scene.Planets[i];
      F :=  G * xPlanet.Mass * Mass/ sqr(GetDistance(Pos, xPlanet.Pos));
      a := F / Mass;
      //xPos := xPlanet.Pos;
      xPos[0] := xPlanet.Pos[0] - Pos[0];
      xPos[1] := xPlanet.Pos[1] - Pos[1];
      xPos[2] := xPlanet.Pos[2] - Pos[2];
//      AddVector(xPos, Pos, -1);
      xSum := sqrt(sqr(xPos[0]) + sqr(xPos[1]));// + xPos[2];
      Accel[0] := Accel[0] + a * xPos[0] / xSum;
      Accel[1] := Accel[1] + a * xPos[1] / xSum;
      //Accel[2] := Accel[2] + a * xPos[2] / xSum;
    end;
  AddVector(Speed, Accel, Window.Scene.dt);
  AddVector(Pos, Speed, Window.Scene.dt);
  writeln(Accel[0]:3:3);
end;

procedure T_CosmoObject.Draw;
var
  q : PGLUquadric;
begin
  glTranslated(Pos[0], Pos[1], Pos[2]);
  glColor3f(1,1,0);
  q := gluNewQuadric;
  gluQuadricTexture(q, GL_TRUE);
  gluSphere(q,4, 512, 512);
  gluDeleteQuadric(q);
  glColor3f(1,1,1);
  glTranslated(-Pos[0], -Pos[1], -Pos[2]);
end;

{ T_Scene_Cosmo }

procedure T_Scene_Cosmo.OnMouseDown(AX, AY: integer; AButton: T_MouseButton);
begin

end;

procedure T_Scene_Cosmo.OnMouseMove(AX, AY: integer);
begin
  if Window.Mouse.RMB then
    begin
      CamPos[0] := CamPos[0] + Window.Mouse.dx;
      CamPos[1] := CamPos[1] - Window.Mouse.dy;
{      CamAngle[0] := CamAngle[0] + Window.Mouse.dy;
      CamAngle[1] := CamAngle[1] + Window.Mouse.dx;
      if CamAngle[0] >= 90 then
        CamAngle[0] := 90;
      if CamAngle[0] <= -90 then
        CamAngle[0] := -90;}
    end;

end;

procedure T_Scene_Cosmo.OnMouseWheel(AValue: integer);
begin
  if AValue > 0 then
    CamPos[2] := CamPos[2] * 1.1
  else
    CamPos[2] := CamPos[2] * 0.9;
end;

procedure T_Scene_Cosmo.PrepareDraw;
begin
  glClearColor(0, 0, 0, 1);
  glClearDepth(1);
  glDepthRange(0, 1.0);
  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);

end;

procedure T_Scene_Cosmo.SetCamera;
begin
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
//  glOrtho(-1500, 1500, -1500, 1500, -100, 100);
  gluPerspective(45, Window.Width / Window.Height, 1, 1000);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  glTranslatef(-CamPos[0], -CamPos[1], -CamPos[2]);
  //gluOrtho2D(-1500, 1500, -1500, 1500);
  //gluLookAt(CamPos[0], CamPos[1], CamPos[2], CamPos[0], CamPos[1], 0, 0, 1, 0);

end;

procedure T_Scene_Cosmo.DrawPlanets;
var
  i : integer;
begin
  for i := 0 to (Planets.Count - 1) do
    T_Planet(Planets[i]).Draw;
end;

procedure T_Scene_Cosmo.LoadPlanets;
var
  F : TIniFile;
  i : integer;
  xPlanet : T_Planet;
begin
  F := TIniFile.Create('data' + PathDelim + 'planets.ini');
  PlanetCount := F.ReadInteger('main', 'PlanetCount', 10);
  for i := 0 to PlanetCount - 1 do
    begin
      xPlanet := T_Planet.Create;
      Planets.Add(xPlanet);
      xPlanet.Radius := F.ReadFloat('Planet' + IntToStr(i), 'radius', 0);
      xPlanet.Mass := F.ReadFloat('Planet' + IntToStr(i), 'mass', 0);
      xPlanet.Mass := xPlanet.Radius * 100;
      xPlanet.Distance := F.ReadFloat('Planet' + IntToStr(i), 'distance', 0) * 2;
      xPlanet.AngleSpeed := F.ReadFloat('Planet' + IntToStr(i), 'anglespeed', 0) / 150;
      xPlanet.Angle := F.ReadFloat('Planet' + IntToStr(i), 'angle', Random * 2 * Pi);
      xPlanet.TexturePath := F.ReadString('Planet' + IntToStr(i), 'texture', 'none');
      xPlanet.Tex := TextureManager.LoadTexture('data' + PathDelim + xPlanet.TexturePath);
      xPlanet.Tex := TextureManager.LoadTexture('data' + PathDelim + 'earth1.tga');
      xPlanet.Name := F.ReadString('Planet' + IntToStr(i), 'name', '');
    end;
  F.Free;
end;

procedure T_Scene_Cosmo.InitScene;
var
  i : integer;
  xPlanet : T_Planet;
  xObject : T_CosmoObject;
begin
  inherited InitScene;
  UPS:= 50;
  Objects := TList.Create;
  xObject := T_CosmoObject.Create;
  xObject.Pos[0] := -300;
  xObject.Pos[1] := -100;
  xObject.Mass := 5;
  xObject.Speed[1] := 10;
  Objects.Add(xObject);
  Planets := TList.Create;
  Control := TS_Control.Create;
  Control.Anchor := tsa_Center;
  Control.Width := Window.Width;
  Control.Height := Window.Height;
  Window.Controls.Add(Control);
  Control.RecalcPos;
  Control.OnMouseDownProc := OnMouseDown;
  Control.OnMouseMoveProc := OnMouseMove;
  Control.OnMouseWheelProc := OnMouseWheel;
  CamPos[2] := 2000;
  Scene := Self;
  LoadPlanets;
{  for i := 0 to 9 do
    begin
      xPlanet := T_Planet.Create;
      Planets.Add(xPlanet);
      with xPlanet do
        begin
          Distance := i * 100 + random * 50;
          if i = 0 then
            Distance := 0;
          Radius := random * 30;
          Angle := Random * 2 * Pi;
//          AngleSpeed := 90 * Pi / 180;
          AngleSpeed := (1 - 2 * Random) * 4 * Pi / 180;
  //        Tex := TextureManager.LoadTexture('data/earth.tga');
        end;
    end;}
end;

procedure T_Scene_Cosmo.UpdateScene;
var
  i : integer;
begin
  inherited UpdateScene;
  for i := 0 to Planets.Count - 1 do
    T_Planet(Planets[i]).Update;
  for i := 0 to Objects.Count - 1 do
    T_CosmoObject(Objects[i]).Update;
end;


procedure T_Scene_Cosmo.DrawScene;
var
  q : PGLUquadric;
  i : integer;
begin
  inherited DrawScene;
  PrepareDraw;
  SetCamera;
  DrawPlanets;
  for i := 0 to Objects.Count - 1 do
    T_CosmoObject(Objects[i]).Draw;
end;

end.

