unit _audio;
{ пример использвоания
SoundServer_Activate; // один раз за всю программу. высвободится сам в finaliz
// загрузить трек 223.ogg в источник 223
xSample := SoundServ.LoadSample('23\BackMusic.ogg', '223', true);
// проигарть источник 223
if xSample <> nil then
  SoundServ.Play(xSample);
// загрузить трек 123.ogg в источник 123
xSample := SoundServ.LoadSample('23\123.ogg', '123');
// проигарть источник 123
if xSample <> nil then
  SoundServ.Play(xSample);
sleep(3000);
// получить ссылку на источник трека 223.ogg
xSample := SoundServ.GetSample('23\223.ogg');
// остановить источник трека 223.ogg
if xSample <> nil then
  SoundServ.Stop(xSample);
}
{$mode delphi}

interface

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  ctypes,
  Classes,
  SysUtils,
  openal,
  ogg,
  vorbis,
  _usual;

procedure SoundServer_Activate;

type
  RSoundSource = record
    Source: ALUint;             // Источники
    SourceName: AnsiString;     // Имена источников
  end;
  PSoundSource = ^RSoundSource;

  RSoundSample = record
    Buffer: ALUint;             // Буферы треков
    FileName: AnsiString;       // Названия треков
 //   Source: PSoundSource;       // Ссылки на записи источников
  end;
  PSoundSample = ^RSoundSample;

  { TSoundServer }

  TSoundServer = class
    private
      FSoundSample: TList; // of PSoundSample Хранение треков
      FSoundSource: TList; // of PSoundSource Хранение источников
      FOGGVorbisFile : OggVorbis_File;
      FVorbisInfo : Vorbis_Info;
      FVorbisComment : Vorbis_Comment;
      FFormat : ALEnum;
      // Поиск источника по имени
      function GetSource(ASourceName: string): PSoundSource;
      // Загрузка данных медиа в буфер из файла
      function LoadInBuffer(ASource: PALUint; AFileName: string): boolean;
      function ErrorString(Code: Integer): String;
    public
      constructor create;
      destructor destroy; override;
      // Загрузка новых треков или изменение источника трека, если такой трек загружен
      function LoadSource(AFileName: string; ASourcePoint: PSoundSource = nil; ALoop: boolean = false): PSoundSource;
      // поиск трека по имени
      function GetSample(AFileName: string): PSoundSample;
      procedure Play(ASource: PSoundSource);
      procedure Stop(ASource: PSoundSource);
      function GetStateSource(ASource: PSoundSource): integer;

  end;

const
  director = 'sound' + DirectorySeparator;
var
  SoundServ: TSoundServer;
implementation

var
  ops_callbacks: ov_callbacks;

procedure SoundServer_Activate;
begin
  if SoundServ = nil then
    SoundServ := TSoundServer.create;
end;

constructor TSoundServer.create;
var
  Context: PALCcontext;
  Device: PALCdevice;
begin
  FSoundSample := TList.Create;                // список треков
//  writeln(inttostr(FSoundSample.Count));
  FSoundSource := TList.Create;      // список источников
//   writeln(inttostr(FSoundSource.Count));
  Device := alcOpenDevice(nil);                // девайс
  Context := alcCreateContext(Device, nil);    // контекст
  alcMakeContextCurrent(Context);              // контекст по умолчанию
end;

destructor TSoundServer.destroy;
var
  i : integer;
  xSSample : PSoundSample;
  xSSource : PSoundSource;
  xContext: PALCcontext;
  xDevice: PALCdevice;
begin
  xContext := alcGetCurrentContext;
  xDevice := alcGetContextsDevice(xContext);
  alcDestroyContext(xContext);
  alcCloseDevice(xDevice);

  for i := FSoundSample.Count - 1 downto 0 do begin
    xSSample := FSoundSample[i];
    xSSample^.Buffer := 0;
    xSSample^.FileName := '';
    Dispose(xSSample);
  end;
  FSoundSample.Free;

  for i := FSoundSource.Count -1 downto 0 do begin
    xSSource:= FSoundSource[i];
    xSSource^.Source:= 0;
    xSSource^.SourceName:= '';
  end;
  FSoundSource.Free;

  inherited destroy;
end;

function TSoundServer.LoadSource(AFileName: string;
  ASourcePoint: PSoundSource; ALoop: boolean): PSoundSource;
var
  i : Integer;
  xSSample: PSoundSample;
  xSSource: PSoundSource;
  xState : Integer;
  xFileName: String;
begin
  xFileName := AFileName;
  AFileName := director + AFileName;

  if not FileExists(AFileName) then begin
    Result := nil;
    Log('Несуществующий файл:' + AFileName, true, 'Audio.txt');
    Exit;
  end;
  xSSample := GetSample(AFileName);        // поиск трека в списке по имени
//  xSSource := GetSource(ASourceName);      // поиск источника по имени
  xSSource := ASourcePoint;
  if xSSample = nil then begin             // создаем новый трек, если нет такого
    New(xSSample);
    xSSample^.FileName:= AFileName;        // записываем его имя
    alGenBuffers(1, @xSSample^.Buffer);    // создаем буфер
    LoadInBuffer(@xSSample^.Buffer, AFileName);  // заливаем медиа в буфер
    FSoundSample.Add(xSSample);            // добавляем запись о треке
  end;
  if xSSource = nil then begin             // создаем источник по имени
    New(xSSource);
   // xSSource^.SourceName := ASourceName;   // записываем имя
    alGenSources(1, @xSSource^.Source);    // создаем источник в openAl
    alSource3f(xSSource^.Source, AL_POSITION, 0, 0, 0);
    alSource3f(xSSource^.Source, AL_VELOCITY, 0, 0, 0);
    alSource3f(xSSource^.Source, AL_DIRECTION, 0, 0, 0);
    alSourcef(xSSource^.Source, AL_ROLLOFF_FACTOR, 0);
    alSourcei(xSSource^.Source, AL_SOURCE_RELATIVE, AL_TRUE);
  //  if xFileName = 'jump.ogg' then
  //  alSourcef (xSSource^.Source, AL_GAIN, 5.0);
    FSoundSource.Add(xSSource);            // создаем запись источника
  end;
  alSourcei( xSSource^.Source, AL_BUFFER, 0 );
  if ALoop then                            // лупим трек. по жопе
    alSourcei(xSSource^.Source, AL_LOOPING, AL_TRUE)
  else
    alSourcei(xSSource^.Source, AL_LOOPING, AL_FALSE);
  // помещаем в источник буфер

  alSourceQueueBuffers(xSSource^.Source, 1, @xSSample^.Buffer );
//  xSSample^.Source := xSSource;            // перезаписываем имя источника для трека
  Result := xSSource;

end;

function TSoundServer.GetSample(AFileName: string): PSoundSample;
var
  i: Integer;
begin
  Result := nil;
  For i := 0 to FSoundSample.Count-1 do begin
    if PSoundSample(FSoundSample[i])^.FileName = AFileName then begin
      Result := FSoundSample[i];
    end;
  end;
end;

function TSoundServer.GetSource(ASourceName: string): PSoundSource;
var
  i: integer;
begin
  Result := nil;
  For i := 0 to FSoundSource.Count -1 do begin
    if PSoundSource(FSoundSource[i])^.SourceName = ASourceName then begin
      Result := FSoundSource[i];
    end;
  end;
end;

procedure TSoundServer.Play(ASource: PSoundSource);
var
  xState : integer;
begin
  alGetSourcei( ASource.Source, AL_SOURCE_STATE, xState );
  if xState <> AL_PLAYING then
    alSourcePlay(ASource^.Source);
end;

procedure TSoundServer.Stop(ASource: PSoundSource);
begin
  if ASource <> nil then
    alSourceStop(ASource^.Source);
end;

function TSoundServer.GetStateSource(ASource: PSoundSource): integer;
begin
  result := 0;
  if ASource = nil then
  begin
    exit;
  end;
  alGetSourcei( ASource.Source, AL_SOURCE_STATE, result );
end;

function TSoundServer.LoadInBuffer(ASource: PALUint; AFileName: string): boolean;
var
  xData    : PChar;
  xSize    : Integer;
  xSection : Integer;
  xRes     : Integer;

  xOGGFile: TFileStream;
begin
  Result := false;
  xOGGFile := TFileStream.Create(AFileName, fmOpenRead); // создание файла
  try
    xRes := ov_open_callbacks(xOGGFile, FOGGVorbisFile, nil, 0, ops_callbacks); // трудные функции
    if xRes = 0 then begin
      FVorbisInfo := ov_info(FOGGVorbisFile, -1)^;       // непонятный инфо
      FVorbisComment := ov_comment(FOGGVorbisFile, -1)^; // и комент к нему
      if FVorbisInfo.channels = 1 then
        FFormat := AL_FORMAT_MONO16
      else
        FFormat := AL_FORMAT_STEREO16;
        xSize := 0;
        GetMem(xData, (xOGGFile.Size*16)+SizeOf(xSize)); // выбеляем память под чтение

        try
        while (xSize < (xOGGFile.Size*16)) do
         begin
         xRes := ov_read(FOGGVorbisFile, @PByteArray(xData)^[xSize],(xOGGFile.Size*16)-xSize,False,2,True,@xSection);
         if xRes > 0 then  // читаем много всего и радуемся
          inc(xSize,xRes)
         else
          if xRes < 0 then
            Log(ErrorString(xRes), true, 'LogAudio.txt') // пишем в лог ошибку чтения огг
          else
           break;
         end;
        if xSize = 0 then
        begin
          Result := False;
          exit;
        end;
        alBufferData(ASource^, FFormat, xData, xSize, FVorbisInfo.Rate); // сливаем медиа данные в буфер
        finally
          Result := True;
          FreeMem(xData);
        end;

      result := true;
    end;

  finally
    xOGGFile.Free;
  end;

end;

function TSoundServer.ErrorString(Code: Integer): String;
begin
  case Code of
   OV_EREAD      : Result := 'Read from Media.';
   OV_ENOTVORBIS : Result := 'Not Vorbis data.';
   OV_EVERSION   : Result := 'Vorbis version mismatch.';
   OV_EBADHEADER : Result := 'Invalid Vorbis header.';
   OV_EFAULT     : Result := 'nternal logic fault (bug or heap/stack corruption.';
  else
   Result := 'Unknown Ogg error.';
  end;
end;

{ TOGGStream }
 {
function TOGGStream.playing: boolean;
var
 State : ALEnum;
begin
  alGetSourcei(Source, AL_SOURCE_STATE, State);
  Result := (State = AL_PLAYING);
end;    }

const
  { Constants taken from the MSVC++6 ANSI C library. These values may be
    different for other C libraries! }
  SEEK_SET = 0;
  SEEK_CUR = 1;
  SEEK_END = 2;

  EOF = -1;

function ops_read_func(ptr: pointer; size, nmemb: csize_t; datasource: pointer): csize_t; cdecl;
{ Returns amount of items completely read successfully, returns indeterminate
  value on error. The value of a partially read item cannot be determined. Does
  not lead to valid feof or ferror responses, because they are not possible to
  supply to VorbisFile }
begin
  if (size = 0) or (nmemb = 0) then
  begin
    result := 0;
    exit;
  end;

  try
    result := Int64(TStream(datasource).Read(ptr^, size * nmemb)) div Int64(size);
  except
    result := 0; { Assume nothing was read. No way to be sure of TStreams }
  end;
end;

function ops_seek_func(datasource: pointer; offset: ogg_int64_t; whence: cint): cint; cdecl;
{ Returns zero on success, returns a non-zero value on error, result is undefined
  when device is unseekable. }
begin
  try
    case whence of
      SEEK_CUR: TStream(datasource).Seek(offset, soFromCurrent);
      SEEK_END: TStream(datasource).Seek(offset, soFromEnd);
      SEEK_SET: TStream(datasource).Seek(offset, soFromBeginning);
    end;
    result := 0;
  except
    result := -1;
  end;
end;

function ops_close_func(datasource: pointer): cint; cdecl;
{ Returns zero when device was successfully closed, EOF on error. }
begin
  try
    TStream(datasource).Free;
    result := 0;
  except
    result := EOF;
  end;
end;

function ops_tell_func(datasource: pointer): clong; cdecl;
{ Returns the current position of the file pointer on success, returns -1 on
  error, result is undefined when device is unseekable, does not set 'errno',
  does not perform linebreak conversion. }
begin
  try
    result := TStream(datasource).Position;
  except
    result := -1;
  end;
end;

initialization
  ops_callbacks.read := ops_read_func;
  ops_callbacks.seek := ops_seek_func;
  ops_callbacks.close := ops_close_func;
  ops_callbacks.tell := ops_tell_func;

finalization
  if SoundServ <> nil then
    FreeAndNil(SoundServ);

end.


