unit _apoc_lobby;

{$mode delphi}

interface

uses
  Classes, SysUtils, _window, gl, _apoc_net, sockets, _keys;

type

  { T_Scene_Apoc_Lobby }

  T_Scene_Apoc_Lobby = class(T_Scene)
    ClientSock : T_Socket;
    ClientConnection : T_Connection;
    lMessages : TS_List;
    eMessage : TS_Edit;
    PlayerName : AnsiString;
    procedure InitScene; override;
    procedure InitSceneClient;
    procedure InitNetClient;
    procedure SendClientName;
    procedure SendChatMessage(AMessage : AnsiString);
    procedure CheckNetClient;
    procedure OneMessageUniChar(AChar : AnsiString; AKeyCode : Word);
    procedure UpdateScene; override;
    procedure DrawScene; override;
  end;

const
  LobbyAddress = 'localhost';
  LobbyPort = 23212;
implementation

{ T_Scene_Apoc_Lobby }

procedure T_Scene_Apoc_Lobby.InitScene;
begin
  inherited InitScene;
  if Window.IsConsoleMode = False then
    InitSceneClient;
end;

procedure T_Scene_Apoc_Lobby.InitSceneClient;
begin
  Randomize;
  Window.LoadControlsFromFile('data' + PathDelim + 'apoc' + PathDelim + 'lobby.ini');
  lMessages := TS_List( Window.FindControlByName('lMessages'));
  eMessage := TS_Edit(Window.FindControlByName('eMessage'));
  eMessage.OnUniCharProc := OneMessageUniChar;
  Window.MainControl.RecalcPos;
  Window.MainControl.Color := 0;
  InitNetClient;
  PlayerName := 'Player'+IntToStr(Random(999999));
end;

procedure T_Scene_Apoc_Lobby.InitNetClient;
var
  xAddr : TSockAddr;
begin
  ClientSock := T_Socket.Create(0);
  FillChar(xAddr, SizeOf(xAddr), 0);
  xAddr.sin_family := AF_INET;
  xAddr.sin_addr.s_addr := Net_GetHostByName(LobbyAddress);
  xAddr.sin_port := htons(LobbyPort);
  ClientConnection := ClientSock.FindConnection(xAddr, True);
  ClientSock.Run;
end;

procedure T_Scene_Apoc_Lobby.SendClientName;
var
  MS : TMemoryStream;
begin
  MS := TMemoryStream.Create;
  //MS.WriteDWord(NET_PLAYERNAME);
  MS.WriteBuffer(PlayerName[1], Length(PlayerName));
  ClientSock.Write(ClientConnection, MS.Memory, MS.Size, False, False);
  MS.Free;
end;

procedure T_Scene_Apoc_Lobby.SendChatMessage(AMessage: AnsiString);
var
  MS : TMemoryStream;
begin
  MS := TMemoryStream.Create;
  //MS.WriteBuffer(NET_PLAYERMESSAGE);
  MS.WriteBuffer(AMessage[1], Length(AMessage));
  ClientSock.Write(ClientConnection, MS.Memory, MS.Size);
  MS.Free;
end;

procedure T_Scene_Apoc_Lobby.CheckNetClient;
var
  xPack : P_OutRecord;
  xType : Integer;
  xMem : Pointer;
begin
  repeat
    xPack := ClientSock.GetNextInPacket;
    if (xPack = nil) or (xPack^.Connection <> ClientConnection) then
      break;
    //xMem := xPack^.Data[0];
    xType := Net_ReadInteger(xMem);
    {Case xType of
      NET_PLAYERMESSAGE:
        begin

        end;
    end;}
  until False;
end;

procedure T_Scene_Apoc_Lobby.OneMessageUniChar(AChar: AnsiString; AKeyCode: Word
  );
begin
  if AKeyCode = MK_RETURN then
    begin
      SendChatMessage(PlayerName + ': ' + eMessage.Caption);
      eMessage.Caption := '';
    end;
end;

procedure T_Scene_Apoc_Lobby.UpdateScene;
begin
  inherited UpdateScene;
  SendClientName;
end;

procedure T_Scene_Apoc_Lobby.DrawScene;
begin
  inherited DrawScene;
  glClearColor(0.1, 0.1, 0.1, 1.0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  Window.MainControl.Draw;
  Window.DrawControls;
end;

end.

