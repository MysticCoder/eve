unit _model;

{$mode delphi}

interface

uses
  Classes, SysUtils, gl, _usual, _textures;

type
  T_MatTexture = class
    FileName : AnsiString;
    TextureID : Cardinal;
  end;

  T_Material = class
    Textures : array of T_MatTexture;
  end;

  { T_Mesh }
  RVerticeWeights = packed record
    GroupCount : integer;
    VertexGroups : array of Integer;
    GroupWeights : array of Single;
  end;

  RPoseBone = packed record
    Matrix : RMatrix16f;
  end;

  RPose = packed record
    BoneCount : integer;
    Bones : array of RPoseBone;
  end;

  RAnimation = packed record
    Name : AnsiString;
    FrameCount : integer;
    Poses : array of RPose;
  end;

  //R_TriangleTexCoords = array[0..2]
  T_Mesh = class
    Materials : array of T_Material;
    VertexCount : integer;
    Vertices : array of RVector3f;
    Normals  : array of RVector3f;
    TriangleCount : integer;
    TriangleMaterialIndexes : array of Byte;
    TriangleTexCoords : array of array[0..2] of RVector2f;
    TriangleTexCoords2 : array of RVector2f;
    VerticesWeights : array of RVerticeWeights;
    BoneCount : integer;
    RestPose : RPose;

    AnimIndex : integer; // текущая анимация
    Animations : array of RAnimation;
    AnimTimer : Single;
    AnimLooped : boolean;

    ActualVertices : array of RVector3f;
    procedure LoadFromFile(AFileName : AnsiString);
    procedure Draw;
    procedure StartAnimation(AAnimName : AnsiString;  ADoLoop : boolean = False);
    procedure CalcAnimation(dt : Single);
    function Add(AVertex1, AVertex2 : RVector3f) : RVector3f;
    function Mult(AVertex : RVector3f; AMatrix : RMatrix16f; AWeight : Single) : RVector3f;
  end;

implementation

{ T_Mesh }

procedure T_Mesh.LoadFromFile(AFileName: AnsiString);
var
  F : TFileStream;
  i : integer;
  j : integer;
  k : integer;
  xStringLength : integer;
  procedure _PrepareTextureCoords;
  var
    i : integer;
    j : integer;
    xIndex : integer;
  begin
    Exit;
    SetLength(TriangleTexCoords2, VertexCount);
    for i := 0 to TriangleCount - 1 do
      for j := 0 to 2 do
        begin
          //xIndex := TriangleVertexIndexes[i][j];
          TriangleTexCoords2[xIndex] := TriangleTexCoords[i][j];
          TriangleTexCoords2[xIndex][1] := 1-TriangleTexCoords2[xIndex][1];
        end;
  end;

  procedure _LoadMaterials;
  var
    xCount : integer;
    xMaterial : T_Material;
    i : integer;
    j : integer;
  begin
    xCount := F.ReadByte;
    SetLength(Materials, xCount);
    for i := 0 to High(Materials) do
      begin
        xMaterial := T_Material.Create;
        Materials[i] := xMaterial;
        xCount := F.ReadByte;
        SetLength(xMaterial.Textures, xCount);
        for j := 0 to High(xMaterial.Textures) do
          begin
            Case F.ReadByte of
              0:;
              1:
                begin
                  xMaterial.Textures[j] := T_MatTexture.Create;
                  F.Read(xCount, SizeOf(Integer));
                  SetLength(xMaterial.Textures[j].FileName, xCount);
                  F.Read(xMaterial.Textures[j].FileName[1], xCount);
                  xMaterial.Textures[j].TextureID := TextureManager.LoadTexture(ExtractFileDir(AFileName) + PathDelim + xMaterial.Textures[j].FileName);
                end;
            end;
          end;
      end;
  end;
begin
  try
  F := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
  except
    on E : Exception do
      writeln(E.ClassName + ' ' + E.Message);
  end;
  _LoadMaterials;
  F.Read(TriangleCount, SizeOf(Integer));
  VertexCount := TriangleCount * 3;
  SetLength(Vertices, VertexCount);
  SetLength(Normals, VertexCount);

  F.Read(Vertices[0][0], VertexCount * 3 * SizeOf(Single));
  F.Read(Normals[0][0], VertexCount * 3 * SizeOf(Single));

  SetLength(TriangleMaterialIndexes, TriangleCount);
  F.Read(TriangleMaterialIndexes[0], TriangleCount * SizeOf(Byte));

  SetLength(TriangleTexCoords, TriangleCount);
  for i := 0 to TriangleCount - 1 do
    F.Read(TriangleTexCoords[i][0][0], 3 * 2 * SizeOf(Single));

  SetLength(VerticesWeights, VertexCount);
  for i := 0 to VertexCount - 1 do
    begin
      F.Read(VerticesWeights[i].GroupCount, SizeOf(Integer));
      SetLength(VerticesWeights[i].VertexGroups, VerticesWeights[i].GroupCount);
      SetLength(VerticesWeights[i].GroupWeights, VerticesWeights[i].GroupCount);
      F.Read(VerticesWeights[i].VertexGroups[0], VerticesWeights[i].GroupCount * SizeOf(Integer));
      F.Read(VerticesWeights[i].GroupWeights[0], VerticesWeights[i].GroupCount * SizeOf(Single));
    end;

  // Rest Pose
  F.Read(RestPose.BoneCount, SizeOf(Integer));
  SetLength(RestPose.Bones, RestPose.BoneCount);
  for i := 0 to RestPose.BoneCount - 1 do
    begin
      F.Read(RestPose.Bones[i].Matrix[0], 16 * SizeOf(Single));
    end;

  F.Read(i, SizeOf(Integer));
  SetLength(Animations, i);
  for i := 0 to High(Animations) do
    begin
      F.Read(xStringLength, SizeOf(Integer));
      SetLength(Animations[i].Name, xStringLength);
      F.Read(Animations[i].Name[1], xStringLength);
      writeln(SizeOf(Integer),'---',Animations[i].Name);
      F.Read(Animations[i].FrameCount, SizeOf(Integer));
      SetLength(Animations[i].Poses, Animations[i].FrameCount);
      for j := 0 to Animations[i].FrameCount - 1 do
        begin
          F.Read(Animations[i].Poses[j].BoneCount, SizeOf(Integer));
          SetLength(Animations[i].Poses[j].Bones, Animations[i].Poses[j].BoneCount);
          for k := 0 to Animations[i].Poses[j].BoneCount - 1 do
            F.Read(Animations[i].Poses[j].Bones[k].Matrix[0], 16 * SizeOf(Single));
        end;
    end;

  F.Free;

  SetLength(ActualVertices, Length(Vertices));
  Move(Vertices[0], ActualVertices[0], Length(Vertices) * SizeOf(Vertices[0]));
  _PrepareTextureCoords;
end;

procedure T_Mesh.Draw;
var
  i, j : integer;
  xIndex : integer;
  xMatIndex : integer;
  xOldMatIndex : integer;
  iMat : integer;
  procedure _SetMaterial(AMatIndex : integer);
  var
    xMaterial : T_Material;
    xMatTexture : T_MatTexture;
    i : integer;
  begin
    if AMatIndex = -1 then
      begin
        TextureManager.UnSetTexture;
        Exit;
      end;
    xMaterial := Materials[AMatIndex];
    for i := 0 to high(xMaterial.Textures) do
      begin
        xMatTexture := xMaterial.Textures[i];
        if xMatTexture <> nil then
          begin
            TextureManager.SetTexture(xMatTexture.TextureID);
            Exit;
          end;
      end;
  end;
var
  xTriangleCountToCurrentMat : integer;
  xCurrentTriangle : integer;
begin
  xOldMatIndex := -2;
  xCurrentTriangle := 0;
  for iMat := 0 to high(TriangleMaterialIndexes) do
    begin
      xMatIndex := TriangleMaterialIndexes[iMat];
      if xMatIndex <> xOldMatIndex then
        _SetMaterial(xMatIndex)
      else
        Continue;
      xTriangleCountToCurrentMat := 0;
      repeat
        inc(xTriangleCountToCurrentMat);
      until (xCurrentTriangle + xTriangleCountToCurrentMat > TriangleCount) or (TriangleMaterialIndexes[xCurrentTriangle + xTriangleCountToCurrentMat] <> xMatIndex);
      xOldMatIndex := xMatIndex;
      glEnableClientState(GL_VERTEX_ARRAY);
      glEnableClientState(GL_NORMAL_ARRAY);
      glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    //  glVertexPointer(3, GL_FLOAT, 0, @Vertices[0][0]);

      glVertexPointer(3, GL_FLOAT, 0, @ActualVertices[0][0]);
      glNormalPointer(GL_FLOAT, 0, @Normals[0][0]);
      glTexCoordPointer(2, GL_FLOAT, 0, @TriangleTexCoords[0][0][0]);
      //glDrawArrays(GL_TRIANGLES, 0, (xTriangleCountToCurrentMat));
      glDrawArrays(GL_TRIANGLES, xCurrentTriangle * 3, (xTriangleCountToCurrentMat * 3));
      //glDrawElements(GL_TRIANGLES, (xTriangleCountToCurrentMat)* 3, GL_UNSIGNED_INT, @TriangleVertexIndexes[xCurrentTriangle][0]);
      glDisableClientState(GL_TEXTURE_COORD_ARRAY);
      glDisableClientState(GL_NORMAL_ARRAY);
      glDisableClientState(GL_VERTEX_ARRAY);
      inc(xCurrentTriangle, xTriangleCountToCurrentMat);
    end;
  Exit;
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
//  glVertexPointer(3, GL_FLOAT, 0, @Vertices[0][0]);

  glVertexPointer(3, GL_FLOAT, 0, @ActualVertices[0][0]);
  glNormalPointer(GL_FLOAT, 0, @Normals[0][0]);
  //glDrawElements(GL_TRIANGLES, (TriangleCount)* 3, GL_UNSIGNED_INT, @TriangleVertexIndexes[0][0]);
  glDisableClientState(GL_NORMAL_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
end;

procedure T_Mesh.StartAnimation(AAnimName: AnsiString; ADoLoop: boolean);
var
  i : integer;
begin
  for i := 0 to High(Animations) do
    begin
      if AnsiUpperCase(Animations[i].Name) = AnsiUpperCase(AAnimName) then
        begin
          if (AnimLooped) and (AnimIndex = i) then
            Exit;
          AnimIndex := i;
          AnimLooped := ADoLoop;
          AnimTimer := 0;
          Exit;
        end;
    end;
end;

procedure T_Mesh.CalcAnimation(dt: Single);
var
  i : integer;
  j : integer;
  xBoneWeight : Single;
  xBoneInd : integer;
  xFrameIndf : Single;
  xFrameInd : integer;
begin
  AnimTimer := AnimTimer + dt;
  if AnimTimer > Animations[AnimIndex].FrameCount / 24 then
    if AnimLooped then
      AnimTimer := 0
    else
      Exit;
  xFrameIndf := AnimTimer * 24;
  xFrameInd := Trunc(xFrameIndf);
  xFrameIndf := Frac(xFrameIndf);
  for i := 0 to VertexCount - 1 do
    begin
      ActualVertices[i][0] := 0;
      ActualVertices[i][1] := 0;
      ActualVertices[i][2] := 0;
      if VerticesWeights[i].GroupCount = 0 then
        begin
          writeln('alarm0');
        end;
      for j := 0 to VerticesWeights[i].GroupCount - 1 do
        begin
          xBoneWeight := VerticesWeights[i].GroupWeights[j];
          xBoneInd := VerticesWeights[i].VertexGroups[j];
          ActualVertices[i] := Add(ActualVertices[i], Mult(Vertices[i], Animations[AnimIndex].Poses[xFrameInd].Bones[xBoneInd].Matrix, xBoneWeight));
        end;
      if (ActualVertices[i][0] = 0) and (ActualVertices[i][1] = 0) and (ActualVertices[i][2] = 0) then
        begin
        writeln('alarm');
        ActualVertices[i] := Vertices[i];
        end;
    end;
end;

function T_Mesh.Add(AVertex1, AVertex2: RVector3f): RVector3f;
var
  i : integer;
begin
  for i := 0 to High(Result) do
    Result[i] := AVertex1[i] + AVertex2[i];
end;

function T_Mesh.Mult(AVertex: RVector3f; AMatrix: RMatrix16f; AWeight: Single): RVector3f;
var
  i : integer;
begin
  for i:=0 to 2 do
    begin
      Result[i] := (AVertex[0] * AMatrix[i * 4 + 0] + AVertex[1] * AMatrix[i * 4 + 1] + AVertex[2] * AMatrix[i * 4 + 2] + AMatrix[i * 4 + 3])*AWeight;
//      Result[i] := (AVertex[0]*AMatrix[i + 0 * 4]+AVertex[1]*AMatrix[i + 1 * 4]+AVertex[2]*AMatrix[i + 2 * 4]+AMatrix[i + 3 * 4])*AWeight;
//      if i=3 then exit;
//      Result[i] := (Vec[0]*Matr[0][i]+Vec[1]*Matr[1][i]+Vec[2]*Matr[2][i]);//*Weight;
    end;

end;

end.

