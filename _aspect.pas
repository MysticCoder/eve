unit _aspect;

{$mode delphi}

interface

uses
  Classes, SysUtils, _usual, gl, glu;

type

  tAspectSubscribeType = (astUpdate, astDraw);
  { T_Aspect }

  T_Aspect = class;
  P_Aspect = ^T_Aspect;
  T_AspectClass = class of T_Aspect;

  { T_Aspects }

  T_Aspects = class(TObjectList)
  private
    function GetItem(Index: Integer): T_Aspect;
    procedure SetItem(Index: Integer; AValue: T_Aspect);
  public
    procedure Update;
    procedure Draw;
    function FindAspect(AClass : T_AspectClass) : T_Aspect; overload;
    procedure FindAspect(AClass : T_AspectClass; AAspect : P_Aspect); overload;
    property Items[Index: Integer]: T_Aspect read GetItem write SetItem; default;
  end;


  T_Aspect = class
    protected
      Aspects : T_Aspects;
      procedure InitAspect; virtual;
    public
    Name : AnsiString;
    constructor Create(AName: AnsiString);
    procedure AddAspect(AAspect : T_Aspect);
    procedure Update; virtual;
    procedure Draw; virtual;
    procedure GetAspect(AClass : T_AspectClass; AAspect : P_Aspect);
    procedure Subscribe(ASbType : tAspectSubscribeType; ADoSubscribe : Boolean);
    class function CreateAspects(AName : AnsiString; AAspectClasses : array of T_AspectClass) : T_Aspects;
  end;

  T_Transform = class(T_Aspect)
    Pos : RVector3f;
    Orientation : RVector3f;
  end;

  { T_Physic }

  T_Physic = class(T_Aspect)
    Transform : T_Transform;
    Speed : TVector3f;
    Accel : TVector3f;
  private
    procedure InitAspect; override;
  public
    procedure Update; override;
  end;

  { T_Graphic }

  T_Graphic = class(T_Aspect)
    Transform : T_Transform;
  private
    procedure InitAspect; override;
  public
    procedure Draw; override;
  end;

var
  UpdateSubscribeList : T_Aspects;
  DrawSubscribeList : T_Aspects;
implementation

{ T_Graphic }

procedure T_Graphic.InitAspect;
begin
  inherited InitAspect;
  GetAspect(T_Transform, @Transform);
  Subscribe(astDraw, True);
end;

procedure T_Graphic.Draw;
var
  q : PGLUquadric;
begin
  inherited Draw;
  q := gluNewQuadric;
  glTranslatef(Transform.Pos[0], Transform.Pos[1], Transform.Pos[2]);
  gluSphere(q, 0.3, 4, 4);
  glTranslatef(-Transform.Pos[0], -Transform.Pos[1], -Transform.Pos[2]);
  gluDeleteQuadric(q);
end;

{ T_Aspects }

function T_Aspects.GetItem(Index: Integer): T_Aspect;
begin
  Result:=T_Aspect(Inherited Get(Index));
end;

procedure T_Aspects.SetItem(Index: Integer; AValue: T_Aspect);
begin
  Inherited SetItem(Index, AValue);
end;

procedure T_Aspects.Update;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    Items[i].Update;
end;

procedure T_Aspects.Draw;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    Items[i].Draw;
end;

function T_Aspects.FindAspect(AClass: T_AspectClass): T_Aspect;
var
  i : integer;
  xAspect : T_Aspect;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    begin
      xAspect := Items[i];
      if xAspect.ClassType = AClass then
        begin
          Result := xAspect;
          Exit;
        end;
    end;
end;

procedure T_Aspects.FindAspect(AClass: T_AspectClass; AAspect: P_Aspect);
begin
  AAspect^ := FindAspect(AClass);
end;

{ T_Physic }

procedure T_Physic.InitAspect;
begin
  inherited InitAspect;
  GetAspect(T_Transform, @Transform);
  Subscribe(astUpdate, True);
end;

procedure T_Physic.Update;
var
  i : integer;
begin
  inherited Update;
  if Transform = nil then
    Exit;
  for i := Low(Speed.Data) to High(Speed.Data) do
    Speed[i] := Speed[i] + Accel[i];
  for i := Low(Accel.Data) to High(Accel.Data) do
    Accel[i] := 0;
  for i := Low(Transform.Pos) to High(Transform.Pos) do
    Transform.Pos[i] := Transform.Pos[i] + Speed[i];
end;

{ T_Aspect }

constructor T_Aspect.Create(AName: AnsiString);
begin
  Name := AName;
end;

procedure T_Aspect.AddAspect(AAspect: T_Aspect);
begin
  Aspects.Add(AAspect);
end;

procedure T_Aspect.Update;
begin

end;

procedure T_Aspect.Draw;
begin

end;

procedure T_Aspect.InitAspect;
begin

end;

procedure T_Aspect.GetAspect(AClass: T_AspectClass; AAspect: P_Aspect);
var
  xAspect : T_Aspect;
  i : integer;
begin
  for i := 0 to Aspects.Count - 1 do
    begin
      xAspect := T_Aspect(Aspects[i]);
      if xAspect.ClassType = AClass then
        begin
          AAspect^ := xAspect;
          Exit;
        end;
    end;
end;

procedure T_Aspect.Subscribe(ASbType: tAspectSubscribeType;
  ADoSubscribe: Boolean);
var
  xList : TList;
begin
  xList := nil;
  Case ASbType of
    astUpdate:
      xList := UpdateSubscribeList;
    astDraw:
      xList := DrawSubscribeList;
  end;
  Case ADoSubscribe of
    True:
      xList.Add(Self);
    False:
      xList.Remove(Self);
  end;
end;

class function T_Aspect.CreateAspects(AName: AnsiString;
  AAspectClasses: array of T_AspectClass): T_Aspects;
var
  i : integer;
  xClass : T_AspectClass;
  xAspect : T_Aspect;
begin
  Result := T_Aspects.create(False);
  for i := Low(AAspectClasses) to high(AAspectClasses) do
    begin
      xClass := AAspectClasses[i];
      xAspect := xClass.Create(AName);
      xAspect.Aspects := Result;
      Result.Add(xAspect);
    end;
  for i := 0 to Result.Count - 1 do
    Result[i].InitAspect;
end;

end.

