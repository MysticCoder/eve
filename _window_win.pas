unit _window_win;

{$mode delphi}{$H+}

interface
  uses
    _window, gl, glu, glext, LazUTF8,
    windows;
type

  { TS_Window_Win }

  TS_Window_Win = class(TS_Window)
    protected
      procedure SetCaption(AValue : string); override;
      procedure SetHeight(AValue: integer); override;
      procedure SetLeft(AValue: integer); override;
      procedure SetTop(AValue: integer); override;
      procedure SetWidth(AValue: integer); override;
      function GetScreenWidth : integer; override;
      function GetScreenHeight : integer; override;
    public
      DC : HDC;
      procedure SwapBuffers; override;
      procedure ProcessEvents; override;
      constructor Create; override;
  end;


implementation

  uses
    _textures;
{ TS_Window_Win }

function WinProc(wnd : Cardinal; Msg : Cardinal; wPar, lPar : Integer) : integer; stdcall;
var
  xWin : TS_Window_Win;
  xUTFString : AnsiString;
  xMsg : T_Message;
begin
  xWin := Pointer(GetWindowLong(wnd, GWL_USERDATA));
  Case Msg of
    WM_KEYDOWN :
      xWin.Keys[wPar] := True;
    WM_KEYUP :
      xWin.Keys[wPar] := False;
    WM_CHAR :
      begin
        SetLength(xUTFString, 2);
        xUTFString := #0#0;
        PWord(@xUTFString[1])^ := LoWord(wPar);
        xUTFString := AnsiToUtf8(xUTFString);
        UTF8FixBroken(xUTFString);

        xUTFString := UnicodeCharToString(@wPar);//xUTFString);
        //xUTFString := UTF8Encode(xUTFString);
        writeln(Length(xUTFString));
        SetConsoleCP(CP_UTF8);
        SetConsoleOutputCP(CP_UTF8);
        writeln(xUTFString);
        writeln(wPar);
        xMsg._type := tmt_UniChar;
        xMsg.UniChar := xUTFString;
        xMsg.KeyCode := wPar;
        xWin.MainControl.ProcessMessage(xMsg);
      end;
    WM_CLOSE :
      begin
        xWin.Terminated := True;
        PostQuitMessage(0);
      end;
    WM_LBUTTONDOWN:
      begin
        xMsg._type := tmt_MouseDown;
        xMsg.X := LoWord(lPar);
        xMsg.Y := HiWord(lPar);
        xMsg.MButton := mbLeft;
        xWin.Mouse.LMB := True;
        xWin.MainControl.ProcessMessage(xMsg);
        //xWin.MainControl.OnMouseDown(LOWORD(lPar), HiWord(lPar), mbLeft);
      end;
    WM_LBUTTONUP:
      begin
        xMsg._type := tmt_MouseUp;
        xMsg.X := LoWord(lPar);
        xMsg.Y := HiWord(lPar);
        xMsg.MButton := mbLeft;
        xWin.Mouse.LMB := False;
        xWin.MainControl.ProcessMessage(xMsg);
      end;
    WM_RBUTTONDOWN:
      begin
        xMsg._type := tmt_MouseDown;
        xMsg.X := LoWord(lPar);
        xMsg.Y := HiWord(lPar);
        xMsg.MButton := mbRight;
        xWin.Mouse.RMB := True;
        xWin.MainControl.ProcessMessage(xMsg);
      end;
    WM_RBUTTONUP:
      begin
        xMsg._type := tmt_MouseUp;
        xMsg.X := LoWord(lPar);
        xMsg.Y := HiWord(lPar);
        xMsg.MButton := mbRight;
        xWin.Mouse.RMB := False;
        xWin.MainControl.ProcessMessage(xMsg);
      end;

    WM_MOUSEMOVE:
      begin
        xMsg._type := tmt_MouseMove;
        xMsg.X := LoWord(lPar);
        xMsg.Y := HiWord(lPar);
        xWin.Mouse.dx := LoWord(lPar) - xWin.Mouse.X;
        xWin.Mouse.dy := HiWord(lPar) - xWin.Mouse.Y;
        xWin.Mouse.X := LoWord(lPar);
        xWin.Mouse.Y := HiWord(lPar);
        xWin.MainControl.ProcessMessage(xMsg);
      end;
    WM_MOUSEWHEEL:
      begin
        xMsg._type := tmt_MouseWheel;
        xMsg.MouseWheel := SmallInt(HiWord(wPar)) div 120;
        xWin.MainControl.ProcessMessage(xMsg);
      end;
  end;

  Result := DefWindowProc(wnd, Msg, wPar, lPar);
end;

procedure TS_Window_Win.SetCaption(AValue: string);
begin
  FCaption := AValue;
  SetWindowText(Handle, PChar(AValue));
end;

procedure TS_Window_Win.SetHeight(AValue: integer);
begin
  if FHeight=AValue then
    Exit;
  inherited;
  SetWindowPos(Handle, 0, Left, Top, Width, Height, 0);
  glViewport(0,0,Width,Height);
end;

procedure TS_Window_Win.SetLeft(AValue: integer);
begin
  if FLeft=AValue then Exit;
  inherited;
  FLeft:=AValue;
  SetWindowPos(Handle, 0, Left, Top, Width, Height, 0);
end;

procedure TS_Window_Win.SetTop(AValue: integer);
begin
  if FTop=AValue then Exit;
  inherited;
  FTop:=AValue;
  SetWindowPos(Handle, 0, Left, Top, Width, Height, 0);
end;

procedure TS_Window_Win.SetWidth(AValue: integer);
begin
  if FWidth=AValue then Exit;
  inherited;
  SetWindowPos(Handle, 0, Left, Top, Width, Height, 0);
end;

function TS_Window_Win.GetScreenWidth: integer;
begin
  Result := GetSystemMetrics(SM_CXSCREEN);
end;

function TS_Window_Win.GetScreenHeight: integer;
begin
  Result := GetSystemMetrics(SM_CYSCREEN);
end;

procedure TS_Window_Win.SwapBuffers;
begin
  windows.SwapBuffers(DC);
end;


procedure TS_Window_Win.ProcessEvents;
var
  Msg : TMsg;
begin
  inherited;

  while PeekMessage(Msg, 0, 0, 0, PM_REMOVE) do
    begin
      TranslateMessage(Msg);
      DispatchMessage(Msg);
    end;
end;

constructor TS_Window_Win.Create;
var
  xClass : TWNDCLASS;
  pfd : TPIXELFORMATDESCRIPTOR;
  ipf : integer;
begin
  inherited;

  FillChar(xClass, SizeOf(xClass), 0);
  xClass.style := CS_OWNDC;
  xClass.lpfnWndProc := @WinProc;
  xClass.hInstance := system.HINSTANCE;
  xClass.lpszClassName := PChar(string(ClassName));
  RegisterClass(xClass);
  Handle := CreateWindow(xClass.lpszClassName, nil, WS_VISIBLE {or WS_MINIMIZEBOX or WS_SYSMENU}or WS_POPUP, 0, 0, 100, 100, 0, 0, xClass.hInstance, nil);
  SetWindowLong(Handle, GWL_USERDATA, Cardinal(Self));
  DC := GetWindowDC(Handle);

  FillChar(pfd, SizeOf(pfd), 0);
  pfd.nSize := SizeOf(pfd);
  pfd.dwFlags := PFD_DOUBLEBUFFER or PFD_DRAW_TO_WINDOW or PFD_SUPPORT_OPENGL;
  pfd.cColorBits := 32;
  pfd.cDepthBits := 64;
  pfd.nVersion := 1;
  pfd.iPixelType := PFD_TYPE_RGBA;
  ipf := ChoosePixelFormat(DC, @pfd);
  SetPixelFormat(DC, ipf, @pfd);

  GLDC := wglCreateContext(DC);
  wglMakeCurrent(DC, GLDC);
  glTexImage3D := wglGetProcAddress('glTexImage3D');
  Load_GL_version_2_0;
end;

end.

