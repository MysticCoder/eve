unit _scene_apoc;

{$mode delphi}

interface

uses
  glu, _model, SyncObjs, Classes, SysUtils, _window, _usual, gl, {glu,} glext,math, _keys, _apoc_map, _apoc_unit, _timer, _apoc_net, typinfo;

type



  { R_Connection }

  { T_Client }

  T_Client = class
    Connection : T_Connection;
    ControlUnit : T_Unit;
    constructor Create(AConnection : T_Connection);
  end;

  { T_Scene_Apoc }
  T_Scene_Apoc = class(T_Scene)
    GameTick : integer;
    Timer : T_Timer;
    Map : T_Map;
    ServSock : T_Socket;
    ClientSock : T_Socket;
    Clients : TObjectList;
    ControlledUnit : T_Unit;
    ServConnection : T_Connection;
    procedure ReadServ(APack : P_OutRecord);
    procedure ReadClient(APack : P_OutRecord);
    function FindClient(AConnection : T_Connection) : T_Client;
    procedure SendMapData(AClient : T_Client);
    procedure SendUnitsData(AClient : T_Client; AOnlyDiffs : boolean = False);
    procedure SendControlData;
    procedure InitScene; override;
    procedure UpdateScene; override;
    procedure DrawScene; override;
    procedure FreeScene; override;
    function MouseToMap : RVector2f;
  end;

const
  NET_METKA = 1234;
  NET_MAPCELL = 1001;
  NET_MAPSIZE = 1000;
  NET_UNIT = 1002;
  NET_CONTROLLEDUNIT = 1003;
  NET_UNITDIFF = 1004;
var
  Scene : T_Scene_Apoc;
implementation

  uses
    _apoc_unit_controller, sockets;

{ T_Client }

constructor T_Client.Create(AConnection: T_Connection);
begin
  Connection := AConnection;
end;


{ T_Scene_Apoc }

procedure T_Scene_Apoc.ReadServ(APack: P_OutRecord);
var
  i : integer;
  xMetka : Integer;
  xServiceID : integer;
  xInd : Pointer;
  xClient : T_Client;
  xUnit : T_Unit;
  xType : integer;
begin
  xInd := @APack^.Data[0];
  xMetka := Net_ReadInteger(xInd);

  if xMetka <> NET_METKA then
    Exit;
  xClient := FindClient(APack^.Connection);
  if xClient = nil then
    begin
      xClient := T_Client.Create(APack^.Connection);
      Clients.Add(xClient);
      SendMapData(xClient);
      xUnit := T_Unit.Create([T_PhysicUnit, T_LogicUnit, T_GraphUnit]);
      Map.AddUnit(xUnit, True);
      xUnit.Physic.Pos[0] := Random(Map.Width);
      xUnit.Physic.Pos[1] := Random(Map.Height);
      xUnit.Physic.Speed := 5;
      xUnit.Physic.Radius:= 0.4;
      xClient.ControlUnit := xUnit;
      T_LogicUnit(xUnit.Logic).Controller := T_UnitControllerPlayer.Create;
      T_LogicUnit(xUnit.Logic).Controller.Host := xUnit;
      xUnit.UpdateNetStream;
      SendUnitsData(xClient);
      Exit;
    end;
  xType := Net_ReadInteger(xInd);
  Case xType of
    NET_CONTROLLEDUNIT:
      begin
        Net_ReadInteger(xInd); // Logic.ClassType
        xClient.ControlUnit.Logic.LoadFromNetStream(xInd);
      end;
  end;
//  while PtrUInt(xInd) < PtrUInt(@AReadBuf[0]) + ASize do
    begin
//      xServiceID := Net_ReadInteger(xInd);
      //CallService(xService, xInd, )
    end;

end;

procedure T_Scene_Apoc.ReadClient(APack: P_OutRecord);
var
  xMetka : Integer;
  xInd : Pointer;
  xType : integer;
  i, j : integer;
  xWidth, xHeight : integer;
  xUnitID : integer;
  xUnit : T_Unit;
begin
  xInd := @APack^.Data[0];
  xMetka := Net_ReadInteger(xInd);

  if xMetka <> NET_METKA then
    Exit;
  while PtrUInt(xInd) < PtrUInt(@APack^.Data[0]) + APack^.DataSize do
    begin
      xType := Net_ReadInteger(xInd);
      Case xType of
        NET_MAPSIZE:
          begin
            xWidth := Net_ReadInteger(xInd);
            xHeight := Net_ReadInteger(xInd);
            if (xWidth <> Map.Width) or (xHeight <> Map.Height) then
              begin
                SetLength(Map.Data, xWidth, xHeight);
                Map.Width := xWidth;
                Map.Height := xHeight;
              end;
            for i := 0 to Map.Width - 1 do
              for j := 0 to Map.Height - 1 do
                Map.Data[i][j].Units := TObjectList.create(False);
          end;
        NET_MAPCELL:
          begin
            i := Net_ReadInteger(xInd);
            j := Net_ReadInteger(xInd);
            Map.Data[i][j].Height := Net_ReadSingle(xInd);
            Map.Data[i][j].LightPassPower := Net_ReadSingle(xInd);
            Map.Data[i][j].Passable := Net_ReadBoolean(xInd);
            Map.Data[i][j]._type := Net_ReadInteger(xInd);
          end;
        NET_UNIT:
          begin
            xUnitID := Net_ReadInteger(xInd);
            xUnit := T_Unit(Map.FindUnitByID(xUnitID));
            if xUnit = nil then
              begin
                xUnit := T_Unit.CreateFromNetStream(xInd);
                Map.AddUnit(xUnit, False);
                xUnit.ID := xUnitID;
              end
            else
              xUnit.LoadFromNetStream(xInd);
          end;
        NET_UNITDIFF:
          begin
            xUnitID := Net_ReadInteger(xInd);
            xUnit := T_Unit(Map.FindUnitByID(xUnitID));
            if xUnit = nil then
              Exit;
            xUnit.LoadFromNetDiffStream(xInd);
          end;
        NET_CONTROLLEDUNIT:
          begin
            xUnitID := Net_ReadInteger(xInd);
            xUnit := T_Unit(Map.FindUnitByID(xUnitID));
            if xUnit <> nil then
              if T_LogicUnit(xUnit.Logic).Controller = nil then
                begin
                  ControlledUnit := xUnit;
                  T_LogicUnit(xUnit.Logic).Controller := T_UnitControllerPlayer.Create;
                  T_LogicUnit(xUnit.Logic).Controller.Host := xUnit;
                end;
          end;
        else
          writeln;
      end;
      //CallService(xService, xInd, )
    end;

end;


function T_Scene_Apoc.FindClient(AConnection: T_Connection): T_Client;
var
  i : integer;
  xClient : T_Client;
begin
  Result := nil;
  for i := 0 to Clients.Count - 1 do
    begin
      xClient := T_Client(Clients[i]);
      if xClient.Connection = AConnection then
        begin
          Result := xClient;
          Exit;
        end;
    end;
end;

procedure T_Scene_Apoc.SendMapData(AClient: T_Client);
var
  i : integer;
  j : integer;
  xMem : Pointer;
  xMemStart : Pointer;
  xSize : integer;
begin
  xMemStart := GetMem(1024);
  xMem := xMemStart;
  xSize := 0;
  Net_WriteInteger(xMem, NET_METKA, xSize);
  Net_WriteInteger(xMem, NET_MAPSIZE, xSize);
  Net_WriteInteger(xMem, Map.Width, xSize);
  Net_WriteInteger(xMem, Map.Height, xSize);
  for i := 0 to Map.Width - 1 do
    for j := 0 to Map.Height - 1 do
      begin
        Net_WriteInteger(xMem, NET_MAPCELL, xSize);
        Net_WriteInteger(xMem, i, xSize);
        Net_WriteInteger(xMem, j, xSize);
        Net_WriteSingle(xMem, Map.Data[i][j].Height, xSize);
        Net_WriteSingle(xMem, Map.Data[i][j].LightPassPower, xSize);
        Net_WriteBoolean(xMem, Map.Data[i][j].Passable, xSize);
        Net_WriteInteger(xMem, Map.Data[i][j]._type, xSize);
        if xSize > 900 then
          begin
//            Sleep(1);
            ServSock.Write(AClient.Connection, xMemStart, xSize, True);
            xMem := xMemStart;
            xSize := 0;
            Net_WriteInteger(xMem, NET_METKA, xSize);
            Net_WriteInteger(xMem, NET_MAPSIZE, xSize);
            Net_WriteInteger(xMem, Map.Width, xSize);
            Net_WriteInteger(xMem, Map.Height, xSize);
          end;
      end;
  if xSize > 4 then
    ServSock.Write(AClient.Connection, xMemStart, xSize, True);
  Freemem(xMemStart);
end;

procedure T_Scene_Apoc.SendUnitsData(AClient: T_Client; AOnlyDiffs: boolean);
var
  i : integer;
  j : integer;
  xUnit : T_Unit;
  xMem : Pointer;
  xMemStart : Pointer;
  xClient : T_Client;
  xSize : integer;
begin
  xMem := GetMem(1024);
  xMemStart := xMem;
  for i := 0 to Clients.Count - 1 do
    begin
      xClient := T_Client(Clients[i]);
      xSize := 0;
      Net_WriteInteger(xMem, NET_METKA, xSize);
      Net_WriteInteger(xMem, NET_CONTROLLEDUNIT, xSize);
      Net_WriteInteger(xMem, xClient.ControlUnit.ID, xSize);
      for j := Map.Units.Count - 1 downto 0 do
        begin
          xUnit := T_Unit(Map.Units[j]);
          if (AOnlyDiffs) and (not xUnit.DoRefreshNet) then
            begin
              if (xUnit.IsNetDiffVarsChangedCalced) then
                begin
                  Net_WriteInteger(xMem, NET_UNITDIFF, xSize);
                  Move(xUnit.NetDiffStream.Memory^, xMem^, xUnit.NetDiffStream.Size);
                  inc(xMem, xUnit.NetDiffStream.Size);
                  inc(xSize, xUnit.NetDiffStream.Size);
                end;
            end
          else
            begin
              Net_WriteInteger(xMem, NET_UNIT, xSize);
              Move(xUnit.NetStream.Memory^, xMem^, xUnit.NetStream.Size);
              inc(xMem, xUnit.NetStream.Size);
              inc(xSize, xUnit.NetStream.Size);
            end;
          if xSize > 900 then
            begin
              ServSock.Write(xClient.Connection, xMemStart, xSize);
              xSize := 0;
              xMem := xMemStart;
              Net_WriteInteger(xMem, NET_METKA, xSize);
            end;
        end;
      if xSize > 4 then
        ServSock.Write(xClient.Connection, xMemStart, xSize);
      xMem := xMemStart;
      xSize:= 0;
    end;
  Freemem(xMemStart);
end;

procedure T_Scene_Apoc.SendControlData;
var
  xUnit : T_Unit;
  xStream : TMemoryStream;
  xDiffStream : TMemoryStream;
  xInt : integer;
begin
  //exit;
  if ControlledUnit = nil then
    Exit;
  xStream := TMemoryStream.Create;
  xInt := NET_METKA;
  xStream.Write(xInt, SizeOf(xInt));
  xInt := NET_CONTROLLEDUNIT;
  xStream.Write(xInt, SizeOf(xInt));

  xDiffStream := TMemoryStream.Create;
  (ControlledUnit.Logic as T_LogicUnit).UpdateNetStream(xStream, xDiffStream);
  xDiffStream.Free;

  ClientSock.Write(ServConnection, xStream.Memory, xStream.Size);
  xStream.Free;
end;

procedure T_Scene_Apoc.InitScene;
var
  xUnit : T_Unit;
  xPropList : PPropList;
  xPropCount : integer;
  xPropInfo : PPropInfo;
  i : integer;
  xTo : TSockAddr;
  xData : array of Byte;
  xCOnnection : T_Connection;
begin
  inherited InitScene;
  if not Window.IsConsoleMode then
    begin
    end;
  UPS := 50;
  Clients := TObjectList.create(True);
  if Window.IsConsoleMode then
    begin
      ServSock := T_Socket.Create(10000);
      ServSock.Run;

    end
  else
    begin
      Randomize;
      ClientSock := T_Socket.Create(Random(65536));
      ClientSock.Run;
      FillChar(xTo, SizeOf(xTo), 0);
      //xTo.sin_addr := sockets.StrToNetAddr('192.168.1.55');
      xTo.sin_addr := sockets.StrToNetAddr(ParamStr(1));
      //xTo.sin_addr := sockets.StrToNetAddr('188.120.225.228');
      xTo.sin_family := PF_INET;
      xTo.sin_port:= htons(10000);
      SetLength(xData, 4);
      PInteger(@xData[0])^ := NET_METKA;
      xCOnnection := ClientSock.FindConnection(xTo, True);
      ClientSock.Write(xConnection, @xData[0], 4);
      ServConnection := xCOnnection;
    end;
  DoDrawOnlyAfterUpdate:= True;
  Timer := T_Timer.Create(UPS);
  Scene := Self;
  Map := T_Map.Create(Timer);
  if Window.IsConsoleMode then
    Map.GenSimpleMap
  else
    begin
//      SetLength(Map.Data, 1000, 1000);
//      Map.Width := 1000;
//      Map.Height := 1000;
    end;
  {xUnit := T_Unit(Map.Units[0]);
  xUnit.Physic.Pos[0] := 5;
  xUnit.Physic.Pos[1] := 5;
  xUnit.Physic.Speed := 1.5;
  xUnit.Physic.Radius := 0.5;
  T_LogicUnit(xUnit.Logic).Controller := T_UnitControllerPlayer.Create;
  T_LogicUnit(xUnit.Logic).Controller.Host := xUnit;}
end;

procedure T_Scene_Apoc.UpdateScene;
var
  xPack : P_OutRecord;
  xClient : T_Client;
  i : integer;
begin
  inherited UpdateScene;
  //if not Window.Keys[MK_RETURN] then
  //  Exit;
  //Sleep(10);
  Map.Update;
  inc(GameTick);
  if Window.IsConsoleMode then
    begin
      repeat
        xPack := ServSock.GetNextInPacket;
        if xPack <> nil then
          begin
            ReadServ(xPack);
          end;
      until xPack = nil;
      for i := 0 to Clients.Count - 1 do
        begin
          xClient := T_Client(Clients[i]);
          SendUnitsData(xClient, False);
        end;
    end;
  if not Window.IsConsoleMode then
    begin
      SendControlData;
      repeat
        xPack := ClientSock.GetNextInPacket;
        if xPack <> nil then
          begin
            ReadClient(xPack);
          end;
      until xPack = nil;
    end;
  //writeln('asd');
end;

procedure T_Scene_Apoc.DrawScene;
var
  i : integer;
begin
  inherited DrawScene;
  //glEnable(GL_LIGHT0);
  //glEnable(GL_LIGHTING);
  glClearColor(0, 0, 0, 1);
  glClearDepth(1);
  glDepthRange(0, 1);
  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  //gluPerspective(45, 800/600, 0.01, 100);
  glOrtho(0, Map.Width, 0, Map.Height, -100, 100);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  //glTranslatef(0, 0, -40);
  Map.Draw;
  //Model.CalcAnimation(0);
  //glEnable(GL_DEPTH_TEST);

  //glRotatef(-90, 1, 0, 0);
  //Model.Draw;
end;

procedure T_Scene_Apoc.FreeScene;
begin
  inherited FreeScene;
end;

function T_Scene_Apoc.MouseToMap: RVector2f;
begin
  Result[0] := (Window.Mouse.X / Window.Width) * 30;
  Result[1] := ((Window.Height - Window.Mouse.Y) / Window.Height) * 20;
end;

end.

