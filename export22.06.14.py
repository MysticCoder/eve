import bpy
import struct
import os
import math
import xml.dom.minidom

textures = []
def findtexid(texname):
    id = 0
    for tex in textures:
        if tex == texname:
            return(id)
        id = id + 1
    return(-1)    
        
def SaveVertexs(obj, meshnode):
    vertcount = len(obj.data.vertices)
    meshnode.setAttribute('vertexcount', str(vertcount))
    vertexstring = ''
    for vert in obj.data.vertices:
        for vertco in vert.co:
            vertexstring = vertexstring + str(vertco) + ' '
    meshnode.setAttribute('vertices', vertexstring)        
            
    vertexstring = ''
    for vert in obj.data.vertices:
        for normal in vert.normal:
            vertexstring = vertexstring + str(normal) + ' '
    meshnode.setAttribute('normals', vertexstring)        

    polycount = len(obj.data.polygons)
    meshnode.setAttribute('polycount', str(polycount))
    
    vertexstring = ''    
    for poly in obj.data.polygons:
        for vert in poly.vertices:
            vertexstring = vertexstring + str(vert) + ' '
    meshnode.setAttribute('polygons', vertexstring)        
    
    vertexstring = ''    
    for poly in obj.data.polygons:
        vertexstring = vertexstring + str(poly.material_index) + ' '
    meshnode.setAttribute('polygonmaterials', vertexstring)
    
    vertexstring = ''    
    for poly in obj.data.polygons:
        texname = ''
        if len(obj.data.uv_textures) > 0:
            if hasattr(obj.data.uv_textures[0].data[poly.index], 'image'):
                if hasattr(obj.data.uv_textures[0].data[poly.index].image, 'name'):
                    texname = obj.data.uv_textures[0].data[poly.index].image.name
        texid = findtexid(texname)        
        vertexstring = vertexstring + str(texid) + ' '
    meshnode.setAttribute('polygontextures', vertexstring)
    
    vertexstring = ''
    if len(obj.data.uv_textures) > 0:
        for poly in obj.data.polygons:
            for i in range(0,3):
            #    print(i)
                vertexstring = vertexstring + str(obj.data.uv_layers[0].data[poly.loop_indices[i]].uv[0]) + ' ' + str(obj.data.uv_layers[0].data[poly.loop_indices[i]].uv[1]) + ' '      
            #    vertexstring = vertexstring + str(obj.data.uv_layers[0].data[poly.loop_indices[0]].uv[0]) + ' ' + str(obj.data.uv_layers[0].data[poly.loop_indices[0]].uv[1]) + ' '      
            #    vertexstring = vertexstring + str(obj.data.uv_layers[0].data[poly.loop_indices[1]].uv[0]) + ' ' + str(obj.data.uv_layers[0].data[poly.loop_indices[1]].uv[1]) + ' '      
            #    vertexstring = vertexstring + str(obj.data.uv_layers[0].data[poly.loop_indices[2]].uv[0]) + ' ' + str(obj.data.uv_layers[0].data[poly.loop_indices[2]].uv[1]) + ' '      
    meshnode.setAttribute('polygonuvs', vertexstring)
    vertexstring = ''
    
def SaveMaterials(obj, meshnode):
    for mat in obj.data.materials:
        matnode = doc.createElement('material')
        meshnode.appendChild(matnode)
        matnode.setAttribute('name', mat.name)
        matnode.setAttribute('diffuse', str(mat.diffuse_color[0]) + ' ' + str(mat.diffuse_color[1]) + ' ' + str(mat.diffuse_color[2]));
        matnode.setAttribute('specular', str(mat.specular_color[0]) + ' ' + str(mat.specular_color[1]) + ' ' + str(mat.specular_color[2]));

def SaveTextures(meshnode):
    for tex in bpy.data.textures:
        texnode = doc.createElement('texture')
        meshnode.appendChild(texnode)
        texnode.setAttribute('filename', tex.image.name)
        textures.append(tex.image.name)
                              
def SaveMesh(obj, node):
    print(obj.name)
    obj1 = obj.copy()
    #obj.data = obj.to_mesh(scene, True, 'PREVIEW')
    meshnode = doc.createElement('mesh')
    meshnode.setAttribute('name', obj1.name)
    node.appendChild(meshnode)
    SaveTextures(meshnode)
    SaveMaterials(obj1, meshnode)
    SaveVertexs(obj1, meshnode)
    #olddata.user_clear()
    #bpy.context.blend_data.meshes.remove(olddata)
    obj1.user_clear()
    del(obj1)
    obj.user_clear()
    
doc = xml.dom.minidom.Document()
root = doc.createElement('root')
doc.appendChild(root)
f = open('d:\creative\out1.txt','w')
try:
    scene = bpy.context.scene    
    for obj in bpy.data.scenes[0].objects:
        print(obj.name)
        if obj.type!='MESH':
            continue
        SaveMesh(obj, root)
        obj.user_clear()
    for obj in bpy.data.scenes[0].objects:
        if obj.type!='MESH':
            continue
        obj.user_clear()
    doc.normalize();  
    f.write(doc.toprettyxml())
    f.flush()
finally:
    f.close()
    del(f)
    del(textures)
    del(doc)    
print('Done')