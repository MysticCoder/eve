unit _town;

{$mode delphi}

interface

uses
  Classes, SysUtils, _window, gl;

type
  T_Building = class
    X, Y : integer;
    Width,
    Height : integer;
  end;

  { T_SceneTown }

  T_SceneTown = class(T_Scene)
    procedure InitScene; override;
    procedure DrawScene; override;
  end;

procedure GenerateTown;

implementation

var
  Buildings : TList;
procedure GenerateTown;
var
  xBits : Array of Array of Boolean;
  i, j : integer;
  xX, xY : integer;
  xWidth, xHeight : integer;
  xMaxX,
  xMaxY : integer;
  xMaxBuildWidth,
  xMaxBuildHeight : integer;
  xMinBuildWidth,
  xMinBuildHeight : integer;
  xBuilding : T_Building;
  xPopitka : integer;
begin
  Buildings := TList.Create;
  xMaxX := 800 - 1;
  xMaxY := 600 - 1;
  xMaxBuildWidth := 100;
  xMaxBuildHeight := 100;
  xMinBuildWidth := 20;
  xMinBuildHeight := 20;
  SetLength(xBits, xMaxX + 1, xMaxY + 1);
  xX := -1;
  xY := 0;
  while true do
    begin
      inc(xX);
      if xX > xMaxX then
        begin
          xX := -1;
          xY := xY + 1;
          Continue;
        end;

      if xY > xMaxY then
        break;
      if xBits[xX][xY] then
        Continue;
      xPopitka := 0;
      repeat
      inc(xPopitka);
      if xPopitka = 4 then
        Break;
      xWidth := xMinBuildWidth + random(xMaxBuildWidth - xMinBuildWidth);
      xHeight := xMinBuildHeight + random(xMaxBuildHeight - xMinBuildHeight);
      if (xX + xWidth > xMaxX) or (xY + xHeight > xMaxY) then
        Continue;
      for i := 0 to xWidth do
        for j := 0 to xHeight do
          if xBits[xX + i][xY + j] then
            Continue;
      break;
      until False;
      for i := xx+0 to xx+xWidth do
        for j := xy+0 to xy+xHeight do
          if (i < xMaxX) and (j < xMaxY) then
            xBits[i][j] := True;
      if xPopitka = 4 then
        Continue;
      xBuilding := T_Building.Create;
      xBuilding.X := xX + 1;
      xBuilding.Y := xY + 1;
      xBuilding.Width := xWidth - 5;
      xBuilding.Height:= xHeight - 5;
      Buildings.Add(xBuilding);
      if Buildings.Count = 17 then
        break;
    end;
end;

{ T_SceneTown }

procedure T_SceneTown.InitScene;
begin
  inherited InitScene;
  GenerateTown;
end;

procedure T_SceneTown.DrawScene;
var
  xBuild : T_Building;
  i : integer;
begin
  inherited DrawScene;
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  glOrtho(0, 800, 0, 600, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  glColor4f(1, 1, 1, 1);
  glBegin(GL_QUADS);
  for i := 0 to Buildings.Count -1 do
    begin
      xBuild := T_Building(Buildings[i]);
      with xBuild do
        begin
          glVertex2f(X, Y);
          glVertex2f(X + Width, Y);
          glVertex2f(X + Width, Y + Height);
          glVertex2f(X, Y + Height);
        end;
    end;
  glEnd;
  Window.SwapBuffers;
end;

end.

