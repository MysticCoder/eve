unit _window_linux;

{$mode delphi}

interface

uses
  Classes, SysUtils, _window, _usual, {LCLType,}
  xlib, x, xutil, glx, gl, glext, xatom, keysym, xrandr;

type

  { TS_Window_Linux }

  TS_Window_Linux = class(TS_Window)
  private
    XDisplay: PDisplay;
    delwin: TAtom;
    app_XIM: PXIM;
    app_XIC: PXIC;

  protected
    procedure SetCaption(AValue: string); override;
    procedure SetHeight(AValue: integer); override;
    procedure SetLeft(AValue: integer); override;
    procedure SetTop(AValue: integer); override;
    procedure SetWidth(AValue: integer); override;
    function GetScreenWidth: integer; override;
    function GetScreenHeight: integer; override;
  public
    procedure SwapBuffers; override;
    procedure ProcessEvents; override;
    constructor Create; override;
  end;

implementation

{ TS_Window_Linux }

procedure TS_Window_Linux.SetCaption(AValue: string);
begin
  FCaption := AValue;
  XStoreName(XDisplay, Handle, PChar(AValue));
end;

procedure TS_Window_Linux.SetHeight(AValue: integer);
begin
  if FHeight = AValue then
    Exit;
  inherited;
  XResizeWindow(XDisplay, Handle, FWidth, FHeight);
  glViewport(0, 0, FWidth, FHeight);
end;

procedure TS_Window_Linux.SetLeft(AValue: integer);
begin
  if FLeft = AValue then
    Exit;
  inherited;
  XMoveWindow(XDisplay, Handle, FLeft, FTop);
end;

procedure TS_Window_Linux.SetTop(AValue: integer);
begin
  if FTop = AValue then
    Exit;
  inherited;
  FTop := AValue;
  XMoveWindow(XDisplay, Handle, FLeft, FTop);
end;

procedure TS_Window_Linux.SetWidth(AValue: integer);
begin
  if FWidth = AValue then
    Exit;
  inherited;
  FWidth := AValue;
  XResizeWindow(XDisplay, Handle, FWidth, FHeight);
  glViewport(0, 0, FWidth, FHeight);
end;

function TS_Window_Linux.GetScreenWidth: integer;
begin
  Result := DefaultScreenOfDisplay(XDisplay)^.Width;
end;

function TS_Window_Linux.GetScreenHeight: integer;
begin
  Result := DefaultScreenOfDisplay(XDisplay)^.Height;
end;

procedure TS_Window_Linux.SwapBuffers;
begin
  try
    glXSwapBuffers(XDisplay, Handle);
  except
    writeln;
  end;
end;

procedure TS_Window_Linux.ProcessEvents;
var
  Event: TXEvent;
  UniChar: ansistring;
  xKeySym: TKeySym;
  xStatus: TStatus;
  xMessage: T_Message;
begin
  inherited;
  // VK_0;
  try
    XSync(XDisplay, False);
    while XPending(XDisplay) > 0 do
    begin
      XNextEvent(XDisplay, @Event);
      case Event._type of
        KeyPress:
        begin
          xKeySym := XKeycodeToKeysym(XDisplay, Event.xkey.keycode, 0);
          Keys[xKeySym] := True;
          UniChar := #0#0#0#0#0#0#0#0;
          SetLength(UniChar, Xutf8LookupString(app_XIC, @Event,
            @UniChar[1], 2, @xKeysym, @xStatus));
          xMessage._type := tmt_UniChar;
          //5sdf;
          xMessage.UniChar := UniChar;
          xMessage.KeyCode := xKeySym;
          MainControl.ProcessMessage(xMessage);
          //if Length(UniChar) > 0 then
          //  MainControl.on;
        end;
        KeyRelease:
        begin
          Keys[XKeycodeToKeysym(XDisplay, Event.xkey.keycode, 0)] := False;
        end;
        //                  Keys[XKeycodeToKeysym(XDisplay,Event.xkey.keycode,0)] := True;
{                  UniChar:='';
                  keys[Event.xkey.keycode] := True;
                  SetLength(UniChar,2);
                  SetLength(UniChar,XLookupString(@Event.xkey,@UniChar[1],2,nil,nil));
                  LastKeyCode := Event.xkey.keycode;
                  XLookupString(@Event.xkey,@LastKey,2,nil,nil);
                  Msg._type := CONTROL_KEYDOWN;
                  Msg.VK    := Event.xkey.keycode;
                  Msg.Unicode :=  UTF8Decode(UniChar);
                  ControlManager.PostMessage(Msg);
                end;
        KeyRelease : Keys[Event.xkey.keycode] := False;//Keys[XKeycodeToKeysym(XDisplay,Event.xkey.keycode,0)] := False;}
        MotionNotify:
        begin
          Mouse.dx := Event.xmotion.x - Mouse.X;
          Mouse.dy := Event.xmotion.y - Mouse.Y;
          Mouse.X := Event.xmotion.x;
          Mouse.Y := Event.xmotion.y;
          xMessage._type := tmt_MouseMove;
          xMessage.X := Event.xmotion.x;
          xMessage.Y := Event.xmotion.y;
          MainControl.ProcessMessage(xMessage);
        end;
        ButtonPress:
        begin
          xMessage._type := tmt_MouseDown;
          xMessage.X := Event.xbutton.x;
          xMessage.Y := Event.xbutton.y;
          case Event.xbutton.button of
            Button1:
            begin
              xMessage.MButton := mbLeft;
              Mouse.LMB := True;
            end;
            Button3:
            begin
              xMessage.MButton := mbRight;
              Mouse.RMB := True;
            end;
            Button2:
            begin
              xMessage.MButton := mbMiddle;
              Mouse.MMB := True;
            end;
            Button4:
            begin
              xMessage._type := tmt_MouseWheel;
              xMessage.MouseWheel := 1;
            end;
            Button5:
            begin
              xMessage._type := tmt_MouseWheel;
              xMessage.MouseWheel := 1;
            end;
          end;
          MainControl.ProcessMessage(xMessage);
        end;
        ButtonRelease:
        begin
          xMessage._type := tmt_MouseUp;
          xMessage.X := Event.xbutton.x;
          xMessage.Y := Event.xbutton.y;
          case Event.xbutton.button of
            Button1:
            begin
              xMessage.MButton := mbLeft;
              Mouse.LMB := False;
            end;
            Button3:
            begin
              xMessage.MButton := mbRight;
              Mouse.RMB := False;
            end;
            Button2:
            begin
              xMessage.MButton := mbMiddle;
              Mouse.MMB := False;
            end;
          end;
          MainControl.ProcessMessage(xMessage);
        end;
        ClientMessage:
          if Event.xclient.Data.l[0] = delwin then
            Terminated := True;
      end;
    end;
  except
    writeln;
  end;
end;

constructor TS_Window_Linux.Create;
var
  Desktop: TWindow;
  vi: PXVisualInfo;
  cmap: TColormap;
  //  attr    : array[0..4] of integer = (GLX_RGBA,GLX_DOUBLEBUFFER,GLX_DEPTH_SIZE,24,0);
  attr: array[0..6] of integer = (GLX_RGBA, 1, GLX_DOUBLEBUFFER,
    1, GLX_DEPTH_SIZE, 24, 0);
  swa: TXSetWindowAttributes;
  _state, _wm_del, _fullscreen: TAtom;
  Event: TXEvent;
begin
  inherited Create;
  XDisplay := XOpenDisplay(nil);
  if XDisplay = nil then
  begin
    Log('XOpenDisplay failed');
    Exit;
  end;
  Desktop := DefaultRootWindow(XDisplay);
  if Desktop = 0 then
  begin
    Log('DefaultRootWindow failed');
    Exit;
  end;
  vi := glXChooseVisual(XDisplay, 0, @attr[0]);
  if vi = nil then
  begin
    Log('glXChooseVisual failed');
    Exit;
  end;
  cmap := XCreateColormap(XDisplay, Desktop, @vi^.visual, AllocNone);
  FillChar(swa, SizeOf(swa), 0);
  swa.colormap := cmap;
  swa.border_pixel := 0;
  swa.event_mask := ExposureMask or KeyPressMask or KeyReleaseMask or
    PointerMotionMask or ButtonPressMask or ButtonReleaseMask;
  FWidth := 1;
  FHeight := 1;
  Handle := XCreateWindow(XDisplay, Desktop, 0, 0, FWidth, FHeight,
    0, vi^.depth, InputOutput, vi^.visual, CWBorderPixel or CWColormap or
    CWEventMask, @swa);
  _state := XInternAtom(XDisplay, '_NET_WM_STATE', False);
  _fullscreen := XInternAtom(XDisplay, '_NET_WM_STATE_FULLSCREEN', False);
  XMapWindow(XDisplay, Handle);
  XStoreName(XDisplay, Handle, 'TINY');
  //if bFullScreen then
  //    XChangeProperty(XDisplay, Handle,_state,XA_ATOM,32,PropModeReplace,@_fullscreen,1);

  delwin := XInternAtom(XDisplay, 'WM_DELETE_WINDOW', 0);
  XSetWMProtocols(XDisplay, Handle, @delwin, 1);

  XFlush(XDisplay);

  app_XIM := XOpenIM(XDisplay, nil, nil, nil);
  app_XIC := XCreateIC(app_XIM, [XNInputStyle, XIMPreeditNothing or
    XIMStatusNothing, 0]);

  GLDC := cardinal(glXCreateContext(XDisplay, vi, nil, True));
  if GLDC = 0 then
  begin
    Log('glXCreateContex failed');
    Exit;
  end;
  glXMakeCurrent(XDisplay, Handle, GLXContext(GLDC));
  glTexImage3D := glXGetProcAddress('glTexImage3D');
  if Load_GL_VERSION_2_1() then
    Log('GL version 2 loaded', True)
  else
    Log('GL version 2 load failed', True);
end;

end.
