unit _timer;

{$mode delphi}

interface

uses
  Classes, SysUtils, _usual;

type

  { T_Timer }

  T_Timer = class
    dt : Single; // сколько секунд прошло с прошлого тика
    private
      StartTimer : Int64;
      {$IFDEF WINDOWS}
        TimerFrequency : Int64;
      {$ENDIF}
    public
      constructor Create(AUPS : DWORD);
      function GetTickcount : DWORD;
  end;

  T_Timing = class
    //TimingClass
  end;

procedure _StartTiming(AName : AnsiString);
procedure _EndTiming(AName : AnsiString);

implementation

{ T_Timer }
{$IFDEF LINUX}
  uses baseunix, unix;

constructor T_Timer.Create(AUPS: DWORD);
var
  buf : tms;
  _tv : timeval;
  _tz : timezone;
begin
  FillChar(_tv, SizeOf(_tv), 0);
  FillChar(_tz, SizeOf(_tz), 0);
  fpgettimeofday(@_tv, @_tz);
  StartTimer := _tv.tv_sec * 1000 + (_tv.tv_usec div 1000);
  dt := 1 / AUPS;
end;

{$ENDIF}
{$IFDEF WINDOWS}
  uses windows;
constructor T_Timer.Create;
begin
  QueryPerformanceFrequency(TimerFrequency);
  QueryPerformanceCounter(StartTimer);
  dt := 1 / AUPS;
end;
{$ENDIF}

function T_Timer.GetTickcount: DWORD;
{$IFDEF LINUX}
var
  buf : tms;
  _tv : timeval;
  _tz : timezone;
begin
  FillChar(_tv, SizeOf(_tv), 0);
  FillChar(_tz, SizeOf(_tz), 0);
  fpgettimeofday(@_tv, @_tz);
  Result := _tv.tv_sec * 1000 + (_tv.tv_usec div 1000) - StartTimer;
end;
{$ENDIF}
{$IFDEF WINDOWS}
var
  t : TLargeInteger;
begin
  QueryPerformanceCounter(t);
  Result := Round(1000 * ((t - StartTimer) / TimerFrequency));
end;
{$ENDIF}

procedure _StartTiming(AName : AnsiString);
begin

end;

procedure _EndTiming(AName : AnsiString);
begin

end;

end.

