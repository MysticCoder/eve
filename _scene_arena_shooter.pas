unit _scene_arena_shooter;

{$mode delphi}

interface

uses
  Classes, SysUtils, _window, _usual, gl, glu, Math;

type
  R_UnitPos = RVector2f;

  { T_Unit }
  T_UnitController = class;
  T_Unit = class;

  { T_KeyPoint }

  T_KeyPoint = class
    ID: integer;
    TagFrom : T_KeyPoint;
    TagDistance : Single;
    Pos: RVector2f;
    VisiblePoints: TList; // of T_KeyPoint
    PointsDistances: TList; // of PSingle
    constructor Create;
    procedure CheckVisiblePoints;
    function Distances(AVisiblePointIndex: integer): single;
    class function FindOptimalWayToPoint(AFrom: RVector2f; ATo: RVector2f;
      var AMoveTo: RVector2f): boolean;
    destructor Destroy; override;
  end;

  { T_Bonus }

  T_Bonus = class
    BonusType: byte;
    Pos: R_UnitPos;
    Cooldawn: single;
    CooldawnTimer: single;
    procedure Activate(AUnit: T_Unit);
    function IsActive : boolean;
    procedure Update;
    procedure Draw;
  end;

  { T_WeaponShot }

  T_WeaponShot = class
    Host : T_Unit;
    Pos: R_UnitPos;
    Dmg: single;
    Speed: single;
    Angle: single;
    constructor Create(AHost : T_Unit);
    procedure Update;
    procedure Draw;
    procedure Die;
  end;

  T_ShootClass = class of T_WeaponShot;

  { T_Weapon }

  T_Weapon = class
    Host : T_Unit;
    Dmg: single;
    ShootSpeed: single;
    Accuracy: single;
    Cooldawn: single;
    CooldawnTimer: single;
    ShootClass: T_ShootClass;
    constructor Create(AHost : T_Unit);
    procedure Update;
    procedure Shoot(AX, AY, ADistance, AAngle: single);
  end;

  T_Unit = class
    CurrentWeapon: T_Weapon;
    Weapons: TList; // of T_Weapon
    Pos: R_UnitPos;
    MaxLife: single;
    Life: single;
    Armor: single;
    MaxArmor: single;
    Speed: single;
    FOVAngle: single; // угол обзора
    ShootAngle: single; // направление взгляда
    // движение
    MoveAngle: single;
    DoMove: boolean;
    wasd: array[1..4] of boolean;
    Controller: T_UnitController;
    constructor Create;
    procedure Draw; virtual;
    procedure DrawLookSector;
    procedure DrawLookSector2;
    procedure Update; virtual;
    procedure CheckMove;
    procedure ReceiveDamage(ADmg : Single; AFrom : T_Unit);
    procedure Die(AFrom : T_Unit);
  end;

  { T_UnitController }

  T_UnitController = class
    GameUnit: T_Unit;
    VisibleUnits: TList;
    constructor Create;
    procedure Update; virtual;
    procedure CheckVisible;
  end;

  { T_PlayerUnitController }

  T_PlayerUnitController = class(T_UnitController)
    procedure Update; override;
  end;

  { T_CompUnitController }

  T_CompUnitController = class(T_UnitController)
    MoveTo : R_UnitPos;
    LookTo : Single;
    PriorityTImer : Single;
    procedure CheckPriority;
    procedure CheckMove;
    procedure Update; override;
  end;

  T_MapCell = record
    isPassable: boolean;
    SpriteType: byte;
  end;

  { T_Map }

  T_Map = class
    Width, Height: integer;
    CellWidth, CellHeight: integer;
    Cells: array of array of T_MapCell;
    function IsPosVisible(AFrom: R_UnitPos; APos: R_UnitPos;
      ALookAngle: single; AFOVAngle: single; AMaxDistance : PSingle = nil): boolean;
    function IsCorrectPlace(APos: R_UnitPos): boolean;
    constructor Create(AWidth, AHeight: integer);
    procedure GenSimpleMap;
    procedure Draw;
  end;

  { T_Scene_Deep }

  { T_Scene_Arena_Shooter }

  T_Scene_Arena_Shooter = class(T_Scene)
    Units: TList; // of T_Unit;
    Map: T_Map;
    Shoots: TList; // of T_WeaponShot
    Bonuses: TList; // of T_Bonus
    KeyPoints: TList; // of T_KeyPoint
    Player : T_Unit;
    OffsetX,
    OffsetY,
    Scale : Single;
    procedure InitScene; override;
    procedure UpdateScene; override;
    procedure DrawScene; override;
    procedure DrawKeyPoints;
    function GetRealXY(AMouseX, AMouseY: Single): RVector2f;
    function MouseToMap: RVector2f;
  end;

const
  BONUS_HEALTH = 0;
  BONUS_ARMOR = 1;

var
  Scene: T_Scene_Arena_Shooter;

implementation

uses
  _keys;

{ T_KeyPoint }

constructor T_KeyPoint.Create;
begin
  VisiblePoints := TList.Create;
  PointsDistances := TList.Create;
end;

procedure T_KeyPoint.CheckVisiblePoints;
var
  i: integer;
  xKeyPoint: T_KeyPoint;
  xSingle: PSingle;
begin
  for i := 0 to Scene.KeyPoints.Count - 1 do
  begin
    xKeyPoint := Scene.KeyPoints[i];
    if xKeyPoint = Self then
      Continue;
    if Scene.Map.IsPosVisible(Pos, xKeyPoint.Pos, 0, 2 * Pi) then
    begin
      VisiblePoints.Add(xKeyPoint);
      New(xSingle);
      xSingle^ := GetDistance(Pos, xKeyPoint.Pos);
      PointsDistances.Add(xSingle);
    end;
  end;
end;

function T_KeyPoint.Distances(AVisiblePointIndex: integer): single;
begin
  Result := PSingle(VisiblePoints[AVisiblePointIndex])^;
end;

class function T_KeyPoint.FindOptimalWayToPoint(AFrom: RVector2f; ATo: RVector2f; var AMoveTo : RVector2f): boolean;
var
  i : integer;
  j : integer;
  xKeyPoint : T_KeyPoint;
  xKeyPoint2 : T_KeyPoint;
  xDistance : SIngle;
  xFound : Boolean;
begin
  Result := False;
  if Scene.Map.IsPosVisible(AFrom, ATo, 0, 2 * Pi) then
    begin
      AMoveTo := ATo;
      Result := True;
      Exit;
    end;

  for i := 0 to Scene.KeyPoints.Count - 1 do
    begin
      T_KeyPoint(Scene.KeyPoints[i]).TagDistance := -1;
      T_KeyPoint(Scene.KeyPoints[i]).TagFrom := nil;
    end;
  xFound := True;
  while xFound do
    begin
      xFound := False;
      for i := 0 to Scene.KeyPoints.Count - 1 do
        begin
          xKeyPoint := Scene.KeyPoints[i];
          if (xKeyPoint.TagDistance = -1) and Scene.Map.IsPosVisible(AFrom, xKeyPoint.Pos, 0, 2 * Pi) and (GetDistance(AFrom, xKeyPoint.Pos)>1e-3) then
            begin
              xKeyPoint.TagDistance := GetDistance(AFrom, xKeyPoint.Pos);
              xFound := True;
            end;
          if xKeyPoint.TagDistance <> -1 then
            for j := 0 to xKeyPoint.VisiblePoints.Count - 1 do
              begin
                xKeyPoint2 := xKeyPoint.VisiblePoints[j];
                xDistance := xKeyPoint.Distances(j);
                if ((xKeyPoint2.TagDistance = -1) or (xKeyPoint2.TagDistance > xKeyPoint.TagDistance + xDistance)) then
                  if xKeyPoint2.TagFrom <> nil then
                  begin
                    xKeyPoint2.TagDistance := xKeyPoint.TagDistance + xDistance;
                    xKeyPoint2.TagFrom := xKeyPoint;
                    xFOund := True;
                  end;
              end;
            end;
        end;
  xDistance := 9999999;
  xKeyPoint2 := nil;
  for i := 0 to Scene.KeyPoints.Count - 1 do
    begin
      xKeyPoint := Scene.KeyPoints[i];
      if (xKeyPoint.TagDistance <> -1) and (Scene.Map.IsPosVisible(ATo, xKeyPoint.Pos, 0, 2 * Pi)) then
        begin
          if xDistance > xKeyPoint.TagDistance + GetDistance(xKeyPoint.Pos, ATo) then
            begin
              xDistance := xKeyPoint.TagDistance + GetDistance(xKeyPoint.Pos, ATo);
              xKeyPoint2 := xKeyPoint;
            end;
        end;
    end;
  if xKeyPoint2 = nil then
    Exit;
  Result := True;
  while xKeyPoint2.TagFrom <> nil do
    xKeyPoint2 := xKeyPoint2.TagFrom;
  AMoveTo[0] := xKeyPoint2.Pos[0];
  AMoveTo[1] := xKeyPoint2.Pos[1];
end;

destructor T_KeyPoint.Destroy;
begin
  inherited Destroy;
end;

{ T_Bonus }

procedure T_Bonus.Activate(AUnit: T_Unit);
begin
  case BonusType of
    BONUS_HEALTH:
      AUnit.Life := Min(AUnit.Life + 25, AUnit.MaxLife);
    BONUS_Armor:
      AUnit.Armor := Min(AUnit.Armor + 25, AUnit.MaxArmor);
  end;
  CooldawnTimer := 0;
end;

function T_Bonus.IsActive: boolean;
begin
  Result := CooldawnTimer >= Cooldawn;
end;

procedure T_Bonus.Update;
var
  i: integer;
begin
  CooldawnTimer := CooldawnTimer + Scene.dt;
  CooldawnTimer := Min(CooldawnTimer, Cooldawn);
  if CooldawnTimer < Cooldawn then
    Exit;

  for i := 0 to Scene.Units.Count - 1 do
  begin
    if GetDistance(Pos, T_Unit(Scene.Units[i]).Pos) < 0.5 then
    begin
      Activate(Scene.Units[i]);
      Exit;
    end;
  end;
end;

procedure T_Bonus.Draw;
var
  q: PGLUquadric;
begin
  if CooldawnTimer < Cooldawn then
    Exit;
  q := gluNewQuadric;
  glTranslatef(Pos[0], Pos[1], 0);
  case BonusType of
    BONUS_HEALTH:
      glColor3f(0, 1, 1);
    BONUS_ARMOR:
      glColor3f(0, 0, 1);
  end;
  gluSphere(q, 0.3, 4, 4);
  glTranslatef(-Pos[0], -Pos[1], 0);
  gluDeleteQuadric(q);
  glColor3f(1, 1, 1);
end;

{ T_CompUnitController }




procedure T_CompUnitController.CheckPriority;
var
  xLife : Single;
  xArmor : Single;
  xPoints : array of R_UnitPos;
  xPrioritys : array of Single;
  xCount : integer;
  xBonus : T_Bonus;
  xMaxPriority : Single;
  xMaxPoint : R_UnitPos;
  i : integer;
  xFlag : boolean;
  xSortPoint : R_UnitPos;
  xSortPrior : Single;
begin
  PriorityTImer := 1;
  xCount := 0;
  SetLength(xPoints, 200);
  SetLength(xPrioritys, 200);
  xLife := (1 - (GameUnit.Life/GameUnit.MaxLife)) * 1;
  xArmor := 0;
  if GameUnit.MaxArmor > 0 then
    xArmor := (1 - (GameUnit.Armor/GameUnit.MaxArmor)) * 0.5;
  for i := 0 to Scene.Bonuses.Count - 1 do
    begin
      xBonus := Scene.Bonuses[i];
      if xBonus.IsActive then
        begin
          inc(xCount);
          xPoints[xCount - 1][0] := xBonus.Pos[0];
          xPoints[xCount - 1][1] := xBonus.Pos[1];
          Case xBonus.BonusType of
            BONUS_HEALTH:
              xPrioritys[xCount - 1] := xLife;
            BONUS_ARMOR:
              xPrioritys[xCount - 1] := xArmor;
          end;
        end;
    end;
  for i := 0 to xCount - 1 do
    xPrioritys[i] := xPrioritys[i] * (1 - (GetDistance(GameUnit.Pos, xPoints[i]) / 200));

  xFlag := True;
  while xFlag do
    begin
      xFlag := False;
      for i := 0 to xCount - 2 do
        if xPrioritys[i] > xPrioritys[i + 1] then
          begin
            xSortPoint[0] := xPoints[i][0];
            xSortPoint[1] := xPoints[i][1];
            xSortPrior := xPrioritys[i];
            xPoints[i][0] := xPoints[i + 1][0];
            xPoints[i][1] := xPoints[i + 1][1];
            xPrioritys[i] := xPrioritys[i + 1];
            xPoints[i + 1][0] := xSortPoint[0];
            xPoints[i + 1][1] := xSortPoint[1];
            xPrioritys[i + 1] := xSortPrior;
          end;
    end;
  for i := xCount - 1 downto 0 do
    if T_KeyPoint.FindOptimalWayToPoint(GameUnit.Pos, xPoints[i], MoveTo) then
      begin
        break;

      end;
//  MoveTo := xMaxPoint;
end;

procedure T_CompUnitController.CheckMove;
var
  xdt : Single;
begin
  GameUnit.wasd[4] := False;
  GameUnit.wasd[3] := False;
  GameUnit.wasd[2] := False;
  GameUnit.wasd[1] := False;

  xdt := 2 * GameUnit.Speed * Scene.dt;
  if GetDistance(MoveTo, GameUnit.Pos) < xdt then
    begin
      GameUnit.Pos := MoveTo;
      CheckPriority;
      Exit;
    end;
  xdt := 0;
  GameUnit.wasd[4] := MoveTo[0] > GameUnit.Pos[0] + xdt;
  GameUnit.wasd[3] := MoveTo[1] + xdt < GameUnit.Pos[1];
  GameUnit.wasd[2] := MoveTo[0] + xdt < GameUnit.Pos[0];
  GameUnit.wasd[1] := MoveTo[1] > GameUnit.Pos[1] + xdt;
end;

procedure T_CompUnitController.Update;
var
  i: integer;
  xTargetUnit: T_Unit;
  xNext : T_KeyPoint;
  xWay : TList;
  xTo : RVector2f;
  xMax : Single;
  xMoveTo : RVector2f;
begin
  inherited Update;
  if VisibleUnits.Count > 0 then
  begin
    xTargetUnit := VisibleUnits[0];
    LookTo :=  AngleToPiRange(arctan2(xTargetUnit.Pos[1] - GameUnit.Pos[1], xTargetUnit.Pos[0] - GameUnit.Pos[0]));
    if Abs(GetMinAngle(GameUnit.ShootAngle, LookTo)) > 5 * Pi/180 then
      GameUnit.ShootAngle := GameUnit.ShootAngle + Sign(GetMinAngle(GameUnit.ShootAngle, LookTo)) * 80 * Pi / 180 * Scene.dt
    else
      GameUnit.CurrentWeapon.Shoot(GameUnit.Pos[0], GameUnit.Pos[1],
        1, GameUnit.ShootAngle);
  end
    else
      begin
        if Abs(GetMinAngle(GameUnit.ShootAngle, LookTo)) > 5 * Pi/180 then
          GameUnit.ShootAngle := GameUnit.ShootAngle + Sign(GetMinAngle(GameUnit.ShootAngle, LookTo)) * 80 * Pi / 180 * Scene.dt
        else
          LookTo :=  Pi - Random * 2 * Pi;
      end;
  //GameUnit.Life := GameUnit.MaxLife / 2;
  if PriorityTImer <= 0 then
    CheckPriority;
  PriorityTImer := PriorityTImer - Scene.dt;
  CheckMove;
  xWay := TList.Create;
  xTo := T_Unit(Scene.Units[0]).Pos;
  xNext := nil;
  xMax := -1;
  xMoveTo[0] := 0;
  xMoveTo[1] := 0;
{  if T_KeyPoint.FindOptimalWayToPoint(GameUnit.Pos, xTo, xMoveTo) then
//  if xNext <> nil then
    begin
      GameUnit.ShootAngle := AngleToPiRange(arctan2(xMoveTo[1] -
        GameUnit.Pos[1], xMoveTo[0] - GameUnit.Pos[0]));
      GameUnit.CurrentWeapon.Shoot(GameUnit.Pos[0], GameUnit.Pos[1],
        1, GameUnit.ShootAngle);
    end;}
  xWay.Free;
end;

{ T_WeaponShot }

constructor T_WeaponShot.Create(AHost: T_Unit);
begin
  Host := AHost;
end;

procedure T_WeaponShot.Update;
var
  xPos: R_UnitPos;
  i: integer;
  xCount: integer;
  xDx: single;
  xDy: single;
  j : integer;

  xUnit : T_Unit;
begin
  xCount := 10;
  xDx := cos(Angle) * Speed * Scene.dt / xCount;
  xDy := sin(Angle) * Speed * Scene.dt / xCount;
  for i := 1 to xCount do
  begin
    xPos[0] := Pos[0] + xDx;
    xPos[1] := Pos[1] + xDy;
    for j := 0 to Scene.Units.Count - 1 do
      begin
        xUnit := Scene.Units[j];
        if GetDistance(xPos, xUnit.Pos) < 0.2 then
          begin
            xUnit.ReceiveDamage(Dmg, Host);
            Die;
            Exit;
          end;
      end;
    if Scene.Map.IsCorrectPlace(xPos) then
      Pos := xPos
    else
      begin
        Die;
        Exit;
      end;
  end;
end;

procedure T_WeaponShot.Draw;
var
  q: PGLUquadric;
begin
  glColor3f(1, 1, 1);
  q := gluNewQuadric;
  glTranslatef(Pos[0], Pos[1], 0);
  gluSphere(q, 0.1, 4, 4);
  glTranslatef(-Pos[0], -Pos[1], -0);
  gluDeleteQuadric(q);
end;

procedure T_WeaponShot.Die;
begin
  Scene.Shoots.Remove(Self);
  Free;
end;

{ T_Weapon }

constructor T_Weapon.Create(AHost: T_Unit);
begin
  Host := AHost;
end;

procedure T_Weapon.Update;
begin
  CooldawnTimer := CooldawnTimer + Scene.dt;
  if CooldawnTimer > Cooldawn then
    CooldawnTimer := Cooldawn;
end;

procedure T_Weapon.Shoot(AX, AY, ADistance, AAngle: single);
var
  xShoot: T_WeaponShot;
begin
  if CooldawnTimer < Cooldawn then
    Exit;
  xShoot := ShootClass.Create(Host);
  Scene.Shoots.Add(xShoot);
  AAngle := AAngle - Accuracy + Random * 2 * Accuracy;
  xShoot.Pos[0] := AX + cos(AAngle) * ADistance;
  xShoot.Pos[1] := AY + sin(AAngle) * ADistance;
  xShoot.Angle := AAngle;
  xShoot.Dmg := Dmg;
  xShoot.Speed := ShootSpeed;
  CooldawnTimer := 0;
end;

{ T_UnitController }

constructor T_UnitController.Create;
begin
  VisibleUnits := TList.Create;
end;

procedure T_UnitController.Update;
begin
  CheckVisible;
end;

procedure T_UnitController.CheckVisible;
var
  i: integer;
  xUnit: T_Unit;
  xUnit2: T_Unit;
  xDist: single;
  xSortFlag: boolean;
begin
  VisibleUnits.Clear;
  for i := 0 to Scene.Units.Count - 1 do
  begin
    xUnit := Scene.Units[i];
    if xUnit <> GameUnit then
    begin
      if Scene.Map.IsPosVisible(GameUnit.Pos, xUnit.Pos,
        GameUnit.ShootAngle, GameUnit.FOVAngle) then
        VisibleUnits.Add(xUnit);
    end;
  end;
  xSortFlag := True;
  while xSortFlag do
  begin
    xSortFlag := False;
    for i := 0 to VisibleUnits.Count - 2 do
    begin
      xUnit := VisibleUnits[i];
      xUnit2 := VisibleUnits[i + 1];
      xDist := GetDistance(GameUnit.Pos, xUnit.Pos) -
        GetDistance(GameUnit.Pos, xUnit2.Pos);
      if xDist > 0 then
      begin
        VisibleUnits[i] := xUnit2;
        VisibleUnits[i + 1] := xUnit;
        xSortFlag := True;
      end;
    end;

  end;
end;

{ T_PlayerUnitController }

procedure T_PlayerUnitController.Update;
var
  xMousePos: RVector2f;
begin
  inherited;
  with GameUnit do
  begin
    wasd[1] := Window.Keys[MK_W];
    wasd[2] := Window.Keys[MK_A];
    wasd[3] := Window.Keys[MK_S];
    wasd[4] := Window.Keys[MK_D];
    xMousePos := Scene.MouseToMap;
    ShootAngle := arctan2(xMousePos[1] - Pos[1], xMousePos[0] - Pos[0]);
    if Window.Mouse.LMB then
    begin
      CurrentWeapon.Shoot(Pos[0], Pos[1], 1, ShootAngle);
    end;
  end;
end;

{ T_Unit }

constructor T_Unit.Create;
begin
  Weapons := TList.Create;
  Speed := 3;
  Controller := T_PlayerUnitController.Create;
  Controller.GameUnit := Self;
end;

procedure T_Unit.Draw;
var
  q: PGLUquadric;
  xColor : Single;
begin
  xColor := Life/MaxLife;
  glColor3f(xColor, xColor, xColor);
  glTranslatef(Pos[0], Pos[1], 0);

  q := gluNewQuadric;
  gluSphere(q, 0.3, 5, 5);
  gluDeleteQuadric(q);

  glTranslatef(-Pos[0], -Pos[1], 0);
  //DrawLookSector;
  glColor3f(1, 1, 1);
end;

procedure T_Unit.DrawLookSector;
begin
  glBegin(GL_LINES);
    glVertex2f(Pos[0], Pos[1]);
    glVertex2f(Pos[0] + 5 * cos(ShootAngle + FOVAngle), Pos[1] + 5 * sin(ShootAngle + FOVAngle));
    glVertex2f(Pos[0], Pos[1]);
    glVertex2f(Pos[0] + 5 * cos(ShootAngle - FOVAngle), Pos[1] + 5 * sin(ShootAngle - FOVAngle));
  glEnd;
end;

procedure T_Unit.DrawLookSector2;
var
  i : integer;
  xCount : integer;
  xAngle : SIngle;
  xMaxDistance : Single;
  xPos : R_UnitPos;
begin
  xCount := 300;
  glColor4f(1, 1, 1, 1);
  glBegin(GL_TRIANGLE_FAN);
    glVertex3f(Pos[0], Pos[1], 0);
    for i := 0 to xCount - 1 do
      begin
        xPos[0] := Pos[0] + 40 * cos(xAngle);
        xPos[1] := Pos[1] + 40 * sin(xAngle);
        xAngle := ShootAngle - FOVAngle + (i/(xCount-1)) * FOVAngle * 2;
        Scene.Map.IsPosVisible(Pos, xPos, xAngle, FOVAngle, @xMaxDistance);
        glVertex3f(Pos[0] + xMaxDistance * cos(xAngle), Pos[1] + xMaxDistance * sin(xAngle), 0);
      end;
  glEnd;
end;

procedure T_Unit.Update;
var
  xPos: R_UnitPos;
  xPos2: R_UnitPos;
  xPos3: R_UnitPos;
begin
  Controller.Update;
  CheckMove;
  if DoMove then
  begin
    xPos[0] := Pos[0] + cos(MoveAngle) * Speed * Scene.dt;
    xPos[1] := Pos[1] + sin(MoveAngle) * Speed * Scene.dt;
    xPos2[0] := Pos[0];
    xPos2[1] := xPos[1];
    xPos3[0] := xPos[0];
    xPos3[1] := Pos[1];
    if Scene.Map.IsCorrectPlace(xPos) then
      Pos := xPos
    else
      if Scene.Map.IsCorrectPlace(xPos2) then
        Pos := xPos2
      else
        if Scene.Map.IsCorrectPlace(xPos3) then
          Pos := xPos3;
  end;
  if CurrentWeapon <> nil then
    CurrentWeapon.Update;
end;

procedure T_Unit.CheckMove;
var
  xAngle: single;
  xIndex: integer;

  procedure _AddAngle(AAngle: single; var AIndex: integer);
  begin
    Inc(AIndex);
    if (Abs(Abs(AAngle - xAngle) - Pi) < 1e-3) and (AIndex > 1) then
    begin
      xAngle := 0;
      AIndex := 0;
      Exit;
    end;
    if Abs(AAngle - xAngle) > Pi then
      xAngle := xAngle + AAngle + 2 * Pi
    else
      xAngle := xAngle + AAngle;
    if xIndex > 1 then
      xAngle := xAngle / 2;
  end;

begin
  DoMove := False;
  xAngle := 0;
  xIndex := 0;
  if wasd[1] then
    _AddAngle(Pi / 2, xIndex);
  if wasd[2] then
    _AddAngle(Pi, xIndex);
  if wasd[3] then
    _AddAngle(3 * Pi / 2, xIndex);
  if wasd[4] then
    _AddAngle(2 * Pi, xIndex);
  if xIndex > 0 then
  begin
    DoMove := True;
    MoveAngle := xAngle;
  end;
end;

procedure T_Unit.ReceiveDamage(ADmg: Single; AFrom: T_Unit);
var
  xDmgArmor : Single;
  xLifeDmg : Single;
begin
  xDmgArmor := Min(ADmg, Armor);
  xLifeDmg := (xDmgArmor / 2) + (ADmg - xDmgArmor);
  Armor := Armor - xDmgArmor;
  Life := Life - xLifeDmg;
  if Life <= 0 then
    Die(AFrom);
end;

procedure T_Unit.Die(AFrom: T_Unit);
begin
  Life := MaxLife;
  repeat
    Pos[0] := Random(Scene.Map.Width);
    Pos[1] := Random(Scene.Map.Height);
  until Scene.Map.IsCorrectPlace(Pos);
end;


{ T_Scene_Deep }

procedure T_Scene_Arena_Shooter.InitScene;
var
  xUnit: T_Unit;
  xBonus: T_Bonus;
  i: integer;
  x, y : integer;
begin
  inherited InitScene;
  Scene := Self;
  KeyPoints := TList.Create;
  Map := T_Map.Create(40, 30);
  Map.GenSimpleMap;
  Shoots := TList.Create;
  Units := TList.Create;
  Bonuses := TList.Create;

  for i := 0 to KeyPoints.Count - 1 do
    T_KeyPoint(KeyPoints[i]).CheckVisiblePoints;
  for i := 0 to 10 do
  begin
    xBonus := T_Bonus.Create;
    xBonus.BonusType := Random(2);
    xBonus.Cooldawn := 10;
    repeat
      xBonus.Pos[0] := Random(Map.Width);
      xBonus.Pos[1] := Random(Map.Height);
    until Scene.Map.IsCorrectPlace(xBonus.Pos);
    Bonuses.Add(xBonus);
  end;

  xUnit := T_Unit(Units[Units.Add(T_Unit.Create)]);
  Player := xUnit;
  with xUnit do
  begin
    repeat
      Pos[0] := Random(Map.Width);
      Pos[1] := Random(Map.Height);
    until Scene.Map.IsCorrectPlace(Pos);
    Life := 100;
    MaxLife := 100;
    FOVAngle := 45 * Pi / 180;
    CurrentWeapon := T_Weapon.Create(xUnit);
    CurrentWeapon.Dmg := 10;
    CurrentWeapon.Cooldawn := 0.5;
    CurrentWeapon.ShootSpeed := 50;
    CurrentWeapon.ShootClass := T_WeaponShot;
    CurrentWeapon.Accuracy := 5 * Pi / 180;
  end;

  for i := 1 to 5 do
    begin
      xUnit := T_Unit(Units[Units.Add(T_Unit.Create)]);
      with xUnit do
      begin
        repeat
          Pos[0] := Random(Map.Width);
          Pos[1] := Random(Map.Height);
        until Scene.Map.IsCorrectPlace(Pos);
        Life := 100;
        MaxLife := 100;
        Armor := 0;
        MaxArmor := 100;
        CurrentWeapon := T_Weapon.Create(xUnit);
        CurrentWeapon.Dmg := 10;
        CurrentWeapon.Cooldawn := 0.5;
        CurrentWeapon.ShootSpeed := 50;
        CurrentWeapon.ShootClass := T_WeaponShot;
        CurrentWeapon.Accuracy := 5 * Pi / 180;
        FOVAngle := 45 * Pi / 180;
        Controller := T_CompUnitController.Create;
        Controller.GameUnit := xUnit;
        T_CompUnitController(Controller).MoveTo := Pos;
      end;

    end;
end;

procedure T_Scene_Arena_Shooter.UpdateScene;
var
  i: integer;
begin
  inherited UpdateScene;

  for i := Units.Count - 1 downto 0 do
    T_Unit(Units[i]).Update;

  for i := Shoots.Count - 1 downto 0  do
    T_WeaponShot(Shoots[i]).Update;

  for i := Bonuses.Count - 1  downto 0 do
    T_Bonus(Bonuses[i]).Update;
end;

procedure T_Scene_Arena_Shooter.DrawScene;
var
  i: integer;
begin
  inherited DrawScene;
  //glEnable(GL_ALPHA_TEST);
  //glAlphaFunc(GL_ALWAYS, 1);
  glClearColor(0, 0, 0, 1);
  glClearDepth(1.0);
  glDepthFunc(GL_LESS);
  glDepthRange(0, 1);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  Scale := 0.05;
  OffsetX := Player.Pos[0];
  OffsetY := Player.Pos[1];
  glOrtho(-Window.Width * Scale / 2 + OffsetX, Window.Width * Scale/ 2 + OffsetX, -Window.Height * Scale/ 2 + OffsetY, Window.Height * Scale/ 2 + OffsetY, -20, 20);
//  glOrtho(0, Map.Width, 0, Map.Height, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  if Player <> nil then
    begin
      glDisable(GL_BLEND);
      Player.DrawLookSector2;
      //GLble;
      glBlendFunc(GL_DST_COLOR, GL_ZERO);
      glEnable(GL_BLEND);
      //glEnable(GL_ALPHA_TEST);
    end;
  //glDisable(GL_DEPTH_TEST);
  Map.Draw;
  glDisable(GL_BLEND);
  //Player.DrawLookSector2;
  if Player <>  nil then
    begin
      Player.Draw;
      for i := 0 to Player.Controller.VisibleUnits.Count - 1 do
        T_Unit(Player.Controller.VisibleUnits[i]).Draw;
    end
  else
    begin
      for i := 0 to Units.Count - 1 do
        T_Unit(Units[i]).Draw;
    end;

  for i := 0 to Shoots.Count - 1 do
    begin
      if Map.IsPosVisible(Player.Pos, T_WeaponShot(Shoots[i]).Pos, Player.ShootAngle, Player.FOVAngle) then
        T_WeaponShot(Shoots[i]).Draw;
    end;
  for i := 0 to Bonuses.Count - 1 do
    T_Bonus(Bonuses[i]).Draw;
  //DrawKeyPoints;
end;

procedure T_Scene_Arena_Shooter.DrawKeyPoints;
var
  q : PGLUquadric;
  i : integer;
  xKey : T_KeyPoint;
begin
  glColor3f(0.5, 0.8, 0.2);
  q := gluNewQuadric;
  for i := 0 to KeyPoints.Count - 1 do
    begin
      xKey := KeyPoints[i];
      glTranslatef(xKey.Pos[0], xKey.Pos[1], 0);
      gluSphere(q, 0.2, 6, 6);
      glTranslatef(-xKey.Pos[0], -xKey.Pos[1], 0);
    end;
  gluDeleteQuadric(q);
  glColor3f(1, 1, 1);
end;

function T_Scene_Arena_Shooter.GetRealXY(AMouseX, AMouseY: Single): RVector2f;
begin
  Result[0] := ((AMouseX - Window.Width / 2) * Scale + OffsetX);
  Result[1] := ({Window.Height - }(AMouseY - Window.Height / 2) * Scale+ OffsetY);
end;

function T_Scene_Arena_Shooter.MouseToMap: RVector2f;
begin
  Result := GetRealXY(Window.Mouse.X, Window.Height - Window.Mouse.Y);
  //Result[1] := (Window.Height - Window.Mouse.Y) * Scene.Map.Height / Window.Height;
  //Result[0] := Window.Mouse.X * Scene.Map.Width / Window.Width;
end;

{ T_Map }

function T_Map.IsPosVisible(AFrom: R_UnitPos; APos: R_UnitPos;
  ALookAngle: single; AFOVAngle: single; AMaxDistance: PSingle): boolean;
var
  i: integer;
  xCount: integer;
  xPos: R_UnitPos;
  xAngle: single;
begin
  xAngle := arctan2(APos[1] - AFrom[1], APos[0] - AFrom[0]);
  xAngle := AngleToPiRange(xAngle);
  if Abs(GetMinAngle(ALookAngle, xAngle)) > AFOVAngle then
  begin
    if AMaxDistance <> nil then
      AMaxDistance^ := 0;
    Result := False;
    Exit;
  end;

  Result := True;
  xCount := 400;
  for i := 1 to xCount do
  begin
    xPos[0] := AFrom[0] + (APos[0] - AFrom[0]) * i / xCount;
    xPos[1] := AFrom[1] + (APos[1] - AFrom[1]) * i / xCount;
    if not IsCorrectPlace(xPos) then
    begin
      if AMaxDistance <> nil then
        AMaxDistance^ := GetDistance(AFrom, xPos);
      Result := False;
      Exit;
    end;
  end;
  if AMaxDistance <> nil then
    AMaxDistance^ := GetDistance(AFrom, APos);
end;


function T_Map.IsCorrectPlace(APos: R_UnitPos): boolean;
begin
  Result := False;
  if (APos[0] < 0) or (APos[1] < 0) then
    Exit;
  if (APos[0] > Width) or (APos[1] > Height) then
    Exit;
  if Scene.Map.Cells[trunc(APos[0])][trunc(APos[1])].isPassable then
    Result := True
  else
    Result := False;
end;

constructor T_Map.Create(AWidth, AHeight: integer);
var
  i: integer;
begin
  Width := AWidth;
  Height := AHeight;
  CellWidth := 1;
  CellHeight := 1;
  SetLength(Cells, Width, Height);
  for i := 0 to Width - 1 do
    FillChar(Cells[i][0], SizeOf(Cells[i][0]) * Length(Cells[i]), 0);
end;

procedure T_Map.GenSimpleMap;
var
  i, j: integer;
  m, n: integer;
  x, y : integer;
  xKeyPoint : T_KeyPoint;
begin
  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
      Cells[i][j].isPassable := True;
  for i := 0 to Width - 1 do
  begin
    Cells[i][0].isPassable := False;
    Cells[i][Height - 1].isPassable := False;
  end;
  for i := 0 to Height - 1 do
  begin
    Cells[0][i].isPassable := False;
    Cells[Width - 1][i].isPassable := False;
  end;

  for i := 1 to 40 do
    begin
      x := Random(Width - 4);
      y := Random(Height - 4);
      for m := 0 to 1 do
        for n := 0 to 1 do
          Cells[x + m][y + n].isPassable := False;
      xKeyPoint := T_KeyPoint.Create;
      xKeyPoint.Pos[0] := x - 0.2;
      xKeyPoint.Pos[1] := y - 0.2;
      if IsCorrectPlace(xKeyPoint.Pos) then
        Scene.KeyPoints.Add(xKeyPoint)
      else
        xKeyPoint.Free;

      xKeyPoint := T_KeyPoint.Create;
      xKeyPoint.Pos[0] := x + 2.2;
      xKeyPoint.Pos[1] := y - 0.2;
      if IsCorrectPlace(xKeyPoint.Pos) then
        Scene.KeyPoints.Add(xKeyPoint)
      else
        xKeyPoint.Free;

      xKeyPoint := T_KeyPoint.Create;
      xKeyPoint.Pos[0] := x + 2.2;
      xKeyPoint.Pos[1] := y + 2.2;
      if IsCorrectPlace(xKeyPoint.Pos) then
        Scene.KeyPoints.Add(xKeyPoint)
      else
        xKeyPoint.Free;

      xKeyPoint := T_KeyPoint.Create;
      xKeyPoint.Pos[0] := x - 0.2;
      xKeyPoint.Pos[1] := y + 2.2;
      if IsCorrectPlace(xKeyPoint.Pos) then
        Scene.KeyPoints.Add(xKeyPoint)
      else
        xKeyPoint.Free;
    end;

  for i := Scene.KeyPoints.Count - 1 downto 0 do
    if not IsCorrectPlace(T_KeyPoint(Scene.KeyPoints[i]).Pos) then
      Scene.KeyPoints.Remove(Scene.KeyPoints[i]);
  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
      case Cells[i][j].isPassable of
        True:
          Cells[i][j].SpriteType := 0;
        False:
          Cells[i][j].SpriteType := 1;
      end;

end;

procedure T_Map.Draw;
var
  i, j: integer;
begin
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glBegin(GL_QUADS);
  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
    begin
      case Cells[i][j].SpriteType of
        0:
          glColor4f(0.2, 0.2, 0.2, 1);
        1:
          glColor4f(0.7, 0.7, 0.7, 1);
      end;
      glVertex2f(i, j);
      glVertex2f(i + CellWidth, j);
      glVertex2f(i + CellWidth, j + CellHeight);
      glVertex2f(i, j + CellHeight);
    end;
  glEnd;
  glPopAttrib;
end;

end.
