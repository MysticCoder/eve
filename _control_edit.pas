unit _control_edit;

{$mode delphi}

interface

uses
  Classes, SysUtils, _textures, _window, _usual, _keys, gl, glu;

type

  { T_Scene_Control_Editor }

  T_Scene_Control_Editor = class(T_Scene)
    CList: TS_List;
    CEditInit: TS_Edit;
    CurrentControl: TS_Control;
    CurrentInternal: TS_Internal;
    CurrentCClassesListItemIndex: integer;
    CurrentCListItemIndex: integer;
    CClassesList: TS_List;
    ClassesList: TObjectList;
    SystemControls : TObjectList;
    OldMouseX,
    OldMouseY : integer;
    function IsKeyDown(AKey: word): boolean;
    procedure OnCListItemDown(AItemIndex: integer);
    procedure OnCClassesListItemDown(AItemIndex: integer);
    procedure OnCEditUniChar(AChar: ansistring; AKeyCode: word);
    procedure OnControlMouseMove(AX, AY : integer; AControl : TS_Control);
    procedure OnSysControlsMouseDown(AX, AY : integer; AButton : T_MouseButton);
    procedure ShowClasses;
    procedure ShowInitials(AControl: TS_Control);
    procedure SaveControlsToFile;
    procedure SetOnMoveToControls;
    procedure InitScene; override;
    procedure UpdateScene; override;
    procedure DrawScene; override;
    procedure FreeScene; override;
  end;

implementation

{ T_Scene_Control_Editor }

function T_Scene_Control_Editor.IsKeyDown(AKey: word): boolean;
begin
  Result := Window.Keys[AKey];
  Window.Keys[AKey] := False;
end;

procedure T_Scene_Control_Editor.OnCListItemDown(AItemIndex: integer);
var
  xInternName: string;
  xInternal: TS_Internal;
  i: integer;
begin
  if CurrentCListItemIndex <> AItemIndex then
  begin
    CurrentCListItemIndex := AItemIndex;
    Exit;
  end;
  xInternName := CList.Strings[AItemIndex];
  if xInternName = 'ClassType' then
    ShowClasses
  else
    begin
    CEditInit.Visible := True;
    CEditInit.SetFocus;
    CEditInit.Parent.BringToFrontChild(CEditInit);
    for i := 0 to CurrentControl.Internals.Count - 1 do
    begin
      xInternal := TS_Internal(CurrentControl.Internals[i]);
      if xInternal.Name = xInternName then
      begin
        CurrentInternal := xInternal;
        CEditInit.Caption := xInternal.GetString;
      end;
    end;

    end;
end;

procedure T_Scene_Control_Editor.OnCClassesListItemDown(AItemIndex: integer);
var
  xInternName: string;
  xInternal: TS_Internal;
  i: integer;
  xMemStream : TMemoryStream;
begin
  if CurrentCClassesListItemIndex <> AItemIndex then
  begin
    CurrentCClassesListItemIndex := AItemIndex;
    Exit;
  end
    else
      begin
        xMemStream := CurrentControl.SaveInternalsToMem;
        xMemStream.Position := 0;
        CurrentControl.Parent.Childs.Remove(CurrentControl);
        //Window.MainControl.Childs.Remove(CurrentControl);
        CurrentControl.Free;
        CurrentControl := TS_Control(TS_ControlClass(ClassesList[AItemIndex]).Create);
        CurrentControl.LoadInternalsFromMem(xMemStream);
        CurrentControl.RecalcPos;
        CurrentControl.OnMouseMoveProc := OnControlMouseMove;
        //Window.MainControl.AddChild(CurrentControl);
        xMemStream.Free;
        CClassesList.Visible := False;
      end;
end;

procedure T_Scene_Control_Editor.OnCEditUniChar(AChar: ansistring; AKeyCode: word);
var
  xParentControl : TS_Control;
begin
  if AKeyCode = MK_RETURN then
  begin
    CurrentInternal.SetValue(CEditInit.Caption);
    if CurrentInternal.Name = 'ParentName' then
      begin
        xParentControl := Window.FindControlByName(CurrentInternal.ValueString^);
        if xParentControl <> nil then
          begin
            CurrentControl.Parent.Childs.Remove(CurrentControl);
            xParentControl.AddChild(CurrentControl);
          end;
      end;
    CEditInit.ReleaseFocus;
    CEditInit.Visible := False;
    CurrentControl.RecalcPos;
  end;
end;

procedure T_Scene_Control_Editor.OnControlMouseMove(AX, AY: integer;
  AControl: TS_Control);
var
  xSign : integer;
begin
  try
  if (Window.FocusedControl = nil) or (Window.FocusedControl = Window.MainControl) then
    Exit;
  if Window.FocusedControl <> AControl then
    Exit;
  if Window.Mouse.LMB then
    begin
      Window.FocusedControl.BaseX := Window.FocusedControl.BaseX + AX - OldMouseX;
      Window.FocusedControl.BaseY := Window.FocusedControl.BaseY + AY - OldMouseY;
    end;
  if Window.Mouse.RMB then
    begin
      xSign := -1;
      if AX > Window.FocusedControl.X + Window.FocusedControl.Width/2 then
        xSign := 1;
      if Window.Keys[MK_LSHIFT] = False then
        xSign := 1;

      Window.FocusedControl.BaseWidth := Window.FocusedControl.BaseWidth + xSign*(AX - OldMouseX);
      xSign := -1;
      if AY > Window.FocusedControl.Y + Window.FocusedControl.Height/2 then
        xSign := 1;
      if Window.Keys[MK_LSHIFT] = False then
        xSign := 1;
      Window.FocusedControl.BaseHeight := Window.FocusedControl.BaseHeight + xSign*(AY - OldMouseY);
    end;

  finally
    OldMouseX := AX;
    OldMouseY := AY;
  end;
end;

procedure T_Scene_Control_Editor.OnSysControlsMouseDown(AX, AY: integer;
  AButton: T_MouseButton);
begin
  if AButton = mbRight then
    Window.FocusedControl.Hide;
end;

procedure T_Scene_Control_Editor.ShowClasses;
var
  i : integer;
  xClass : TClass;
begin
  CClassesList.Strings.Clear;
  for i := 0 to ClassesList.Count - 1 do
    begin
      xClass := TClass(ClassesList[i]);
      CClassesList.Strings.Add(xClass.ClassName);
      if xClass.ClassName = CurrentControl.ClassName then
        CClassesList.SelIndex := i;
    end;
  CClassesList.Show;
  CClassesList.Parent.BringToFrontChild(CClassesList);
  CClassesList.SetFocus;
end;

procedure T_Scene_Control_Editor.ShowInitials(AControl: TS_Control);
var
  i: integer;
  xInternal: TS_Internal;
begin
  CurrentControl := AControl;
  CList.Strings.Clear;
  CList.Strings.Add('ClassType');
  for i := 0 to CurrentControl.Internals.Count - 1 do
  begin
    xInternal := TS_Internal(AControl.Internals[i]);
    CList.Strings.Add(xInternal.Name);
  end;
  CList.Show;
  CList.SetFocus;
  CList.Parent.BringToFrontChild(CList);
end;

procedure T_Scene_Control_Editor.SaveControlsToFile;
  procedure _SaveControl(AControl : TS_Control; AF : TStream);
  var
    xM : TMemoryStream;
    i : integer;
    xControl : TS_Control;
    F : TextFile;
  begin
    if SystemControls.IndexOf(AControl) <> -1 then
      Exit;
    if AControl.isCreatedByParent then
      Exit;
    if AControl <> Window.MainControl then
      begin
        WritelnToStream(AF, AControl.ClassName);
        xM := AControl.SaveInternalsToMem;
        xM.Position := 0;
        AF.CopyFrom(xM, xM.Size);
        xM.Free;
      end;
    for i := 0 to AControl.Childs.Count - 1 do
      begin
        xControl := TS_Control(AControl.Childs[i]);
        _SaveControl(xControl, AF);
      end;
  end;

var
  i : integer;
  M : TMemoryStream;
  F : TFileStream;
begin
  DeleteFile('controls.ini');
  F := TFileStream.Create('controls.ini', fmCreate or fmOpenReadWrite);
  _SaveControl(Window.MainControl, F);
  F.Free;
end;

procedure T_Scene_Control_Editor.SetOnMoveToControls;
  procedure _SetOnMoveToControl(AControl : TS_Control);
  var
    i : integer;
  begin
    if SystemControls.IndexOf(AControl) <> -1 then
      Exit;
    if (AControl <> Window.MainControl) and (not AControl.isCreatedByParent) then
      AControl.OnMouseMoveProc := OnControlMouseMove;
    for i := 0 to AControl.Childs.Count - 1 do
      _SetOnMoveToControl(TS_Control(AControl.Childs[i]));
  end;
begin
  _SetOnMoveToControl(Window.MainControl);
end;

procedure T_Scene_Control_Editor.InitScene;
begin
  inherited InitScene;
  SystemControls := TObjectList.create(False);
  Window.MainControl.Color := $FF000000;
  CList := TS_List.Create;
  CList.BaseWidth := 200;
  CList.BaseHeight := 200;
  CList.Anchor := tsa_Center;
  CList.RecalcPos;
  CList.ItemHeight := CList.Font.Height;
  CList.Color := $FFFFA0A0;
  CList.OnItemMouseDownProc := OnCListItemDown;
  CList.Visible := False;
  CList.OnMouseDownProc := OnSysControlsMouseDown;
  Window.MainControl.AddChild(CList);
  SystemControls.Add(CList);
  //CList.OnReleaseFocus := CList.Hide;

  CClassesList := TS_List.Create;
  CClassesList.BaseWidth := 200;
  CClassesList.BaseHeight := 200;
  CClassesList.Anchor := tsa_Center;
  CClassesList.RecalcPos;
  CClassesList.ItemHeight := CList.Font.Height;
  CClassesList.Color := $FFFFA0A0;
  CClassesList.OnItemMouseDownProc := OnCClassesListItemDown;
  CClassesList.Visible := False;
  CClassesList.OnMouseDownProc := OnSysControlsMouseDown;
  SystemControls.Add(CClassesList);
  //CClassesList.OnReleaseFocus := CClassesList.Hide;
  Window.MainControl.AddChild(CClassesList);

  ClassesList := TObjectList.Create(False);
  ClassesList.Add(TObject(TS_Control));
  ClassesList.Add(TObject(TS_List));
  ClassesList.Add(TObject(TS_Edit));
  ClassesList.Add(TObject(TS_VertScrollBar));


  CEditInit := TS_Edit.Create;
  Window.MainControl.AddChild(CEditInit);
  CEditInit.Visible := False;
  CEditInit.BaseWidth := 300;
  CEditInit.BaseHeight := 40;
  CEditInit.Anchor := tsa_Center;
  CEditInit.Color := $A0A0A0A0;
  CEditInit.RecalcPos;
  CEditInit.Name := 'CEdit';
  CEditInit.OnUniCharProc := OnCEditUniChar;
  SystemControls.Add(CEditInit);
  //CEditInit.OnReleaseFocus := CEditInit.Hide;
  //CEditInit.OnUniCharProc := ;
  CEditInit.OnMouseDownProc := OnSysControlsMouseDown;
  Window.LoadControlsFromFile('controls.ini');
  SetOnMoveToControls;
  Window.MainControl.RecalcPos;
end;

procedure T_Scene_Control_Editor.UpdateScene;
var
  xControl: TS_Control;
begin
  inherited UpdateScene;
  xControl := Window.FocusedControl;
  if (xControl = nil) or (xControl = Window.MainControl) then
    begin
      if IsKeyDown(MK_N) then
      begin
        xControl := TS_Control.Create;
        xControl.Anchor := tsa_Center;
        xControl.BaseWidth := 100;
        xControl.BaseHeight := 100;
        xControl.RecalcPos;
        xControl.SetFocus;
        xControl.DoDrawFocus := True;
        xControl.OnMouseMoveProc := OnControlMouseMove;
        Window.MainControl.AddChild(xControl);
      end;
      if IsKeyDown(MK_S) then
      begin
        SaveControlsToFile;
      end;
      Exit;
    end;
  if CEditInit.Visible then
    Exit;
  if Window.Keys[MK_LSHIFT] and Window.Keys[MK_BACKSPACE] then
    begin
      xControl.Free;
      Exit;
    end;
  if IsKeyDown(MK_NUMPAD7) then
    xControl.Anchor := tsa_LeftTop;
  if IsKeyDown(MK_NUMPAD8) then
    xControl.Anchor := tsa_Top;
  if IsKeyDown(MK_NUMPAD9) then
    xControl.Anchor := tsa_RightTop;
  if IsKeyDown(MK_NUMPAD4) then
    xControl.Anchor := tsa_Left;
  if IsKeyDown(MK_NUMPAD5) then
    xControl.Anchor := tsa_Center;
  if IsKeyDown(MK_NUMPAD6) then
    xControl.Anchor := tsa_Right;
  if IsKeyDown(MK_NUMPAD1) then
    xControl.Anchor := tsa_LeftBottom;
  if Window.Keys[MK_NUMPAD2] then
    xControl.Anchor := tsa_Bottom;
  if Window.Keys[MK_NUMPAD3] then
    xControl.Anchor := tsa_RightBottom;
  if Window.Keys[MK_LEFT] then
    if Window.Keys[MK_LSHIFT] then
      xControl.BaseWidth := xControl.BaseWidth - 1
    else
      xControl.BaseX := xControl.BaseX - 1;
  if Window.Keys[MK_RIGHT] then
    if Window.Keys[MK_LSHIFT] then
      xControl.BaseWidth := xControl.BaseWidth + 1
    else
      xControl.BaseX := xControl.BaseX + 1;
  if Window.Keys[MK_DOWN] then
    if Window.Keys[MK_LSHIFT] then
      xControl.BaseHeight := xControl.BaseHeight - 1
    else
      xControl.BaseY := xControl.BaseY + 1;
  if Window.Keys[MK_UP] then
    if Window.Keys[MK_LSHIFT] then
      xControl.BaseHeight := xControl.BaseHeight + 1
    else
      xControl.BaseY := xControl.BaseY - 1;
  if Window.Keys[MK_NUMPAD0] then
  begin
    xControl.BaseX := 0;
    xControl.BaseY := 0;
    xControl.Anchor := tsa_Center;
  end;
  if IsKeyDown(MK_RETURN) then
    ShowInitials(xControl);
  xControl.RecalcPos;
end;

procedure T_Scene_Control_Editor.DrawScene;
begin
  glClear(GL_COLOR_BUFFER_BIT);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  Window.DrawControls;
  inherited DrawScene;
end;

procedure T_Scene_Control_Editor.FreeScene;
begin
  inherited FreeScene;
end;

end.


