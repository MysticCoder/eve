unit _space_stealth_scenes;

{$mode delphi}

interface

uses
  _window, gl, _textures,  _font;

type

  { T_Space_Stealth_LogoScene }

  T_Space_Stealth_LogoScene = class(T_Scene)
    TexLogo : Cardinal;
    LogoControl : TS_Control;
    ShadeTimer : Single;
    FadeDirection : Boolean;
    Font : T_Font;
    LogoProceeded : Boolean;
    procedure InitScene; override;
    procedure UpdateScene; override;
    procedure DrawScene; override;
    procedure FreeScene; override;
  end;

implementation

  uses
    _space_stealth;

procedure T_Space_Stealth_LogoScene.InitScene;
begin
  inherited InitScene;
  LogoControl := TS_Control.Create;
  LogoControl.Anchor := tsa_Center;
  LogoControl.Width := 400;
  LogoControl.Height := 400;
  LogoControl.BaseX := 0;//-LogoControl.Width div 2;
  LogoControl.BaseY := 0;//-LogoControl.Height div 2;
  LogoControl.SetTexture('data' + DirectorySeparator +'logo.tga');
  LogoControl.RecalcPos;
  Window.Controls.Add(LogoControl);
  Font := T_Font.Create;
  Font.LoadFromFile('DefaultFont');
  LogoControl.Font := Font;
  LogoControl.Caption := 'Caption';
end;

procedure T_Space_Stealth_LogoScene.UpdateScene;
begin
  inherited UpdateScene;
  if FadeDirection = False then
    begin
      ShadeTimer := ShadeTimer + Window.Scene.dt;
      if ShadeTimer > 5 then
        begin
          FadeDirection := True;
          ShadeTimer := 5;
        end;
    end;
  if FadeDirection then
    begin
      ShadeTimer := ShadeTImer - Window.Scene.dt * 2;
      if ShadeTimer < 0 then
        begin
          if LogoProceeded then
            begin
              Window.DelayedChangeScene(T_Space_Stealth.Create);
            end
          else
            begin
              LogoControl.Width := Window.Width;
              LogoControl.Height := Window.Height;
              LogoControl.RecalcPos;
              LogoControl.SetTexture('data' + DirectorySeparator + 'menu_fon.tga');
              LogoProceeded := True;
              ShadeTimer := 0;
              FadeDirection := False;
            end;
        end;
    end;
end;


procedure T_Space_Stealth_LogoScene.DrawScene;
begin
  inherited DrawScene;
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glColor4f(1, 1, 1, ShadeTimer / 5);
  Window.DrawControls;
  glDisable(GL_BLEND);
end;

procedure T_Space_Stealth_LogoScene.FreeScene;
begin
  Window.Controls.Remove(LogoControl);
  LogoControl.Free;
  Font.Free;
  inherited FreeScene;
end;

end.

