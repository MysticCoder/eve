unit _font;

{$mode delphi}

interface

uses
  Classes, SysUtils, _usual, _textures, gl, math;

type
  R_FontChar = packed record
    Char : AnsiString;
    TexCoords : RVector4f;
    iTexCoords : RVector4i;
  end;

  P_FontChar = ^R_FontChar;
  { T_Font }

  T_Font = class
    Texture : Cardinal;
    Height : integer;
    Color : Cardinal;
    Chars : TList; // of T_FontChar;
    constructor Create;
    procedure LoadFromFile(AFileName : AnsiString);
    procedure DrawChars(AString : AnsiString);
    procedure DrawCharsXY(AString : AnsiString; AX, AY : Single);
    function FindChar(AChar : AnsiString) : P_FontChar;
    private
      procedure DrawChar(AChar : AnsiString);
  end;

implementation
  uses LazUTF8;
{ T_Font }

constructor T_Font.Create;
begin
  Chars := TList.Create;
  Color := $FFFFFFFF;
end;

procedure T_Font.LoadFromFile(AFileName: AnsiString);
var
  F : TMemoryStream;
  xCount : integer;
  xUCount : integer;
  xString : AnsiString;
  xChar : P_FontChar;
  i, j, k : integer;
  xWidth,
  xHeight : integer;
  xMem : PByte;
  xByte  : Byte;
  xR, xG, xB : Byte;
  xPixel : Cardinal;
  xMax : Byte;
  xPos : Int64;
  xK : Single;
begin
  F := TMemoryStream.Create;
  F.LoadFromFile(AFileName);

  xCount := F.ReadDWord;
  SetLength(xString, xCount);
  F.Read(xString[1], xCount);

  xUCount := UTF8Length(xString);
  Height := 0;
  for i := 0 to xUCount - 1 do
    begin
      New(xChar);
      xChar^.Char := UTF8Copy(xString, i + 1, 1);
      xChar^.iTexCoords[0] := F.ReadWord;
      xChar^.iTexCoords[1] := F.ReadWord;
      xChar^.iTexCoords[2] := F.ReadWord;
      xChar^.iTexCoords[3] := F.ReadWord;
      Height := Max(Height, xChar^.iTexCoords[3]);
      Chars.Add(xChar);
    end;
  xWidth := F.ReadDWord;
  xHeight := F.ReadDWord;

  for i := 0 to xUCount - 1 do
    begin
      xChar := Chars[i];
      with xChar^ do
        begin
          TexCoords[0] := iTexCoords[0] / xWidth;
          TexCoords[1] := iTexCoords[1] / xHeight;
          TexCoords[2] := iTexCoords[2] / xWidth;
          TexCoords[3] := iTexCoords[3] / xHeight;
        end;
    end;

  GetMem(xMem, xWidth * xHeight * 4);
  FillChar(xMem^, xWidth * xHeight * 4, 100);
  xMax := 0;
  xPos := F.Position;
  for i := 0 to xWidth - 1 do
    for j := 0 to (xHeight - 1) do
      begin
        xByte := F.ReadByte;
        xMax := Max(xByte, xMax);
      end;

  xK := 255/xMax;
  F.Position := xPos;
  for i := 0 to xWidth - 1 do
    for j := 0 to (xHeight - 1) do
      begin
        xByte := Round(F.ReadByte * xK);

        xR := xByte;
        xG := xByte;
        xB := xByte;
        xPixel := (xByte shl 24) or (xB shl 16) or (xG shl 8) or xR;
        PCardinal(PtrUInt(xMem) + i * 4 + (j) * xWidth * 4)^ := xPixel;

      end;
  Texture := TextureManager.LoadTextureFromMem(xWidth, xHeight, xMem);
  Freemem(xMem);
  F.Free;
end;

procedure T_Font.DrawChar(AChar: AnsiString);
var
  xChar : P_FontChar;
begin
  xChar := FindChar(AChar);
  if xChar = nil then
    Exit;
  glPushAttrib(GL_COLOR_BUFFER_BIT);
  glColor4ubv(@Color);
  with xChar^ do
    begin
      glBegin(GL_QUADS);

      glTexCoord2f(TexCoords[0], TexCoords[1]);
        glVertex2f(0, 0);
      glTexCoord2f(TexCoords[0] + TexCoords[2], TexCoords[1]);
        glVertex2f(iTexCoords[2], 0);
      glTexCoord2f(TexCoords[0] + TexCoords[2], TexCoords[1] + TexCoords[3]);
        glVertex2f(iTexCoords[2], iTexCoords[3]);
      glTexCoord2f(TexCoords[0], TexCoords[1] + TexCoords[3]);
        glVertex2f(0, iTexCoords[3]);
      glEnd;

    end;
end;

procedure T_Font.DrawChars(AString: AnsiString);
var
  i : integer;
  xChar : P_FontChar;
begin
  TextureManager.SetTexture(Texture);
  glPushMatrix;
  for i := 0 to UTF8Length(AString) - 1 do
    begin
      xChar := FindChar(UTF8Copy(AString, i + 1, 1));
      if xChar = nil then
        continue;
      DrawChar(xChar^.Char);
      glTranslatef(xChar^.iTexCoords[2], 0, 0);
    end;
  glPopMatrix;
  TextureManager.UnSetTexture;
end;

procedure T_Font.DrawCharsXY(AString: AnsiString; AX, AY: Single);
begin
  glTranslatef(AX, AY, 0);
  DrawChars(AString);
  glTranslatef(-AX, -AY, 0);
end;

function T_Font.FindChar(AChar: AnsiString): P_FontChar;
var
  i : integer;
begin
  for i := 0 to Chars.Count - 1 do
    begin
      Result := Chars[i];
      if Result^.Char = AChar then
        begin
          Exit;
        end;
    end;
  Result := nil;
end;

end.

