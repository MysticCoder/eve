unit _scene_explore;

{$mode delphi}

interface

uses
  Classes, SysUtils, _window, gl, glu, glext, _usual, _aspect, _keys, math;

type
  R_Camera = record
    Angle : RVector3f;
    Pos   : RVector3f;
  end;

  R_LandCell = record
    Height : Single;
  end;
  P_LandCell = ^R_LandCell;

  { T_PlayerController }

  T_PlayerController = class(T_Aspect)
    Physic : T_Physic;
    procedure InitAspect; override;
    procedure Update; override;
  end;

  { T_Land }

  T_Land = class
    Width,
    Height : integer;
    Cells : array of array of P_LandCell;
    Vx : array of Single;
    VBuf : Cardinal;
    HeightMap : array of array of Single;
    procedure CreateRock(AX, AY, AZ : Single);
    procedure CreatePlayer(AX, AY, AZ : Single);
    procedure GeneratePlaneLand(AWidth, AHeight : integer);
    procedure Draw;
    procedure Draw1;
    procedure Draw2;
    function FindZ(AX, AY: Single): Single;
    function FindZ2(AX, AY : Single) : Single;
  end;

  { T_Scene_Explore }

  T_Scene_Explore = class(T_Scene)
    Land : T_Land;
    Camera : R_Camera;
    UpdateList : T_Aspects;
    DrawList : T_Aspects;
    procedure InitScene; override;
    procedure UpdateScene; override;
    procedure DrawScene; override;
    procedure SetCamera;
  end;

var
  Scene : T_Scene_Explore;
implementation

{ T_PlayerController }

procedure T_PlayerController.InitAspect;
begin
  inherited InitAspect;
  Subscribe(astUpdate, True);
  GetAspect(T_Physic, @Physic);
end;

procedure T_PlayerController.Update;
begin
  inherited Update;
  Physic.Speed[0] := 0;
  Physic.Speed[1] := 0;
  if Window.Keys[MK_W] then
    begin
      Physic.Speed[0] := Physic.Speed[0] + Cos(Physic.Transform.Orientation[2]);
      Physic.Speed[1] := Physic.Speed[1] - Sin(Physic.Transform.Orientation[2]);
    end;
  if Window.Keys[MK_S] then
    begin
      Physic.Speed[0] := Physic.Speed[0] - Cos(Physic.Transform.Orientation[2]);
      Physic.Speed[1] := Physic.Speed[1] + Sin(Physic.Transform.Orientation[2]);
    end;
  if Window.Keys[MK_A] then
    begin
      Physic.Speed[0] := Physic.Speed[0] + Cos(Physic.Transform.Orientation[2] - Pi/2);
      Physic.Speed[1] := Physic.Speed[1] - Sin(Physic.Transform.Orientation[2] - Pi/2);
    end;
  if Window.Keys[MK_D] then
    begin
      Physic.Speed[0] := Physic.Speed[0] + Cos(Physic.Transform.Orientation[2] + Pi/2);
      Physic.Speed[1] := Physic.Speed[1] - Sin(Physic.Transform.Orientation[2] + Pi/2);
    end;

  if Physic.Speed.Length <> 0 then
    if Window.Keys[MK_LSHIFT] then
      Physic.Speed.SetLength(5 * Scene.dt)
    else
      Physic.Speed.SetLength(1.4 * Scene.dt);

  if Window.Keys[MK_LEFT] then
    Physic.Transform.Orientation[2] := Physic.Transform.Orientation[2] - 30 * Pi/180 * Scene.dt;
  if Window.Keys[MK_RIGHT] then
    Physic.Transform.Orientation[2] := Physic.Transform.Orientation[2] + 30 * Pi/180 * Scene.dt;
  if Window.Keys[MK_UP] then
    Physic.Transform.Orientation[0] := Physic.Transform.Orientation[0] - 30 * Pi/180 * Scene.dt;
  if Window.Keys[MK_DOWN] then
    Physic.Transform.Orientation[0] := Physic.Transform.Orientation[0] + 30 * Pi/180 * Scene.dt;

  Physic.Transform.Pos[2] := Scene.Land.FindZ(Physic.Transform.Pos[0],Physic.Transform.Pos[1]) + 1.7;
  Scene.Camera.Pos := Physic.Transform.Pos;
  Scene.Camera.Angle[0] := Physic.Transform.Orientation[0] * 180/Pi;
  Scene.Camera.Angle[1] := Physic.Transform.Orientation[1] * 180/Pi;
  Scene.Camera.Angle[2] := Physic.Transform.Orientation[2] * 180/Pi;
end;

{ T_Land }

procedure T_Land.CreateRock(AX, AY, AZ: Single);
var
  xRock : T_Aspects;
  xTransform : T_Transform;
begin
  xRock := T_Aspect.CreateAspects('Rock', [T_Transform, T_Graphic]);
  xRock.FindAspect(T_Transform, @xTransform);
  xTransform.Pos[0] := AX;
  xTransform.Pos[1] := AY;
  xTransform.Pos[2] := AZ;
end;

procedure T_Land.CreatePlayer(AX, AY, AZ: Single);
var
  xPlayer : T_Aspects;
  xTransform : T_Transform;
begin
  xPlayer := T_Aspect.CreateAspects('Player', [T_Transform, T_Physic, T_PlayerController]);
  xPlayer.FindAspect(T_Transform, @xTransform);
  xTransform.Pos[0] := AX;
  xTransform.Pos[1] := AY;
  xTransform.Pos[2] := AZ;
end;

procedure T_Land.GeneratePlaneLand(AWidth, AHeight: integer);
var
  i, j : integer;
  m, n : integer;
  xCell : P_LandCell;
  xInd : integer;
  xHeight : Single;
  procedure _add(x,y,z : single);
  begin
    Vx[xInd] := x;
    Vx[xInd + 1] := y;
    Vx[xInd + 2] := z;
    inc(xInd, 3);
  end;

begin
  Width := AWidth;
  Height := AHeight;
  glGenBuffers(1, @VBuf);
  glBindBuffer(GL_ARRAY_BUFFER, VBuf);
  SetLength(Vx, Round(Width*Height*10*3/4));
  xInd := 0;
  for i := 0 to (Width div 2) - 1 do
    for j := 0 to (Height div 2) - 1 do
      begin
        m := (i * 2) + 1;
        n := (j * 2) + 1;
        _add(m, n, 0);
        _add(m - 1, n - 1, 0.0);
        _add(m + 0, n - 1, 0.0);
        _add(m + 1, n - 1, 0.0);
        _add(m + 1, n + 0, 0.0);
        _add(m + 1, n + 1, 0.0);
        _add(m + 0, n + 1, 0.0);
        _add(m - 1, n + 1, 0.0);
        _add(m - 1, n + 0, 0.0);
        //_add(m,n,2);//,0,0);
          _add(m - 1, n - 1, 0.0);
      end;
  glBufferData(GL_ARRAY_BUFFER, xInd - 1, @Vx[0], GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  //glDisable(GL_CULL_FACE);
  SetLength(Cells, Width, Height);
  SetLength(HeightMap, Width, Height);
  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
      begin
        New(xCell);
        xCell^.Height := random*0.5;
        Cells[i][j] := xCell;
        HeightMap[i][j] := Random(30);
      end;

  for xInd := 1 to 20 do
  for i := 1 to Width - 2 do
    for j := 1 to Height - 2 do
      begin
        xHeight := 0;
        for m := -1 to 1 do
          for n := -1 to 1 do
            xHeight := xHeight + HeightMap[i+m][j+n];
        xHeight := xHeight/9;
        HeightMap[i][j] := xHeight;
      end;

  for i := 0 to Round(0.02*Width*Height) do
    begin
      CreateRock(Random(Width), Random(Height), 0);
    end;
  CreatePlayer(5, 5, 5);
end;

procedure T_Land.Draw;
var
  i, j : integer;
  xCell : P_LandCell;
begin
  Draw2;
  Exit;
  RandSeed := 10;
  glEnableClientState(GL_VERTEX_ARRAY);
  glBindBuffer(GL_ARRAY_BUFFER, VBuf);
  glVertexPointer(3, GL_FLOAT, 0, nil);//@Vx[0]);
  //for i := 0 to (Width*Height div 4)-1 do
    begin
//      glEnableClientState(GL_VERTEX_ARRAY);
      //glColor3f(random,random,random);
    glDrawArrays(GL_LINE_LOOP, 0, 10*(Width*Height div 4)-1);
//    glDisableClientState(GL_VERTEX_ARRAY);
    end;
      //glDrawArrays(GL_TRIANGLE_FAN, 0, 600);
  glDisableClientState(GL_VERTEX_ARRAY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  Exit;
  glBegin(GL_QUADS);
  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
      begin
        xCell := Cells[i][j];
        glVertex3f(i, j, xCell^.Height);
        glVertex3f(i + 1, j, xCell^.Height);
        glVertex3f(i + 1, j + 1, xCell^.Height);
        glVertex3f(i, j + 1, xCell^.Height);
      end;
  glEnd;
end;

procedure T_Land.Draw1;

var
  i, j : integer;
  xCamX,
  xCamY : Single;
  xAngle : Single;
  xMinAng : Single;
  xAng : Single;
  xCount : integer;
  xDist : Single;
  xNextDist : Single;
  xX, xY, xZ : Single;
begin
  xCamX := Scene.Camera.Pos[0];
  xCamY := Scene.Camera.Pos[1];
//  xCamx:=0;
//  xCamY:=0;
//xAngle := 0;
RandSeed:=10;
xAngle := -(Scene.Camera.Angle[2]-0)*Pi/180;
  glBegin(GL_QUADS);
  xCount := 100;
  xDist := 0;
  for i := 0 to 100 do
    begin
    xNextDist := sqr(i/10);
    for j := 0 to xCount - 1 do
      begin
        glColor3f(random,random,random);
        xAng := Pi/xCount;
        xX := xCamX + (xDist + 0)*cos(xAngle + (j+0)*xAng);
        xY := xCamY + (xDist + 0)*sin(xAngle + (j+0)*xAng);
        xZ := FindZ(xX, xY);
        glVertex3f(xX, xY, xZ);
        xX := xCamX + (xDist + 0)*cos(xAngle + (j+1)*xAng);
        xY := xCamY + (xDist + 0)*sin(xAngle + (j+1)*xAng);
        xZ := FindZ(xX, xY);
        glVertex3f(xX, xY, xZ);
        xX := xCamX + (xDist + xNextDist)*cos(xAngle + (j+1)*xAng);
        xY := xCamY + (xDist + xNextDist)*sin(xAngle + (j+1)*xAng);
        xZ := FindZ(xX, xY);
        glVertex3f(xX, xY, xZ);
        xX := xCamX + (xDist + xNextDist)*cos(xAngle + (j+0)*xAng);
        xY := xCamY + (xDist + xNextDist)*sin(xAngle + (j+0)*xAng);
        xZ := FindZ(xX, xY);
        glVertex3f(xX, xY, xZ);
      end;
    xDist := xDist + xNextDist;

    end;
  glEnd;
  Scene.DrawList.Draw;
end;

procedure T_Land.Draw2;
var
  i, j : integer;
  xMinX, xMinY, xMaxX, xMaxY : integer;
  xRadius : integer;
begin
  xRadius := 100;
  xMinX := Round(Scene.Camera.Pos[0] - xradius);
  xMaxX := xMinX + 2 * xradius;
  xMinY := Round(Scene.Camera.Pos[1] - xradius);
  xMaxY := xMinY + 2 * xradius;
  xMinX := Min(xMinX, Width - 2);
  xMinX := Max(xMinX, 0);
  xMaxX := Min(xMaxX, Width - 2);
  xMaxX := Max(xMaxX, 0);
  xMinY := Min(xMinY, Height - 2);
  xMinY := Max(xMinY, 0);
  xMaxY := Min(xMaxY, Height - 2);
  xMaxY := Max(xMaxY, 0);
  RandSeed := 100;
  glBegin(GL_QUADS);
  for i := xMinX to xMaxX do
    for j := xMinY to xMaxY do
      begin
      glColor3f(i/Width,j/Height,1);
        glVertex3f(i, j, HeightMap[i, j]);
        glVertex3f(i+1, j, HeightMap[i+1, j]);
        glVertex3f(i+1, j+1, HeightMap[i+1, j+1]);
        glVertex3f(i, j+1, HeightMap[i, j+1]);
      end;
  glEnd;
end;

function T_Land.FindZ(AX, AY: Single): Single;
var
  xI, xJ : integer;
  x0,y0 : Single;
  x2, y2, z2,
  x3, y3, z3,
  x1, y1, z1 : single;
  a,b,c,d : Single;
begin
  xI := floor(AX);
  xJ := floor(AY);
  x0 := AX - xI;
  y0 := AY - xJ;

  //точка 2
  X2:= xI;
  Y2:= (xJ+1);
  Z2:=HeightMap[xI][xJ+1];
  //точка 4
  X3:= (xI+1);
  Y3:= xJ;
  Z3:=HeightMap[xI+1][xJ];
  //точка 1 или 3
  if  (X0+Y0>=1) then
    begin
         X1:= xI;
         Y1:= xJ;
         Z1:=HeightMap[xI][xJ];
    end
  else
      begin
         X1:= (xI+1);
         Y1:=(xJ+1);
         Z1:=HeightMap[xI+1][xJ+1];
      end;
      a:=-(y3*z2-y1*z2-y3*z1+z1*y2+z3*y1-y2*z3);
      b:= (y1*x3+y2*x1+y3*x2-y2*x3-y1*x2-y3*x1);
      c:= (z2*x3+z1*x2+z3*x1-z1*x3-z2*x1-x2*z3);
      d:=-a*x1-b*z1-c*y1;
      Result :=-(a*AX+c*AY+d)/b;
end;

function T_Land.FindZ2(AX, AY: Single): Single;
var
  xMinX,
  xMinY : integer;
  xMaxX,
  xMaxY : integer;
  xAllWeight : Single;
  function _FindZ2(AX2, AY2 : Single; var AWeight : Single) : Single;
  var
    xWeight : Single;
  begin
    xWeight := sqrt(sqr(AX2-AX)+sqr(AY2-AY));
    AWeight := AWeight + xWeight;
    Result := xWeight*HeightMap[Round(AX2)][Round(AY2)];
  end;

begin
  AX := Max(AX, 0);
  AX := Min(AX, Width - 2);
  AY := Max(AY, 0);
  AY := Min(AY, Height - 2);
  xMinX := floor(AX);
  xMinY := floor(AY);
  xMaxX := ceil(AX);
  xMaxY := ceil(AY);
  Result := HeightMap[Round(AX)][Round(AY)];
  xAllWeight:=0;
  Result := _FindZ2(xMinX, xMinY, xAllWeight)+
  _FindZ2(xMinX+1, xMinY, xAllWeight)+
  _FindZ2(xMinX+1, xMinY+1, xAllWeight)+
  _FindZ2(xMinX, xMinY+1, xAllWeight);
  Result := Result/xAllWeight;
  if Result > 20 then
    sleep(1);
end;

{ T_Scene_Explore }

procedure T_Scene_Explore.InitScene;
begin
  inherited InitScene;
  Scene := Self;
  UpdateList := T_Aspects.Create(False);
  DrawList := T_Aspects.Create(False);
  UpdateSubscribeList := UpdateList;
  DrawSubscribeList := DrawList;
  Land := T_Land.Create;
  Land.GeneratePlaneLand(200, 200);
  Camera.Pos[2] := 10;
  Camera.Angle[0] := -90;
  Camera.Angle[1] := 0;
  Camera.Angle[2] := 45;
end;

procedure T_Scene_Explore.UpdateScene;
begin
  inherited UpdateScene;
  UpdateList.Update;
end;

procedure T_Scene_Explore.DrawScene;
begin
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glClearDepth(1.0);
  glDepthFunc(GL_LESS);
  glDepthRange(0, 1);
  glEnable(GL_DEPTH_TEST);
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  SetCamera;
  Land.Draw;
  //DrawList.Draw;
  Window.SwapBuffers;
  inherited DrawScene;
end;

procedure T_Scene_Explore.SetCamera;
begin
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  gluPerspective(45, Window.Width/Window.Height, 0.1, 200);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  Camera.Angle[0] := Camera.Angle[0] - 90;
  Camera.Angle[1] := Camera.Angle[1] + 0;
  Camera.Angle[2] := Camera.Angle[2] + 90;
  glRotatef(Camera.Angle[0], 1, 0, 0);
  glRotatef(Camera.Angle[1], 0, 1, 0);
  glRotatef(Camera.Angle[2], 0, 0, 1);
  glTranslatef(-Camera.Pos[0], -Camera.Pos[1], -Camera.Pos[2]);
end;

end.

