unit _usual;

{$mode delphi}

interface

uses
  Classes, SysUtils, contnrs, gl, glext,  math;

type
  RVector2f = array[0..1] of Single;
  RVector3f = array[0..2] of Single;
  RVector2i = array[0..1] of Integer;
  RVector3i = array[0..2] of Integer;
  RVector4i = array[0..3] of Integer;
  RVector4f = array[0..3] of Single;

  RQuaternion = array[0..3] of Single;
  RMatrix16f = array[0..15] of Single;

  TObjectList = contnrs.TObjectList;

  { TVector3f }

  TVector3f = object
    function Get(Index: Integer): Single;
    procedure Put(Index: Integer; AValue: Single);
  public
    Data : RVector3f;
    procedure SetLength(ALength : Single);
    function Length : Single;
  published
    property Vector[Index: Integer] : Single read Get write Put; default;
  end;

function Lerp(A, B : Single; K : Single) : Single;

procedure ShaderLog(AText : AnsiString; AShader: GLUInt);
function GLGetErrorString : AnsiString;
procedure Log(ALogString: string; ADoFlush: boolean = false; ALogFile: string = 'Log.txt');
procedure LogFlushAll;

function GetRValue(APixel : DWORD) : Byte;
function GetGValue(APixel : DWORD) : Byte;
function GetBValue(APixel : DWORD) : Byte;

procedure AddVector(var A : RVector2f; B : RVector2f; Koeff : Double = 1.0); overload;
procedure AddVector(var A : RVector3f; B : RVector3f; Koeff : Double = 1.0); overload;
procedure AddDataf(var APointer : PSingle; AData : array of Single);

function GetDistance(A : RVector3f; B : RVector3f) : Single; overload;
function GetDistance(A : RVector2f; B : RVector2f) : Single; overload;

function AngleToPiRange(A : Single) : Single;

function GetMinAngle(A : Single; B : Single) : Single;
function GetAngleBetween(A, B : RVector2f) : Single; overload;
function GetAngleBetween(A, B : RVector3f) : Single; overload;

function GetTickCount : DWord;
function GetTickCountMks : DWord;

procedure WritelnToStream(AStream : TStream; S : AnsiString);
function ReadlnFromStream(AStream : TStream) : AnsiString;
var
  Path_Data : AnsiString;

implementation

{$IFDEF LINUX}
  uses baseunix, unix;
  var
    MTimer : Int64;
    MksTimer : Int64;
function GetTickCount : DWord;
var
  buf : tms;
  _tv : timeval;
  _tz : timezone;
begin
  FillChar(_tv, SizeOf(_tv), 0);
  FillChar(_tz, SizeOf(_tz), 0);
  fpgettimeofday(@_tv, @_tz);
  if MTimer = 0 then
    MTimer := _tv.tv_sec * 1000 + (_tv.tv_usec div 1000);
  Result := _tv.tv_sec * 1000 + (_tv.tv_usec div 1000) - MTimer;
  //Result := FpTimes(buf)*10;
end;

function GetTickCountMks : DWord;
var
  buf : tms;
  _tv : timeval;
  _tz : timezone;
begin
  FillChar(_tv, SizeOf(_tv), 0);
  FillChar(_tz, SizeOf(_tz), 0);
  fpgettimeofday(@_tv, @_tz);
  if MksTimer = 0 then
    MksTimer := _tv.tv_sec * 1000000 + _tv.tv_usec;
  Result := _tv.tv_sec * 1000000 + _tv.tv_usec - MksTimer;
  //Result := FpTimes(buf)*10;
end;


{$ENDIF}

{$IFDEF WINDOWS}
  uses windows;
var
  TimerFrequency : TLargeInteger;
  TimerCounterAtStart : TLargeInteger;
  TimerCounterMksAtStart : TLargeInteger;
function GetTickCount : DWORD;
var
  t : TLargeInteger;
begin
  QueryPerformanceCounter(t);
  Result := Round(1000 * ((t - TimerCounterAtStart) / TimerFrequency));
  //Result := windows.GetTickCOunt;
end;

function GetTickCountMks : DWORD;
var
  t : TLargeInteger;
begin
  QueryPerformanceCounter(t);
  Result := Round(1000000 * ((t - TimerCounterMksAtStart) / TimerFrequency));
  //Result := windows.GetTickCOunt;
end;

{$ENDIF}

procedure WritelnToStream(AStream: TStream; S: AnsiString);
begin
  AStream.Write(S[1], Length(S));
  AStream.WriteByte(13);
  AStream.WriteByte(10);
end;

function ReadlnFromStream(AStream: TStream): AnsiString;
var
  x : Char;
begin
  Result := '';
  x := #0;
  repeat
    if x <> #0 then
      Result := Result + x;
    x := Char(AStream.ReadByte);
  until (x = #13) or (x = #10);
  if AStream.Position <> AStream.Size then
    begin
      x := Char(AStream.ReadByte);
      if (x <> #13) and (x <> #10) then
        AStream.Position := AStream.Position - 1;
    end;
end;

function AngleToPiRange(A: Single): Single;
begin
  //Result := trunc(A/(2*Pi))*2*Pi;
  Result := frac(A/(2*Pi))*2*Pi;
  while Result > Pi do
    Result := Result - 2 * Pi;
  while Result < -Pi do
    Result := Result + 2 * Pi;
end;

function GetMinAngle(A : Single; B : Single) : Single;
begin
  A := AngleToPiRange(A);
  B := AngleToPiRange(B);
  Result := AngleToPiRange(B - A);
end;

function GetAngleBetween(A, B: RVector2f): Single;
begin
  Result := AngleToPiRange(arctan2(B[1] - A[1], B[0] - A[0]));
end;

function GetAngleBetween(A, B: RVector3f): Single;
begin
  Result := AngleToPiRange(arctan2(B[1] - A[1], B[0] - A[0]));
end;

function Lerp(A, B : Single; K : Single) : Single;
begin
  Result := A + (B - A) * K;
end;

function GetDistance(A : RVector2f; B : RVector2f) : Single;
begin
  Result := sqrt(sqr(B[0] - A[0]) + sqr(B[1] - A[1]));
end;

function GetDistance(A : RVector3f; B : RVector3f) : Single;
begin
  Result := sqrt(sqr(B[0] - A[0]) + sqr(B[1] - A[1]) + sqr(B[2] - A[2]));
end;

procedure AddVector(var A: RVector2f; B: RVector2f; Koeff: Double);
begin
  A[0] := A[0] + B[0] * Koeff;
  A[1] := A[1] + B[1] * Koeff;
end;

procedure AddVector(var A: RVector3f; B: RVector3f; Koeff: Double);
begin
  A[0] := A[0] + B[0] * Koeff;
  A[1] := A[1] + B[1] * Koeff;
  A[2] := A[2] + B[2] * Koeff;
end;

procedure AddDataf(var APointer : PSingle; AData : array of Single);
var
  i : integer;
begin
  for i := Low(AData) to High(AData) do
    begin
      APointer^ := AData[i];
      inc(APointer);
    end;
end;

function GetRValue(APixel : DWORD) : Byte;
begin
  Result := (APixel shr 16) and $FF;
end;

function GetGValue(APixel : DWORD) : Byte;
begin
  Result := (APixel shr 8) and $FF;
end;

function GetBValue(APixel : DWORD) : Byte;
begin
  Result := (APixel) and $FF;
end;

type

  // record
  RRecOfLog = record
    FileName : string;
    MemoryStream : TMemoryStream;
  end;

  PRecOfLog = ^RRecOfLog; // pointer на RRecoOfLog

  { T_Log }

  T_Log = class
  private
    Logs : TList; // of PRecOfLog
  public
    procedure Log(ALogString: string; ADoFlush: boolean = false; ALogFile: string = 'Log.txt');
    procedure Flush(AIndex : integer = -1);
    constructor Create;  // инициализация
    destructor Destroy; override;
  end;

{ T_Log }
var
  _Log : T_Log;

procedure ShaderLog(AText : AnsiString; AShader: GLUInt);
var
  Buf : array[1..1024] of Char;
  InfoLen : GLuint;
begin
  glGetShaderiv(AShader, GL_INFO_LOG_LENGTH, @InfoLen);
  if InfoLen > 0 then
    glGetShaderInfoLog(AShader, InfoLen, @InfoLen, @Buf[1]);
  Log(AText + PChar(@Buf[1]), True, 'shader.log');
end;

function GLGetErrorString: AnsiString;
begin
  //Result := gluErrorString(glGetError);
end;

procedure Log(ALogString: string; ADoFlush: boolean = false; ALogFile: string = 'Log.txt');
begin
  if _Log = nil then
    _Log := T_Log.Create;
  _Log.Log(ALogString, ADoFlush, ALogFile);
end;

procedure LogFlushAll;
begin
  if _Log = nil then
    _Log := T_Log.Create;
  _Log.Flush;
end;

{ TVector3f }

function TVector3f.Get(Index: Integer): Single;
begin
  Result := Data[Index];
end;

procedure TVector3f.Put(Index: Integer; AValue: Single);
begin
  Data[Index] := AValue;
end;

procedure TVector3f.SetLength(ALength: Single);
var
  xK : Single;
begin
  xK := ALength / Length;
  Data[0] := Data[0] * xK;
  Data[1] := Data[1] * xK;
  Data[2] := Data[2] * xK;
end;

function TVector3f.Length: Single;
begin
  Result := sqrt(sqr(Data[0]) + sqr(Data[1]) + sqr(Data[2]));
end;

{ TOList }


procedure T_Log.Log(ALogString: string; ADoFlush: boolean; ALogFile: string);
var
  xLog : PRecOfLog;
  i : integer;
  xIndex : integer;

  xI, xFlag: integer;
  xNewLog: boolean;
begin
  xIndex := -1;
  for i := 0 to Logs.Count - 1 do
    if PRecOfLog(Logs[i])^.FileName = ALogFile then
      begin
        xIndex := i;
        break;
      end;

  if xIndex = -1 then
    begin
      New(xLog);
      xLog^.FileName := ALogFile;
      xLog^.MemoryStream := TMemoryStream.Create;
      xIndex := Logs.Add(xLog);
    end;

  with PRecOfLog(Logs[xIndex])^ do
    begin
      ALogString := '[' + FormatDateTime('DD.MM.YYYY hh:mm', now) + '] ' + ALogString + #13;
      MemoryStream.Seek(0, soFromEnd);
      MemoryStream.Size:= MemoryStream.Size + length(ALogString);
      MemoryStream.Write(ALogString[1], length(ALogString));
      if ADoFlush then
        Flush(xIndex);
    end;

end;

procedure T_Log.Flush(AIndex: integer);
var
  xFileStream : TFileStream;
  xLog : PRecOfLog;
  i : integer;
begin
  if AIndex = -1 then
    begin
      for i := 0 to Logs.Count - 1 do
        Flush(i);
      Exit;
    end;

  if not DirectoryExists('logs') then
     CreateDir('logs');
  xLog := Logs[AIndex];
  if FileExists('logs' + PathDelim + xLog^.FileName) then
    xFileStream := TFileStream.Create('logs' + PathDelim + xLog^.FileName, fmOpenWrite)
  else
    xFileStream := TFileStream.Create('logs' + PathDelim + xLog^.FileName, fmCreate);
  xFileStream.Seek(0, soFromEnd);
  xLog^.MemoryStream.Position := 0;
  xFileStream.CopyFrom(xLog^.MemoryStream, xLog^.MemoryStream.Size);
  FreeAndNil(xFileStream);

  xLog^.MemoryStream.Size := 0;
end;

constructor T_Log.Create;
begin
  Logs := TList.Create;
end;

destructor T_Log.Destroy;
var
  i : integer;
  xLog : PRecOfLog;
begin
  Flush;
  for i := Logs.Count - 1 downto 0 do
    begin
      xLog := Logs[i];
      xLog^.MemoryStream.Free;
      xLog^.FileName := '';
      Dispose(xLog);
    end;
  Logs.Free;
  inherited Destroy;
end;

initialization
  Path_Data := 'data' + PathDelim;
  {$IFDEF WINDOWS}
  QueryPerformanceFrequency(TimerFrequency);
  QueryPerformanceCounter(TimerCounterAtStart);
  QueryPerformanceCounter(TimerCounterMksAtStart);
  {$ENDIF}
finalization
  if _Log <> nil then
    FreeAndNil(_Log);
end.

