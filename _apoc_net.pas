unit _apoc_net;

{$mode delphi}

interface

uses
  Classes, SysUtils, sockets,
  {$IFDEF LINUX}
  BaseUnix,
  {$ENDIF}
  {$IFDEF WINDOWS}
  WinSock,
  {$ENDIF}
  syncobjs, _usual;

type
  TSockAddr = sockets.TSockAddr;
  { T_Socket }

  T_Connection = class;

  R_OutRecord = record
    UID : Integer;
    Data : array of Byte;
    DataSize : integer;
    Address : TSockAddr;
    NeedConfirm : boolean;
    ConfirmSendTime : Cardinal;
    ConfirmAttemptsCountLeft : Integer;
    Connection : T_Connection;
  end;

  P_OutRecord = ^R_OutRecord;

  { T_Connection }

  T_Connection = class
    Addr : TSockAddr;
    OutList : TList; // of P_OutRecord
    ConfirmOutList : TList;
    LastSecondSended : Cardinal;
    LastSecondTime : Cardinal;
    NextSendedTime : Cardinal;
    LastReceivedTime : Cardinal;
    MaxBytesPerSecond : Single;
    ConfirmIDGenerator : Integer;
    CritSection : TCriticalSection;
    constructor Create(AAddr : TSockAddr);
    procedure CheckLastSecond;
    function GetConfirmID : integer;
  end;

  T_Socket = class
    Port : WORD;
    Sock : Cardinal;
    InList : TList;
    InListIndex : integer;
    InListFilledIndex : integer;
    ReadBufSize : Integer;
    ReadBuf : array of Byte;
    Connections : TObjectList;
    ReadCritSection : TCriticalSection;
    constructor Create(APort : WORD);
    function FindConnection(AAddr : TSockAddr; ADoCreateNew : boolean = False) : T_Connection;
    function GetNextInPacket: P_OutRecord;
    procedure Read;
    procedure SendConfirmation(AConnection : T_Connection; AConfirmID : integer);
    procedure RemoveConfirmation(AConnection : T_Connection; AConfirmID : integer);

    //procedure Write(ATo : TSockAddr; AData : array of Byte); overload;
    procedure Write(AConnection : T_Connection; AMem : Pointer; ASize : integer; ANeedConfirm : Boolean = False; APure : boolean = false); overload;
    procedure SendAllData;
    procedure Run;
  end;

function Net_GetHostByName(AName : AnsiString) : Cardinal;
function Net_ReadInteger(var APointer : Pointer) : Integer;
function Net_ReadSingle(var APointer : Pointer) : Single;
function Net_ReadBoolean(var APointer: Pointer): Boolean;
procedure Net_WriteInteger(var APointer: Pointer; AValue : integer; var ASize : integer);
procedure Net_WriteSingle(var APointer: Pointer; AValue : Single; var ASize : integer);
procedure Net_WriteBoolean(var APointer: Pointer; AValue : Boolean; var ASize : integer);
function SameAddr(AAddr1, AAddr2 : TSockAddr) : boolean;
implementation
  uses
    Math, resolve;

function Net_GetHostByName(AName: AnsiString): Cardinal;
var
  xResolver : THostResolver;
begin
  ;
  xResolver := THostResolver.Create(nil);
  Result := xResolver.HostAddress.s_addr;
  xResolver.Free;
end;

function Net_ReadInteger(var APointer : Pointer) : Integer;
begin
  Result := PInteger(APointer)^;
  inc(APointer, SizeOf(Integer));
end;

function Net_ReadSingle(var APointer: Pointer): Single;
begin
  Result := PSingle(APointer)^;
  inc(APointer, SizeOf(Single));
end;

function Net_ReadBoolean(var APointer: Pointer): Boolean;
begin
  if PInteger(APointer)^ = 0 then
    Result := False
  else
    Result := True;
  inc(APointer, SizeOf(Integer));
end;

procedure Net_WriteInteger(var APointer: Pointer; AValue: integer;
  var ASize: integer);
begin
  PInteger(APointer)^ := AValue;
  inc(APointer, SizeOf(AValue));
  inc(ASize, SizeOf(AValue));
end;

procedure Net_WriteSingle(var APointer: Pointer; AValue: Single;
  var ASize: integer);
begin
  PSingle(APointer)^ := AValue;
  inc(APointer, SizeOf(AValue));
  inc(ASize, SizeOf(AValue));
end;

procedure Net_WriteBoolean(var APointer: Pointer; AValue: Boolean;
  var ASize: integer);
begin
  if AValue then
    PInteger(APointer)^ := 1
  else
    PInteger(APointer)^ := 0;
  inc(APointer, SizeOf(Integer));
  inc(ASize, SizeOf(Integer));
end;

function SameAddr(AAddr1, AAddr2: TSockAddr): boolean;
begin
  Result := (AAddr1.sin_addr.s_addr = AAddr2.sin_addr.s_addr)
          and (AAddr1.sin_port = AAddr2.sin_port);
end;

{ T_Connection }

constructor T_Connection.Create(AAddr: TSockAddr);
begin
  Addr := AAddr;
  OutList := TList.Create;
  ConfirmOutList := TList.Create;
  MaxBytesPerSecond := 200 * 1024;
  CritSection := TCriticalSection.Create;
  LastSecondTime := GetTickCount;
end;

procedure T_Connection.CheckLastSecond;
begin
  if GetTickCount - LastSecondTime > 1000 then
    begin
      LastSecondTime := GetTickCount;
      LastSecondSended := 0;
    end;
end;

function T_Connection.GetConfirmID: integer;
begin
  Result := ConfirmIDGenerator;
  inc(ConfirmIDGenerator);
  if ConfirmIDGenerator < 0 then
    ConfirmIDGenerator := 0;
end;

{ T_Socket }

constructor T_Socket.Create(APort: WORD);
var
  addr : TSockAddr;
  addrlen : TSockLen;
  i : integer;
  xRec : P_OutRecord;
  xNonBlock : LongInt;
begin
  Sock := fpsocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  FillChar(addr, SizeOf(addr), 0);
  addr.sa_family := AF_INET;
  addr.sin_addr.s_addr := INADDR_ANY;
  addr.sin_port := htons(APort);
  addrlen:= SizeOf(addr);
  fpbind(Sock, @addr, addrlen);
  {$IFDEF WINDOWS}
  xNonBlock := 1;
  ioctlsocket(Sock, FIONBIO, xNonBlock);
  {$ENDIF}
  {$IFDEF LINUX}
  FpFcntl(Sock, F_SetFl, O_NONBLOCK);
  {$ENDIF}
  ReadBufSize := 1024;
  InList := TList.Create;
  SetLength(ReadBuf, ReadBufSize);
  for i := 0 to 1024 do
    begin
      New(xRec);
      SetLength(xRec^.Data, ReadBufSize);
      InList.Add(xRec);
    end;
  InListIndex := -1;
  InListFilledIndex := -1;
  Connections := TObjectList.create(True);
  ReadCritSection := TCriticalSection.Create;
end;

function T_Socket.FindConnection(AAddr: TSockAddr; ADoCreateNew: boolean
  ): T_Connection;
var
  i : integer;
  xConnection : T_Connection;
begin
  Result := nil;
  for i := 0 to Connections.Count - 1 do
    begin
      xConnection := T_Connection(Connections[i]);
      if SameAddr(xConnection.Addr, AAddr) then
        begin
          Result := xConnection;
          Exit;
        end;
    end;
  if ADoCreateNew then
    begin
      xConnection := T_Connection.Create(AAddr);
      Connections.Add(xConnection);
      Result := xConnection;
      Exit;
    end;
end;

function T_Socket.GetNextInPacket: P_OutRecord;
begin
  Result := nil;
  ReadCritSection.Enter;
  if InListIndex <> InListFilledIndex then
    begin
      inc(InListIndex);
      if InListIndex > InList.Count - 1 then
        InListIndex := 0;
      Result := P_OutRecord(InList[InListIndex]);
    end;
  ReadCritSection.Leave;
end;

procedure T_Socket.Read;
var
  xFrom : TSockAddr;
  xFromLen : TSocklen;
  xReaded : integer;
  xRec : P_OutRecord;
  xConfirmID : Integer;
  xConfirmStatus : Byte;
  xConnection : T_Connection;
begin
  xConnection := nil;
  repeat
    xFromLen := SizeOf(xFrom);
    xReaded := fprecvfrom(Sock, @ReadBuf[0], ReadBufSize, 0, @xFrom, @xFromLen);
    if (xReaded <> -1) then
      begin
        if (xConnection = nil) or (not SameAddr(xConnection.Addr, xFrom)) then
          xConnection := FindConnection(xFrom);
        if xConnection = nil then
          begin
            xConnection := T_Connection.Create(xFrom);
            Connections.Add(xConnection);
          end;
        xConfirmStatus := ReadBuf[0];
        xConfirmID := PInteger(@ReadBuf[1])^;
        if xConfirmStatus = 0 then
          // подтверждения не надо, все норм
        else
          if xConfirmStatus = 1 then
            // нужно подтвердить
              SendConfirmation(xConnection, xConfirmID)
          else
            // 2 и более - пришло подтверждение
            begin
              RemoveConfirmation(xConnection, xConfirmID);
              Continue;
            end;
        ReadCritSection.Enter;
        inc(InListFilledIndex);
        if InListFilledIndex > InList.Count - 1 then
          InListFilledIndex := 0;
        xRec := InList[InListFilledIndex];
        Move(ReadBuf[5], xRec^.Data[0], xReaded - 5);
        xRec^.Address := xFrom;
        xRec^.Connection := xConnection;
        xRec^.DataSize := xReaded - 5;
        ReadCritSection.Leave;
      end;
  until xReaded = -1;
end;

procedure T_Socket.SendConfirmation(AConnection: T_Connection;
  AConfirmID: integer);
var
  xConfirm : array[0..4] of Byte;
begin
  xConfirm[0] := 2;
  PInteger(@xConfirm[1])^ := AConfirmID;
  while fpsendto(Sock, @xConfirm[0], 5, 0, @AConnection.Addr, SizeOf(AConnection.Addr)) = -1 do;
  //Write(AConnection, @xConfirm[0], 5, False, True);
end;

procedure T_Socket.RemoveConfirmation(AConnection: T_Connection;
  AConfirmID: integer);
var
  i : integer;
  xRec : P_OutRecord;
begin
  if AConfirmID < 0 then
    Exit;
  AConnection.CritSection.Enter;
  try
  for i := 0 to AConnection.ConfirmOutList.Count - 1do
    begin
      xRec := AConnection.ConfirmOutList[i];
      if xRec^.UID = AConfirmID then
        begin
          Dispose(xRec);
          AConnection.ConfirmOutList.Remove(xRec);
          Exit;
        end;
    end;

  finally
  AConnection.CritSection.Leave;
  end;
end;

{procedure T_Socket.Write(ATo: TSockAddr; AData: array of Byte);
var
  xRec : P_OutRecord;
begin
  New(xRec);
  xRec^.Address := ATo;
  SetLength(xRec^.Data, Length(AData));
  Move(AData[0], xRec^.Data[0], Length(AData));
  CritSection.Enter;
  OutList.Add(xRec);
  CritSection.Leave;
end;}

procedure T_Socket.Write(AConnection: T_Connection; AMem: Pointer;
  ASize: integer; ANeedConfirm: Boolean; APure: boolean);
var
  xRec : P_OutRecord;
begin
  New(xRec);
  xRec^.Address := AConnection.Addr;
  xRec^.NeedConfirm := ANeedConfirm;
  if not APure then
    ASize := ASize + 5;
  SetLength(xRec^.Data, ASize);
  if not APure then
    begin
      if ANeedConfirm then
        xRec^.Data[0] := 1
      else
        xRec^.Data[0] := 0;
      Move(AMem^, xRec^.Data[5], ASize - 5);
    end
  else
    Move(AMem^, xRec^.Data[0], ASize);
  AConnection.CritSection.Enter;
  AConnection.OutList.Add(xRec);
  AConnection.CritSection.Leave;
end;

procedure T_Socket.SendAllData;
var
  i : integer;
  j : integer;
  xOverSended : boolean;
  xRec : P_OutRecord;
  xBPS : Single;
  xConnection : T_Connection;
begin
  for i := 0 to Connections.Count - 1 do
    begin
      xConnection := T_Connection(Connections[i]);
      xOversended := False;
      xConnection.CheckLastSecond;
      if xConnection.OutList.Count > 0 then
        repeat
          xConnection.CritSection.Enter;
          xRec := xConnection.OutList[0]; // #ВНИМАНИЕ#: при одновременном Add из другого потока память может реаллоцироваться со сменой указателя, в результе вернется мусор
          xConnection.CritSection.Leave;
{          if xConnection.LastSecondSended > xConnection.MaxBytesPerSecond then
            begin
              xOverSended := True;
              break;
            end;}
          {if GetTickCountMks + 1000000 < xConnection.NextSendedTime then
            begin
              xOverSended := True;
              break;
            end;}
          if xRec^.NeedConfirm then
            begin
              xConnection.CritSection.Enter;
              xConnection.ConfirmOutList.Add(xRec);
              xRec^.UID := xConnection.GetConfirmID;
              PInteger(@xRec^.Data[1])^ := xRec^.UID;
              xRec^.ConfirmSendTime := GetTickCount;
              xRec^.ConfirmAttemptsCountLeft := 25;
              xConnection.CritSection.Leave;
            end;
          xConnection.LastSecondSended := xConnection.LastSecondSended + Length(xRec^.Data);
          xConnection.NextSendedTime := xConnection.NextSendedTime + Round(Length(xRec^.Data)/(xConnection.MaxBytesPerSecond/1000000));
          while fpsendto(Sock, @xRec^.Data[0], Length(xRec^.Data), 0, @xRec^.Address, SizeOf(xRec^.Address)) = -1 do;
          //writeln(Length(xRec^.Data));
          xConnection.CritSection.Enter;
          xConnection.OutList.Remove(xRec);
          xConnection.CritSection.Leave;
          if not xRec^.NeedConfirm then
            Dispose(xRec);
        until xConnection.OutList.Count <= 0;
      j := 0;
      // #ВНИМАНИЕ# : разобраться с синхронизацией из RemoveConfirmation
      if (not xOverSended) and (xConnection.ConfirmOutList.Count > 0) then
        repeat
          xRec := xConnection.ConfirmOutList[j];
          if (GetTickCount - xRec^.ConfirmSendTime) > 5000 then
            begin
              if GetTickCountMks < xConnection.NextSendedTime then
                begin
                  xOverSended := True;
                  break;
                end;
              while fpsendto(Sock, @xRec^.Data[0], Length(xRec^.Data), 0, @xRec^.Address, SizeOf(xRec^.Address)) = -1 do;
              xConnection.NextSendedTime := Round(GetTickCountMks + Length(xRec^.Data)/(xConnection.MaxBytesPerSecond/1000000));
              Dec(xRec^.ConfirmAttemptsCountLeft);
              xRec^.ConfirmSendTime := GetTickCount;
              xConnection.CritSection.Enter;
              if xRec^.ConfirmAttemptsCountLeft <= 0 then
                xConnection.ConfirmOutList.Remove(xRec)
              else
                inc(j);
              xConnection.CritSection.Leave;
            end
          else
            inc(j);
        until j > xConnection.ConfirmOutList.Count - 1;
    end;
end;

function RunSock(parameter : pointer) : ptrint;
var
  xSock : T_Socket;
  t : integer;
begin
  xSock := parameter;
  while true do
    begin
      Sleep(0);
      xSock.Read;
      xSock.SendAllData;
    end;
end;

procedure T_Socket.Run;
begin
  BeginThread(RunSock, Self);
end;

end.

