unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Spin, LazUTF8, math;

type

  { TForm1 }

  TForm1 = class(TForm)
    bFont: TButton;
    bRender: TButton;
    bSave: TButton;
    FontDialog1: TFontDialog;
    mChars: TMemo;
    pbFont: TPaintBox;
    SaveDialog1: TSaveDialog;
    seSize: TSpinEdit;
    seMax: TSpinEdit;
    procedure bFontClick(Sender: TObject);
    procedure bRenderClick(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

const
  DefaultChars = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM' +
                 'йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮёЁ' +
                 '1234567890' +
                 '`~!@#$%^&*()_+"№;:()+-=' +
                 '{}[]\|'',./<>?';
var
  Form1: TForm1;
  xSizes : array of Word;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  mChars.Text := DefaultChars;
end;

procedure TForm1.bFontClick(Sender: TObject);
begin
  if FontDialog1.Execute then
    begin
      mChars.Font := FontDialog1.Font;
      mChars.Font.Size := seSize.Value;
      bFont.Caption := FontDialog1.Font.Name;
    end;
end;

procedure TForm1.bRenderClick(Sender: TObject);
var
  i : integer;
  xX, xY : integer;
  xWidth,
  xHeight : integer;
  xMax : integer;
  xChar : AnsiString;
  xMaxHeight : integer;
begin
  Caption:='';
  xMax := seMax.Value;
  pbFont.Width := xMax;
  pbFont.Height := xMax;
  pbFont.Canvas.Font := mChars.Font;
  pbFont.Canvas.Font.Size := seSize.Value;
  pbFont.Canvas.Font.Color := clWhite;

  xX := 1;
  xY := 1;
  pbFont.Canvas.Brush.Color := 0;

  pbFont.Refresh;
  with pbFont do
    pbFont.Canvas.FillRect(0, 0, Width, Height);


  SetLength(xSizes, 4 * UTF8Length(mChars.Text));

  for i := 1 to UTF8Length(mChars.Text) do
    begin
      xChar := UTF8Copy(mChars.Text, i, 1);
      xWidth := pbFont.Canvas.GetTextWidth(xChar) + 1;
      xHeight := pbFont.Canvas.GetTextHeight(xChar) + 1;
      if xWidth + xX > xMax then
        begin
          xX := 1;
          xY := xY + xMaxHeight;
          xMaxHeight := 0;
        end;
      pbFont.Canvas.TextOut(xX, xY, xChar);

      xSizes[(i - 1) * 4 + 0] := xX;
      xSizes[(i - 1) * 4 + 1] := xY;
      xSizes[(i - 1) * 4 + 2] := xWidth;
      xSizes[(i - 1) * 4 + 3] := xHeight;
      xX := xX + xWidth;
      xMaxHeight := Max(xHeight, xMaxHeight);
    end;
end;

function GetBValue(APixel : Cardinal) : Byte;
begin
  Result := (APixel shr 16) and $FF;
end;

function GetGValue(APixel : Cardinal) : Byte;
begin
  Result := (APixel shr 8) and $FF;
end;

function GetRValue(APixel : Cardinal) : Byte;
begin
  Result := (APixel shr 0) and $FF;
end;

procedure TForm1.bSaveClick(Sender: TObject);
var
  i, j : integer;
  k : integer;
  xByte : Byte;
  xPixel : Byte;
  F : TMemoryStream;
begin
  if SaveDialog1.Execute then
    begin
      F := TMemoryStream.Create;
      F.WriteDWord(Length(mChars.Text));
      F.Write(mChars.Text[1], Length(mChars.Text));

      for i := 0 to High(xSizes) do
        F.WriteWord(xSizes[i]);
      F.WriteDWord(pbFont.Width);
      F.WriteDWord(pbFont.Height);

      for i := 0 to pbFont.Width - 1 do
        for j := 0 to pbFont.Height - 1do
          begin
            xByte := 0;

            xPixel := pbFont.Canvas.Pixels[i, j];
            xByte := Round(GetRValue(xPixel) + GetGValue(xPixel) + GetBValue(xPixel)) div 3;
            //xByte := Round(1 * GetRValue(xPixel) + 1 * GetGValue(xPixel) + 1 * GetBValue(xPixel));
            //0.3R + 0.59G + 0.11B
            F.WriteByte(xByte);
          end;
      F.SaveToFile(SaveDialog1.FileName);
    end;
end;

end.

