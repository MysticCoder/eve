unit _keys;

{$mode delphi}

interface
uses
  {$IFDEF WINDOWS}
  windows;
{$ENDIF}
{$IFDEF LINUX}
  keysym;
{$ENDIF}

const
{$IFDEF LINUX}
  MK_1 = XK_1;
  MK_2 = XK_2;
  MK_3 = XK_3;
  MK_4 = XK_4;
  MK_5 = XK_5;
  MK_6 = XK_6;
  MK_7 = XK_7;
  MK_8 = XK_8;
  MK_9 = XK_9;
  MK_0 = XK_0;
  MK_Q = XK_Q;
  MK_W = XK_W;
  MK_E = XK_E;
  MK_R = XK_R;
  MK_T = XK_T;
  MK_Y = XK_Y;
  MK_U = XK_U;
  MK_I = XK_I;
  MK_O = XK_O;
  MK_P = XK_P;
  MK_A = XK_A;
  MK_S = XK_S;
  MK_D = XK_D;
  MK_F = XK_F;
  MK_G = XK_G;
  MK_H = XK_H;
  MK_J = XK_J;
  MK_K = XK_K;
  MK_L = XK_L;
  MK_Z = XK_Z;
  MK_X = XK_X;
  MK_C = XK_C;
  MK_V = XK_V;
  MK_B = XK_B;
  MK_N = XK_N;
  MK_M = XK_M;
  MK_SPACE = XK_space;
  MK_ESCAPE = XK_Escape;
  MK_RETURN = XK_Return;
  MK_NUMPAD0 = XK_KP_Insert;
  MK_NUMPAD1 = XK_KP_End;
  MK_NUMPAD2 = XK_KP_Down;
  MK_NUMPAD3 = XK_KP_Page_Down;
  MK_NUMPAD4 = XK_KP_Left;
  MK_NUMPAD5 = XK_KP_Begin;
  MK_NUMPAD6 = XK_KP_Right;
  MK_NUMPAD7 = XK_KP_Home;
  MK_NUMPAD8 = XK_KP_Up;
  MK_NUMPAD9 = XK_KP_Page_Up;
  MK_LEFT = XK_Left;
  MK_RIGHT = XK_Right;
  MK_UP = XK_UP;
  MK_DOWN = XK_DOWN;
  MK_LSHIFT = XK_Shift_L;
  MK_RSHIFT = XK_Shift_R;
  MK_BACKSPACE = XK_BackSpace;
{$ENDIF}

{$IFDEF WINDOWS}
  MK_1 = VK_1;
  MK_2 = VK_2;
  MK_3 = VK_3;
  MK_4 = VK_4;
  MK_5 = VK_5;
  MK_6 = VK_6;
  MK_7 = VK_7;
  MK_8 = VK_8;
  MK_9 = VK_9;
  MK_0 = VK_0;
  MK_Q = VK_Q;
  MK_W = VK_W;
  MK_E = VK_E;
  MK_R = VK_R;
  MK_T = VK_T;
  MK_Y = VK_Y;
  MK_U = VK_U;
  MK_I = VK_I;
  MK_O = VK_O;
  MK_P = VK_P;
  MK_A = VK_A;
  MK_S = VK_S;
  MK_D = VK_D;
  MK_F = VK_F;
  MK_G = VK_G;
  MK_H = VK_H;
  MK_J = VK_J;
  MK_K = VK_K;
  MK_L = VK_L;
  MK_Z = VK_Z;
  MK_X = VK_X;
  MK_C = VK_C;
  MK_V = VK_V;
  MK_B = VK_B;
  MK_N = VK_N;
  MK_M = VK_M;
  MK_SPACE = VK_SPACE;
  MK_ESCAPE = VK_ESCAPE;
  MK_RETURN = VK_RETURN;
  MK_NUMPAD0 = VK_NUMPAD0;
  MK_NUMPAD1 = VK_NUMPAD1;
  MK_NUMPAD2 = VK_NUMPAD2;
  MK_NUMPAD3 = VK_NUMPAD3;
  MK_NUMPAD4 = VK_NUMPAD4;
  MK_NUMPAD5 = VK_NUMPAD5;
  MK_NUMPAD6 = VK_NUMPAD6;
  MK_NUMPAD7 = VK_NUMPAD7;
  MK_NUMPAD8 = VK_NUMPAD8;
  MK_NUMPAD9 = VK_NUMPAD9;
  MK_LEFT = VK_Left;
  MK_RIGHT = VK_Right;
  MK_UP = VK_UP;
  MK_DOWN = VK_DOWN;
  MK_LSHIFT = VK_LSHIFT;
  MK_RSHIFT = VK_RSHIFT;
  MK_BACKSPACE = VK_BACK;
{$ENDIF}
implementation

end.

