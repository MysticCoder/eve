unit _apoc_unit_controller;

{$mode delphi}

interface

uses
  Classes, SysUtils, _apoc_unit, _keys;

type
    T_UnitControllerPlayer = class(T_UnitController)
      procedure Update; override;
  end;

implementation
  uses _Window, _scene_apoc, _usual, math;

  { T_UnitControllerPlayer }

procedure T_UnitControllerPlayer.Update;
var
  xMousePos : RVector2f;
  xScene : T_Scene_Apoc;
begin
  if Window.IsConsoleMode then
    Exit;
  with (Host.Physic as T_PhysicUnit), T_LogicUnit(Host.Logic) do
    begin
      wasd[1] := Window.Keys[MK_W];
      wasd[2] := Window.Keys[MK_A];
      wasd[3] := Window.Keys[MK_S];
      wasd[4] := Window.Keys[MK_D];
      DoWeaponShoot := Window.Mouse.LMB;
      xScene := T_Scene_Apoc(Window.Scene);
      xMousePos := xScene.MouseToMap;
      T_LogicUnit(Host.Logic).LookAngle := arctan2(xMousePos[1] - Host.Physic.Pos[1], xMousePos[0] - Host.Physic.Pos[0]);
      {if Window.Keys[MK_SPACE] then
        (Host.Logic as T_LogicUnit).Weapon.Shoot(Host.Physic.Pos[0], Host.Physic.Pos[1], 0);}
    end;
end;

end.

