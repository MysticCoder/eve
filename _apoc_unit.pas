unit _apoc_unit;

{$mode delphi}

interface

uses
  Classes, SysUtils, gl, {glu, }glext, _usual, _apoc_map, _timer, _model;

type
  T_Unit = class;
  T_LogicBase = class;
  T_PhysicBase = class;
  T_GraphBase = class;
  T_UnitController = class;
  T_Weapon = class;
  { T_Base }


  TBaseFunction = (BF_Physic, BF_Logic, BF_Graph);

  { T_NetDiffRec }

  T_NetDiffRec = class
    pVariable : Pointer;
    pOldVariable : Pointer;
    Size : integer;
    FromClientAgreed : Boolean;
    constructor Create(AVariable : Pointer; ASize : integer);
    function IsDiffed : boolean;
    procedure Update;
  end;

  T_Base = class
    Timer : T_Timer;
    Host : T_Unit;
    Map : T_Map;
    Logic : T_LogicBase;
    Physic : T_PhysicBase;
    Graph  : T_GraphBase;
    BaseFunction : TBaseFunction;
    DiffList : TObjectList;
    constructor Create(AHost : T_Unit); virtual;
    procedure Init; virtual;
    procedure Update; virtual;
    procedure Draw; virtual;
    function IsNetDiffVarsChanged : boolean;
    procedure RegisterNetDiffVar(APointer : Pointer; ASize : integer; AFromClientAgreed : boolean = False);
    class procedure SetMaskBit(var AWord: Word; ABitNumber: Byte; ABoolean: Boolean
      );
    class function GetMaskBit(var AWord : Word; ABitNumber: Byte) : Boolean;
    procedure UpdateNetStream(AStream : TMemoryStream; ADiffStream : TMemoryStream); virtual;
    procedure LoadFromNetStream(var AInd : Pointer); virtual;
    procedure LoadFromDiffNetStream(var AInd : Pointer); virtual;
  end;



  T_BaseClass = class of T_Base;

  { T_PhysicBase }

  T_PhysicBase = class(T_Base)
    Angle : Single;
    OldPos : R_UnitPos;
    Pos : R_UnitPos;
    Speed : Single;
    Radius : Single;
    constructor Create(AHost : T_Unit); override;
    destructor Destroy; override;
    procedure Update; override;
    procedure OnPosChange(AOldPos : R_UnitPos); virtual;
    procedure LoadFromNetStream(var AInd : Pointer); override;
  end;

  { T_PhysicUnit }

  T_PhysicUnit = class(T_PhysicBase)
    DoMove : boolean;
    MoveAngle : Single;
    constructor Create(AHost : T_Unit); override;
    procedure Update; override;
    procedure CheckMove;
    procedure Move;
    procedure LoadFromNetStream(var AInd : Pointer); override;
  end;

  { T_LogicBase }

  T_LogicBase = class(T_Base)
    Name : AnsiString;
    Invisibility : Single; // маскировка
    constructor Create(AHost : T_Unit); override;
    procedure ReceiveDamage(AFrom : T_Unit; ADmg : Single); virtual;
    procedure Update; override;
    procedure Die; virtual;
    procedure LoadFromNetStream(var AInd : Pointer); override;
  end;

  { T_LogicShoot }

  T_LogicShoot = class(T_LogicBase)
    Owner : T_Unit;
    Dmg : Single;
    constructor Create(AHost : T_Unit); override;
    procedure Detonate(AUnits : TObjectList);
  end;

  { T_LogicUnit }

  T_LogicUnit = class(T_LogicBase)
    Weapon : T_Weapon;
    Controller : T_UnitController;
    VisibleUnits : TObjectList;
    wasd : array[1..4] of boolean;
    DoWeaponShoot : boolean;
    Life : Single;
    MaxLife : Single;
    Visibility : Single; // обзор
    FOVAngle : Single;
    posture : Byte; // поза(лежит, сидит, стоит, бежит)
    LookAngle : Single;
    LookSector : array of R_UnitPos;
    constructor Create(AHost : T_Unit); override;
    procedure Update; override;
    procedure CheckVisible;
    procedure BuildLookSector;
    procedure ReceiveDamage(AFrom : T_Unit; ADmg : Single); override;
    function IsVisible(AUnit : T_Unit) : boolean; overload;
    function IsVisible(APos : R_UnitPos; AInvisibility : Single; OMaxDistance : PSingle) : boolean; overload;
    procedure LoadFromNetStream(var AInd : Pointer); override;
  end;

  { T_GraphBase }

  T_GraphBase = class(T_Base)
    procedure Draw; override;
    procedure DrawSimple; virtual;
  end;


  { T_GraphTree }

  T_GraphTree = class(T_GraphBase)
    procedure Draw; override;
  end;

  { T_GraphUnit }

  T_GraphUnit = class(T_GraphBase)
    Model : T_Mesh;
    constructor Create(AHost : T_Unit); override;
    procedure Draw; override;
    procedure DrawSimple; override;
    procedure DrawSector;
  end;

  { T_Unit }

  T_Unit = class
    ID : integer;
    Timer : T_Timer;
    Map : T_Map;
    Bases : TObjectList;
    Logic : T_LogicBase;
    Physic : T_PhysicBase;
    Graph : T_GraphBase;
    Died : Boolean;
    NetStream : TMemoryStream;
    NetDiffStream : TMemoryStream;
    IsNetDiffVarsChangedCalced : boolean;
    DoRefreshNet : boolean;
    constructor BaseCreate;
    constructor Create(ABases : array of T_Base); virtual; overload;
    constructor Create(ABases : array of T_BaseClass); virtual; overload;
    constructor CreateFromNetStream(var AInd : Pointer); virtual;
    destructor Destroy; override;
    procedure Init;
    procedure Update; virtual;
    procedure Draw; virtual;
    procedure UpdateNetStream;
    procedure LoadFromNetStream(var AInd : Pointer);
    procedure LoadFromNetDiffStream(var AInd : Pointer);
    function IsNetDiffVarsChanged : boolean;

  end;

  T_UnitController = class
    Host : T_Unit;
    procedure Update; virtual; abstract;
  end;

  { T_UnitControllerPlayer }


  { T_PhysicShoot }

  T_PhysicShoot = class(T_PhysicBase)
    procedure Update; override;
  end;

  { T_Shoot }

  T_Shoot = class(T_Unit)
    constructor Create(AOwner : T_Unit; APos : R_UnitPos; AAngle : Single; ASpeed : Single);
  end;

  T_ShootClass = class of T_Shoot;

  { T_Weapon }

  T_Weapon = class
    Timer : T_Timer;
    Owner : T_Unit;
    Accuracy : Single;
    MinAccuracy : Single;
    MaxAccuracy : Single;
    Ammo : Integer;
    MaxAmmo : Integer;
    ShootCooldawn : Single;
    ShootTimer : Single;
    ShootSpeed : Single;
    Damage : Single;
    ShootClass : T_ShootClass;
    constructor Create(ATimer : T_Timer);
    procedure Shoot(AX, AY : Single; AAngle : Single);
    procedure Update;
  end;

  function BaseToInteger(AClass : TClass) : integer;
  function IntegerToBase(AInteger : Integer) : T_BaseClass;
implementation
  uses
    math, _window, _apoc_net, _scene_apoc,
    _apoc_unit_controller;
var
  BaseClassesList : TList;
  BaseClassesIndexes : TList;
  procedure InitBaseClassesIndexes;
    procedure _Add(AClass : T_BaseClass; AIndex : Integer);
    begin
      BaseClassesList.Add(AClass);
      BaseClassesIndexes.Add(Pointer(AIndex));
    end;

  begin
    BaseClassesList := TList.Create;
    BaseClassesIndexes := TList.Create;
    _Add(T_GraphBase, 1);
    _Add(T_GraphUnit, 2);
    _Add(T_GraphTree, 3);
    _Add(T_PhysicBase, 11);
    _Add(T_PhysicUnit, 12);
    _Add(T_PhysicShoot, 13);
    _Add(T_LogicBase, 21);
    _Add(T_LogicUnit, 22);
    _Add(T_LogicShoot, 23);
  end;

  function BaseToInteger(AClass: TClass): integer;
  var
    i : integer;
  begin
    for i := 0 to BaseClassesList.Count - 1 do
      if BaseClassesList[i] = AClass then
        begin
          Result := Integer(BaseClassesIndexes[i]);
          Exit;
        end;
  end;

  function IntegerToBase(AInteger: Integer): T_BaseClass;
  var
    i : integer;
  begin
    for i := 0 to BaseClassesIndexes.Count - 1 do
      if Integer(BaseClassesIndexes[i]) = AInteger then
        begin
          Result := T_BaseClass(BaseClassesList[i]);
          Exit;
        end;
  end;

{ T_NetDiffRec }

constructor T_NetDiffRec.Create(AVariable: Pointer; ASize: integer);
begin
  pVariable := AVariable;
  Size := ASize;
  pOldVariable := GetMem(Size);
  Update;
end;

function T_NetDiffRec.IsDiffed: boolean;
begin
  Result := not CompareMem(pVariable, pOldVariable, Size);
end;

procedure T_NetDiffRec.Update;
begin
  Move(pVariable^, pOldVariable^, Size);
end;

{ T_LogicShoot }

constructor T_LogicShoot.Create(AHost: T_Unit);
begin
  inherited Create(AHost);
  RegisterNetDiffVar(@Dmg, SizeOf(Dmg));
end;

procedure T_LogicShoot.Detonate(AUnits: TObjectList);
var
  i : integer;
  xUnit : T_Unit;
begin
  for i := 0 to AUnits.Count - 1 Do
    begin
      xUnit := T_Unit(AUnits[i]);
      if (xUnit <> Host) and (xUnit <> Owner) then
        xUnit.Logic.ReceiveDamage(Owner, Dmg);
    end;
  Die;
end;

{ T_GraphUnit }

constructor T_GraphUnit.Create(AHost: T_Unit);
begin
  inherited Create(AHost);
  if Window.IsConsoleMode then
    Exit;
  Model := T_Mesh.Create;
  Model.LoadFromFile('data' + PathDelim + 'model.txt');
end;

procedure T_GraphUnit.Draw;
var
  i : integer;
begin
  if ((Logic as T_LogicUnit).Controller <> nil) and ((Logic as T_LogicUnit).Controller is T_UnitControllerPlayer) then
    begin
      DrawSimple;
      DrawSector;
      for i := 0 to T_LogicUnit(Logic).VisibleUnits.Count - 1 do
        T_GraphUnit(T_Unit(T_LogicUnit(Logic).VisibleUnits[i]).Graph).DrawSimple;
    end;
end;

procedure T_GraphUnit.DrawSimple;
var
  //q : PGLUquadric;
  xLen : Single;
  xLive : SIngle;
  xHeight : Single;
begin
  if T_PhysicUnit(Physic).DoMove then
    Model.StartAnimation('run', True)
  else
    Model.StartAnimation('stay', True);
  Model.CalcAnimation(Scene.ft);
  //glColor3f(0, 1, 0);
//  q := gluNewQuadric();
  glTranslatef(Physic.Pos[0], Physic.Pos[1], Physic.Pos[2]);
  glRotatef((Logic as T_LogicUnit).LookAngle * 180/Pi, 0, 0, 1);
  Model.Draw;
  glRotatef(-(Logic as T_LogicUnit).LookAngle * 180/Pi, 0, 0, 1);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_TEXTURE_2D);
  {glPointSize(10);
  glBegin(GL_POINTS);
  glVertex3f(0, 0, 0);
  glEnd;}
//  gluSphere(q, Physic.Radius, 8, 8);
  glTranslatef(-Physic.Pos[0], -Physic.Pos[1], -Physic.Pos[2]);
//  gluDeleteQuadric(q);
  xLen := 0.7;
  xHeight := 0.2;

  glColor3f(1, 0, 0);
  glBegin(GL_QUADS);
    glVertex3f(Physic.Pos[0] - xLen, Physic.Pos[1] + 1, 0.1 + Physic.Pos[2]);
    glVertex3f(Physic.Pos[0] + xLen, Physic.Pos[1] + 1, 0.1 + Physic.Pos[2]);
    glVertex3f(Physic.Pos[0] + xLen, Physic.Pos[1] + 1 + xHeight, 0.1 + Physic.Pos[2]);
    glVertex3f(Physic.Pos[0] - xLen, Physic.Pos[1] + 1 + xHeight, 0.1 + Physic.Pos[2]);
  glEnd;

  xLive := T_LogicUnit(Logic).Life / T_LogicUnit(Logic).MaxLife;
  glColor3f(0, 1, 0);
  glBegin(GL_QUADS);
    glVertex3f(Physic.Pos[0] - xLen, Physic.Pos[1] + 1, 0.2 + Physic.Pos[2]);
    glVertex3f(Physic.Pos[0] - xLen + 2 * xLen * xLive, Physic.Pos[1] + 1, 0.2 + Physic.Pos[2]);
    glVertex3f(Physic.Pos[0] - xLen + 2 * xLen * xLive, Physic.Pos[1] + 1 + xHeight, 0.2 + Physic.Pos[2]);
    glVertex3f(Physic.Pos[0] - xLen, Physic.Pos[1] + 1 + xHeight, 0.2 + Physic.Pos[2]);
  glEnd;
  glColor3f(1, 1, 1);
end;

procedure T_GraphUnit.DrawSector;
var
  i : integer;
  xCount : integer;
  xAngle : SIngle;
  xMaxDistance : Single;
  xPos : R_UnitPos;
begin
  xCount := 50;
  glColor4f(1, 1, 1, 1);
  xMaxDistance := (Logic as T_LogicUnit).Visibility;
//  glPolygonMode(GL_FRONT, GL_LINE_LOOP);
  glBegin(GL_LINE_LOOP);
//    glVertex3f(Physic.Pos[0], Physic.Pos[1], 0.1);
    for i := 0 to High(T_LogicUnit(Logic).LookSector) do
      begin
//        xAngle := Physic.Angle - (Logic as T_LogicUnit).FOVAngle + (i/(xCount-1)) * (Logic as T_LogicUnit).FOVAngle * 2;
//        xPos[0] := Physic.Pos[0] + 40 * cos(xAngle);
//        xPos[1] := Physic.Pos[1] + 40 * sin(xAngle);
//        glVertex3f(Physic.Pos[0] + xMaxDistance * cos(xAngle), Physic.Pos[1] + xMaxDistance * sin(xAngle), 0.1);
        glVertex3f(T_LogicUnit(Logic).LookSector[i][0], T_LogicUnit(Logic).LookSector[i][1], 0.01);
      end;
//    glVertex3f(Physic.Pos[0], Physic.Pos[1], 0.1);
  glEnd;
  glPolygonMode(GL_FRONT, GL_FILL);
end;

{ T_LogicUnit }


constructor T_LogicUnit.Create(AHost: T_Unit);
begin
  inherited Create(AHost);
  VisibleUnits := TObjectList.Create(False);
  Visibility := 20;
  Invisibility := 0.3;
  FOVAngle := 45 * Pi / 180;
  MaxLife := 100;
  Life := MaxLife / 2;
  RegisterNetDiffVar(@LookAngle, SizeOf(LookAngle), True);
  RegisterNetDiffVar(@wasd[1], SizeOf(wasd[1]) * Length(wasd), True);
  RegisterNetDiffVar(@DoWeaponShoot, SizeOf(DoWeaponShoot), True);
  RegisterNetDiffVar(@Life, SizeOf(Life));
  RegisterNetDiffVar(@MaxLife, SizeOf(MaxLife));
  RegisterNetDiffVar(@Visibility, SizeOf(Visibility));
  RegisterNetDiffVar(@FOVAngle, SizeOf(FOVAngle));
  RegisterNetDiffVar(@posture, SizeOf(Posture));
end;

procedure T_LogicUnit.Update;
var
  i : integer;
begin
  inherited Update;
  CheckVisible;
  if Life <= 0 then
    Exit;
  if Controller <> nil then
    Controller.Update;
  if Weapon = nil then
    Weapon := T_Weapon.Create(Timer);
  Weapon.ShootClass := T_Shoot;
  Weapon.Owner := Host;
  Weapon.ShootSpeed := 5;
  Weapon.ShootCooldawn:= 0.5;
  Weapon.Update;
  if DoWeaponShoot then
    Weapon.Shoot(Physic.Pos[0], Physic.Pos[1], LookAngle);
  //Weapon.Shoot(Physic.Pos[0], Physic.Pos[1], Random * 2 * Pi);}
end;

procedure T_LogicUnit.CheckVisible;
var
  i : integer;
  xUnit : T_Unit;
begin
  VisibleUnits.Clear;
  for i := 0 to Map.Units.Count - 1 do
    begin
      xUnit := T_Unit(Map.Units[i]);
      if IsVisible(xUnit) then
        VisibleUnits.Add(xUnit);
    end;
  if (Controller <> nil) and (Controller is T_UnitControllerPlayer) then
    BuildLookSector;
end;

procedure T_LogicUnit.BuildLookSector;
var
  xCount : integer;
  i : integer;
  xPos : R_UnitPos;
  xAngle : SIngle;
  xMaxDistance : Single;
begin
  xCount := 100;
  SetLength(LookSector, xCount);
  LookSector[0] := Physic.Pos;
  for i := 1 to xCount - 1 do
    begin
      xAngle := LookAngle - FOVAngle + 2 * FOVAngle * (i - 1)/(xCount-1);
      xPos[0] := Physic.Pos[0] + Cos(xAngle) * Visibility * 0.99;
      xPos[1] := Physic.Pos[1] + Sin(xAngle) * Visibility * 0.99;
      xMaxDistance := 10;
      IsVisible(xPos, 0, @xMaxDistance);
      LookSector[i][0] := Physic.Pos[0] + Cos(xAngle) * xMaxDistance;
      LookSector[i][1] := Physic.Pos[1] + Sin(xAngle) * xMaxDistance;
    end;
end;

procedure T_LogicUnit.ReceiveDamage(AFrom: T_Unit; ADmg: Single);
begin
  Life := Max(0, Life - ADmg);
end;

function T_LogicUnit.IsVisible(AUnit: T_Unit): boolean;
begin
  Result := IsVisible(AUnit.Physic.Pos, AUnit.Logic.Invisibility, nil);
end;

function T_LogicUnit.IsVisible(APos: R_UnitPos; AInvisibility: Single;
  OMaxDistance: PSingle): boolean;
var
  xAngleFOV : Single;
  xAngle : SIngle;
  xDistance : Single;
  dx, dy : Single;
  dz : Single;
  xRealDZ : Single;
  xShag : Single;
  xPos : R_UnitPos;
  xCurX, xCurY : Integer;
  xNewX, xNewY : Integer;
begin
  Result := False;
  if OMaxDistance <> nil then
    OMaxDistance^ := 0;
  xAngle := GetAngleBetween(Physic.Pos, APos);
  xAngleFOV := GetMinAngle(LookAngle, xAngle);
  if abs(xAngleFOV) > FOVAngle then
    Exit;
  xDistance := GetDistance(Physic.Pos, APos) - AInvisibility;
  if xDistance > Visibility then
    Exit;
  xShag := 0.2;
  dx := xShag * cos(xAngle);
  dy := xShag * Sin(xAngle);
  dz := 0;
  xRealDZ := 0;
  xPos := Physic.Pos;
  xCurX := Trunc(xPos[0]);
  xCurY := Trunc(xPos[1]);
  repeat
    xPos[0] := xPos[0] + dx;
    xPos[1] := xPos[1] + dy;
    xNewX := Trunc(xPos[0]);
    xNewY := Trunc(xPos[1]);
    if (xNewX <> xCurX) or (xNewY <> xCurY) then
      begin
        xCurX := xNewX;
        xCurY := xNewY;
        dz := dz + (Visibility - dz) * Map.LightPassPower(xPos);
      end;
    dz := dz + xShag;
    xRealDZ := xRealDZ + xShag;
  until (dz >= Visibility) or (xRealDZ >= xDistance);
  if xRealDZ >= xDistance then
    Result := True;
  if OMaxDistance <> nil then
    OMaxDistance^ := xRealDZ;
end;


procedure T_LogicUnit.LoadFromNetStream(var AInd: Pointer);
begin
  inherited LoadFromNetStream(AInd);
end;

{ T_Weapon }

constructor T_Weapon.Create(ATimer: T_Timer);
begin
  Timer := ATimer;
end;

procedure T_Weapon.Shoot(AX, AY: Single; AAngle: Single);
var
  xPos : R_UnitPos;
begin
  if not Window.IsConsoleMode then
    Exit;
  if ShootTimer < ShootCooldawn then
    Exit;
  ShootTimer := 0;
  AAngle := AAngle + Accuracy - 2 * Random * Accuracy;
  xPos[0] := AX + cos(AAngle);
  xPos[1] := AY + sin(AAngle);
  ShootClass.Create(Owner, xPos, AAngle, ShootSpeed);
end;

procedure T_Weapon.Update;
begin
  ShootTimer := ShootTimer + Timer.dt;
  if ShootTimer > ShootCooldawn then
    ShootTimer := ShootCooldawn;
end;

{ T_Shoot }

constructor T_Shoot.Create(AOwner: T_Unit; APos: R_UnitPos; AAngle: Single;
  ASpeed: Single);
begin
  inherited Create([T_PhysicShoot, T_LogicShoot, T_GraphBase]);
  AOwner.Map.AddUnit(Self);
  Init;
  (Logic as T_LogicShoot).Owner := AOwner;
  (Logic as T_LogicShoot).Dmg := 10;
  (Physic as T_PhysicShoot).Pos := APos;
  (Physic as T_PhysicShoot).Radius := 0.1;
  (Physic as T_PhysicShoot).Angle := AAngle;
  (Physic as T_PhysicShoot).Speed := ASpeed;
  UpdateNetStream;
  DoRefreshNet := True;
  //Scene.;
end;

{ T_PhysicShoot }

procedure T_PhysicShoot.Update;
var
  xPos : R_UnitPos;
  dx, dy : Single;
  xDistance : Single;
  xShag : Single;
  dz : Single;
  xUnits : TObjectList;
begin
  inherited Update;
  writeln(Pos[0]:3:3,' ',Pos[1]:3:3);
  xShag := 0.2;
  dx := xShag * Cos(Angle);
  dy := xShag * Sin(Angle);
  dz := 0;
  xDistance := Speed * Timer.dt;
  xPos := Pos;
  repeat
    xPos[0] := xPos[0] + dx;
    xPos[1] := xPos[1] + dy;
    dz := dz + xShag;
    if dz >= xDistance then
      begin
        xPos[0] := Pos[0] + xDistance * cos(Angle);
        xPos[1] := Pos[1] + xDistance * sin(Angle);
      end;

    if not Map.IsPlacePass(xPos) then
      begin
        Pos := xPos;
        Logic.Die;
        Exit;
      end;

    xUnits := Map.GetUnitsOnRadius(xPos, Radius, [Host, (Logic as T_LogicShoot).Owner]);
    if xUnits.Count > 0 then
      begin
        Pos := xPos;
        (Logic as T_LogicShoot).Detonate(xUnits);
        xUnits.Free;
        Exit;
      end;
    xUnits.Free;

  until dz >= xDistance;
  Pos := xPos;
end;

{ T_LogicBase }

constructor T_LogicBase.Create(AHost: T_Unit);
begin
  inherited Create(AHost);
  RegisterNetDiffVar(@Invisibility, SizeOf(Invisibility));
end;


procedure T_LogicBase.LoadFromNetStream(var AInd: Pointer);
begin
  inherited LoadFromNetStream(AInd);
end;

procedure T_LogicBase.ReceiveDamage(AFrom: T_Unit; ADmg: Single);
begin

end;

procedure T_LogicBase.Update;
begin
  inherited Update;
end;

procedure T_LogicBase.Die;
begin
  Host.Died := True;
end;


{ T_PhysicUnit }

constructor T_PhysicUnit.Create(AHost: T_Unit);
begin
  inherited Create(AHost);
  RegisterNetDiffVar(@DoMove, SizeOf(DoMove));
  RegisterNetDiffVar(@MoveAngle, SizeOf(MoveAngle));
end;

procedure T_PhysicUnit.Update;
begin
  inherited Update;
  CheckMove;
  if DoMove then
    Move;
end;

procedure T_PhysicUnit.CheckMove;
  procedure _SetMoveAngle(AAngle : Single);
  begin
    DoMove := True;
    MoveAngle := AAngle;
  end;
begin
  DoMove := False;
  with Logic as T_LogicUnit do
  if wasd[1] then
    begin
      if wasd[4] then
        _SetMoveAngle(Pi/4)
      else
        if wasd[2] then
          _SetMoveAngle(3*Pi/4)
        else
          _SetMoveAngle(Pi/2);
    end
  else
    if wasd[3] then
      begin
        if wasd[4] then
          _SetMoveAngle(-Pi/4)
        else
          if wasd[2] then
            _SetMoveAngle(-3*Pi/4)
          else
            _SetMoveAngle(-Pi/2);
      end
    else
      if wasd[2] then
        _SetMoveAngle(Pi)
      else
        if wasd[4] then
          _SetMoveAngle(0);
end;

procedure T_PhysicUnit.Move;
var
  xPos : R_UnitPos;
  xPos2 : R_UnitPos;
  xPos3 : R_UnitPos;
begin
  if DoMove then
  begin
    FillChar(xPos, SizeOf(xPos), 0);
    FillChar(xPos2, SizeOf(xPos), 0);
    FillChar(xPos3, SizeOf(xPos), 0);
{    xPos[2] := 0;
    xPos2[2] := 0;
    xPos3[2] := 0;}
    xPos[0] := Pos[0] + cos(MoveAngle) * Speed * Timer.dt;
    xPos[1] := Pos[1] + sin(MoveAngle) * Speed * Timer.dt;
    xPos2[0] := Pos[0];
    xPos2[1] := xPos[1];
    xPos3[0] := xPos[0];
    xPos3[1] := Pos[1];
    if Map.IsPlacePass(xPos) then
      Pos := xPos
    else
      if Map.IsPlacePass(xPos2) then
        Pos := xPos2
      else
        if Map.IsPlacePass(xPos3) then
          Pos := xPos3;
  end;
end;


procedure T_PhysicUnit.LoadFromNetStream(var AInd: Pointer);
begin
  inherited LoadFromNetStream(AInd);
end;

{ T_PhysicBase }

constructor T_PhysicBase.Create(AHost: T_Unit);
begin
  inherited Create(AHost);
  RegisterNetDiffVar(@Pos[0], Length(Pos) * SizeOf(Pos[0]));
  RegisterNetDiffVar(@Angle, SizeOf(Angle));
  RegisterNetDiffVar(@Speed, SizeOf(Speed));
  RegisterNetDiffVar(@Radius, SizeOf(Radius));
end;

destructor T_PhysicBase.Destroy;
var
  xPos : R_UnitPos;
begin
  OnPosChange(OldPos);
  xPos := Pos;
  Pos[0] := -1000;
  Pos[1] := -1000;
  OnPosChange(xPos);
  Pos := xPos;
  inherited Destroy;
end;

procedure T_PhysicBase.Update;
begin
  if (Pos[0] <> OldPos[0]) or (Pos[1] <> OldPos[1]) then
    OnPosChange(OldPos);
  OldPos := Pos;
  inherited Update;
end;

procedure T_PhysicBase.OnPosChange(AOldPos: R_UnitPos);
var
  i, j : integer;
begin
  for i := Trunc(AOldPos[0] - Radius) to Trunc(AOldPos[0] + Radius) do
    for j := Trunc(AOldPos[1] - Radius) to Trunc(AOldPos[1] + Radius) do
      if (i >= 0) and (i <= Map.Width - 1) and (j >= 0) and (j <= Map.Height - 1) then
        Map.Data[i][j].Units.Remove(Host);
  for i := Trunc(Pos[0] - Radius) to Trunc(Pos[0] + Radius) do
    for j := Trunc(Pos[1] - Radius) to Trunc(Pos[1] + Radius) do
      if (i >= 0) and (i <= Map.Width - 1) and (j >= 0) and (j <= Map.Height - 1) then
        Map.Data[i][j].Units.Add(Host);
end;


procedure T_PhysicBase.LoadFromNetStream(var AInd: Pointer);
begin
  inherited LoadFromNetStream(AInd);
end;

{ T_GraphTree }

procedure T_GraphTree.Draw;
//var
//  q : PGLUquadric;
begin
  glColor3f(0, 1, 0);
//  q := gluNewQuadric();
  glTranslatef(Physic.Pos[0], Physic.Pos[1], Physic.Pos[2]);
//  gluSphere(q, 0.5, 8, 8);
  glPointSize(10);
  glBegin(GL_POINTS);
  glVertex3f(0, 0, 0);
  glEnd;
  glTranslatef(-Physic.Pos[0], -Physic.Pos[1], -Physic.Pos[2]);
//  gluDeleteQuadric(q);
  glColor3f(1, 1, 1);
end;

{ T_GraphBase }

procedure T_GraphBase.Draw;
begin
  inherited;
end;

procedure T_GraphBase.DrawSimple;
//var
//  q : PGLUquadric;
begin
  inherited;
  glTranslatef(Physic.Pos[0], Physic.Pos[1], Physic.Pos[2]);
  glPointSize(10);
  glBegin(GL_POINTS);
  glVertex3f(0, 0, 0);
  glEnd;
//  q := gluNewQuadric();
//  gluSphere(q, Physic.Radius, 4, 4);
//  gluDeleteQuadric(q);
  glTranslatef(-Physic.Pos[0], -Physic.Pos[1], -Physic.Pos[2]);
end;


{ T_Unit }

constructor T_Unit.BaseCreate;
begin
  NetStream := TMemoryStream.Create;
  NetDiffStream := TMemoryStream.Create;
  Bases := TObjectList.create(True);
end;

constructor T_Unit.Create(ABases: array of T_Base);
var
  i : integer;
begin
  BaseCreate;
  for i := 0 to High(ABases) do
    begin
      Bases.Add(ABases[i]);
      ABases[i].Host := Self;
    end;
  Init;
end;

constructor T_Unit.Create(ABases: array of T_BaseClass);
var
  i : integer;
begin
  BaseCreate;
  for i := 0 to high(ABases) do
    Bases.Add(ABases[i].Create(Self));
  Init;
end;

constructor T_Unit.CreateFromNetStream(var AInd: Pointer);
var
  xCount : integer;
  xBaseType : integer;
  xBase : T_Base;
  i : integer;
begin
  BaseCreate;
  xCount := Net_ReadInteger(AInd);
  if xCount = 0 then
    writeln('sos');
  for i := 0 to xCount - 1 do
    begin
      xBaseType := Net_ReadInteger(AInd);
      xBase := IntegerToBase(xBaseType).Create(Self);
      xBase.LoadFromNetStream(AInd);
      Bases.Add(xBase);
    end;
  Init;
end;

destructor T_Unit.Destroy;
var
  i : integer;
begin
  Bases.Free;
  inherited Destroy;
end;

procedure T_Unit.Init;
var
  i : integer;
begin
  for i := 0 to Bases.Count - 1 do
    begin
      T_Base(Bases[i]).Timer := Timer;
      T_Base(Bases[i]).Map := Map;
      T_Base(Bases[i]).Init;
      case T_Base(Bases[i]).BaseFunction of
        BF_Graph:
          Graph := T_GraphBase(Bases[i]);
        BF_Logic:
          Logic := T_LogicBase(Bases[i]);
        BF_Physic:
          Physic := T_PhysicBase(Bases[i]);
      end;
    end;
end;

procedure T_Unit.Update;
var
  i : integer;
begin
  DoRefreshNet := False;
  if (Window.IsConsoleMode) then
    begin
      UpdateNetStream;
      IsNetDiffVarsChangedCalced := IsNetDiffVarsChanged;
    end;
  if Died then
    begin
      Map.Units.Remove(Self);
      Exit;
    end;
  for i := 0 to Bases.Count - 1 do
    T_Base(Bases[i]).Update;
end;

procedure T_Unit.Draw;
var
  i : integer;
begin
  for i := 0 to Bases.Count - 1 do
    T_Base(Bases[i]).Draw;
end;

procedure T_Unit.UpdateNetStream;
var
  i : integer;
begin
  NetStream.Position := 0;
  NetStream.Size := 0;
  NetStream.Write(ID, SizeOf(ID));
  NetStream.Write(Bases.Count, SizeOf(Bases.Count));
  NetDiffStream.Position := 0;
  NetDiffStream.Size := 0;
  NetDiffStream.Write(ID, SizeOf(ID));
  NetDiffStream.Write(Bases.Count, SizeOf(Bases.Count));
  for i := 0 to Bases.Count - 1 do
    T_Base(Bases[i]).UpdateNetStream(NetStream, NetDiffStream);
end;

procedure T_Unit.LoadFromNetStream(var AInd: Pointer);
var
  xCount : integer;
  xBase : T_Base;
  i : integer;
begin
  xCount := Net_ReadInteger(AInd);
  for i := 0 to Bases.Count - 1 do
    begin
      xBase := T_Base(Bases[i]);
      Net_ReadInteger(AInd); // тип базы
      xBase.LoadFromNetStream(AInd);
    end;
end;

procedure T_Unit.LoadFromNetDiffStream(var AInd: Pointer);
var
  xCount : integer;
  xBase : T_Base;
  i : integer;
begin
  xCount := Net_ReadInteger(AInd);
  for i := 0 to Bases.Count - 1 do
    begin
      xBase := T_Base(Bases[i]);
      xBase.LoadFromDiffNetStream(AInd);
    end;
end;

function T_Unit.IsNetDiffVarsChanged: boolean;
var
  i : integer;
  xBase : T_Base;
begin
  Result := True;
  for i := 0 to Bases.Count - 1 do
    begin
      xBase := T_Base(Bases[i]);
      if xBase.IsNetDiffVarsChanged then
        Exit;
    end;
  Result := False;
end;

{ T_Base }

constructor T_Base.Create(AHost: T_Unit);
begin
  DiffList := TObjectList.create(True);
  Host := AHost;
  if (Self is T_GraphBase) then
    BaseFunction := BF_Graph;
  if (Self is T_LogicBase) then
    BaseFunction := BF_Logic;
  if (Self is T_PhysicBase) then
    BaseFunction := BF_Physic;
end;

procedure T_Base.Init;
var
  i : integer;
begin
  for i := 0 to Host.Bases.Count - 1 do
    begin
      Case T_Base(Host.Bases[i]).BaseFunction of
        BF_Graph :
          Graph := T_GraphBase(Host.Bases[i]);
        BF_Logic :
          Logic := T_LogicBase(Host.Bases[i]);
        BF_Physic :
          Physic := T_PhysicBase(Host.Bases[i]);
      end;
    end;
end;

procedure T_Base.Update;
var
  i : integer;
begin
  for i := 0 to DiffList.Count - 1 do
    T_NetDiffRec(DiffList[i]).Update;
end;

procedure T_Base.Draw;
begin

end;

function T_Base.IsNetDiffVarsChanged: boolean;
var
  i : integer;
  xRec : T_NetDiffRec;
begin
  Result := False;
  for i := 0 to DiffList.Count - 1 do
    begin
      xRec := T_NetDiffRec(DiffList[i]);
      if xRec.IsDiffed then
        begin
          Result := True;
          Exit;
        end;
    end;
end;

procedure T_Base.RegisterNetDiffVar(APointer: Pointer; ASize: integer;
  AFromClientAgreed: boolean);
var
  xRec : T_NetDiffRec;
begin
  xRec := T_NetDiffRec.Create(APointer, ASize);
  xRec.FromClientAgreed := AFromClientAgreed;
  DiffList.Add(xRec);
end;

class procedure T_Base.SetMaskBit(var AWord: Word; ABitNumber: Byte;
  ABoolean: Boolean);
begin
  if ABoolean then
    AWord := AWord or (1 shl ABitNumber - 1)
  else
    AWord := AWord and not(1 shl ABitNumber - 1);
end;

class function T_Base.GetMaskBit(var AWord: Word; ABitNumber: Byte): Boolean;
begin

end;

procedure T_Base.UpdateNetStream(AStream: TMemoryStream;
  ADiffStream: TMemoryStream);
var
  i : integer;
  xDiffRec : T_NetDiffRec;
  xDiffCount : Byte;
begin
  AStream.Write(BaseToInteger(ClassType), SizeOf(Integer));

  xDiffCount := 0;
  for i := 0 to DiffList.Count - 1 do
    begin
      xDiffRec := T_NetDiffRec(DiffList[i]);
      if xDiffRec.IsDiffed then
        inc(xDiffCount);
    end;

  ADiffStream.WriteByte(xDiffCount);
  for i := 0 to DiffList.Count - 1 do
    begin
      xDiffRec := T_NetDiffRec(DiffList[i]);
      AStream.Write(xDiffRec.pVariable^, xDiffRec.Size);
      if xDiffRec.IsDiffed then
        begin
          ADiffStream.WriteByte(i);
          ADiffStream.Write(xDiffRec.pVariable^, xDiffRec.Size);
        end;
    end;

end;

procedure T_Base.LoadFromNetStream(var AInd: Pointer);
var
  i : integer;
  xDiffRec : T_NetDiffRec;
begin
  for i := 0 to DiffList.Count - 1 do
    begin
      xDiffRec := T_NetDiffRec(DiffList[i]);
      if (not Window.IsConsoleMode) or (Window.IsConsoleMode and xDiffRec.FromClientAgreed) then
        Move(AInd^, xDiffRec.pVariable^, xDiffRec.Size);
      inc(AInd, xDiffRec.Size);
    end;
end;

procedure T_Base.LoadFromDiffNetStream(var AInd: Pointer);
var
  i : integer;
  xDiffRec : T_NetDiffRec;
  xDiffInd : Byte;
  xDiffCount : Byte;
begin
  xDiffCount := PByte(AInd)^;
  inc(AInd, 1);
  for i := 0 to xDiffCount - 1 do
    begin
      xDiffInd := PByte(AInd)^;
      inc(AInd, 1);
      xDiffRec := T_NetDiffRec(DiffList[xDiffInd]);
      if (not Window.IsConsoleMode) or (Window.IsConsoleMode and xDiffRec.FromClientAgreed) then
        Move(AInd^, xDiffRec.pVariable^, xDiffRec.Size);
      inc(AInd, xDiffRec.Size);
    end;
end;

initialization
  InitBaseClassesIndexes;
end.

