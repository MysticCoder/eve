unit _space_stealth;

{$mode delphi}

interface

uses
  _window, _usual, classes, gl, glu, glext, _keys, math, _model, sysutils, _textures, _audio, inifiles;

type

  { T_Unit }

  T_Asteroid = class;
  T_WarderUnit = class;

  T_Unit = class
    Pos : RVector2f;
    Angle : Single;
    Speed : Single;
    MaxTurnUnits : integer;
    TurnUnits : integer;
    SoundSource : PSoundSource;
    Live : Integer;
    MaxLive : Integer;
    LastLive : Integer;
    UnderAttack : Boolean;
    Radius : Single;
    constructor Create;
    destructor Destroy; override;
    procedure DoMoveTo(APos : RVector2f);
    procedure Update; virtual;
    function IsTurnComplete : boolean;
    procedure InitTurn;
    function FindNearAsteroid : T_Asteroid;
    procedure Draw; virtual; abstract;
  end;

  { T_Asteroid }

  T_Asteroid = class(T_Unit)
    AngleSpeed : Single;
    Selected : boolean;
    constructor Create;
    procedure Update; override;
    procedure Draw; override;
  end;

  { T_PlayerUnit }

  T_PlayerUnit = class(T_Unit)
    Asteroid : T_Asteroid;
    AsteroidAngle : Single;
    AsteroidToMove : T_Asteroid;
    AsteroidToMoveAngle : Single;
    Status : integer;
    MaxDistance : Single;
    WarderToAttack : T_WarderUnit;
    EndJump : boolean;
    EndGo : Boolean;
    SuperPower : Boolean;
    SuperPowerQueryTime : SIngle;
    constructor Create;
    procedure Update; override;
    procedure Draw; override;
    procedure DoAttack(AWarder : T_WarderUnit);
    procedure DrawMaxDistanceCircle;
    procedure SetDestination(ATarget : T_Asteroid; AAngle : Single);
    procedure AttackWarder(AWarder : T_WarderUnit);
    procedure QuerySuperPower;
  end;

  { T_WarderUnit }

  T_WarderUnit = class(T_Unit)
    Asteroid : T_Asteroid;
    AsteroidAngle : Single;
    AsteroidToMove : T_Asteroid;
    AngleLook : Single;
    LengthLook : Single;
    Status : integer;
    indexAsteroid : Integer;
    LastPlayerPos : RVector2f;
    LastPlayerAngle : Single;
    LastLastPlayerPos : RVector2f;
    IsPlayerVisibleLastTurn : boolean;
    AttackDistance : Single;
    Radius : Single;
    constructor Create;
    procedure Draw; override;
    procedure DrawSector;
    procedure DrawPlayerFantom;
    procedure Update; override;
    procedure DoAttack(AUnit : T_PlayerUnit);
    function FindPossiblePlayerAsteroid(APos1, APos2 : RVector2f) : T_Asteroid;
    function IsPosVisible(APos: RVector2f; pMaxVisibleDistance: PSingle=
      nil): boolean;
    function IsPlayerVisible : boolean;
    function GetDistanationAsteroid: T_Asteroid;
    procedure SetDistanation(ATarget: T_asteroid; AAngle: single);
  end;

  { T_Space_Stealth }

  T_Space_Stealth = class(T_Scene)
    OffsetX,
    OffsetY,
    Scale : Single;
    TargetCameraX,
    TargetCameraY,
    TargetScale : Single;
    DoMoveCamera : boolean;
    OffsetXDown, OffsetYDown : Integer;
    MapControl : TS_Control;
    Asteroids : TList; // of T_Asteroid
    Player : T_PlayerUnit;
    Warders : TList;
    SelectedAsteroid : T_Asteroid;
    SelectedAsteroidAngle : Single;
    SelectedWarder : T_WarderUnit;
    PlayerTurn : boolean;
    ModelSpider : T_Mesh;
    texAsteroid : Cardinal;
    texFon : Cardinal;
    SoundSource : PSoundSource;
    SuperPowerControl : TS_Control;
    Shader : T_Shader;
    function GetRealXY(AMouseX, AMouseY: Single): RVector2f;
    procedure GenerateAsteroidsSpheric(ACount : integer);
    function IsPlaceFree(APos : RVector2f; ARadius : Single) : boolean;
    procedure DrawFantom;
    procedure OnChar(AChar : String);
    procedure InitScene; override;
    procedure UpdateScene; override;
    procedure DrawScene; override;
    procedure FreeScene; override;
    procedure ReloadScene;
    procedure SetLight;
    procedure DrawFon;
    procedure OnMapMouseDown(AX, AY: Integer; AButton: T_MouseButton);
    procedure OnMapMouseMove(AX, AY: Integer);
    procedure OnMapMouseWheel(AValue: Integer);
    procedure MoveCamera(AX, AY, AScale : Single);

  end;

var
  Scene : T_Space_Stealth;
const
  CameraSpeed = 100;
  SuperPowerChance = 0.05 / 2;
  MaxSuperPowerQueryTime = 1;
  SuperPowerStrength = 100;
  ST_PLANTED = 0;
  ST_MOVE = 1;
  ST_ATTACK_WARDER = 2;

  ST_ATTACK_ASTEROID = 0;
  ST_EAT_ASTEROID = 1;
  ST_ALERT = 2;

  ASTEROID_LIVE = 100;
  WARDER_ANGLE_LOOK = 45;
  WARDER_LENGTH_LOOK = 150;
implementation

  uses
    _space_stealth_scenes;
function AngleToAroundZero(AAngle : Single) : Single;
begin
  while AAngle > Pi do
    AAngle := AAngle - 2 * Pi;
  while AAngle < -Pi do
    AAngle := AAngle + 2 * Pi;
  Result := AAngle;
end;

function GetAngleBetweenPos(APos1, APos2 : RVector2f) : Single;
begin
  Result := arctan2((APos2[1] - APos1[1]) , (APos2[0] - APos1[0]));
end;

function GetPosOnAsteroid(AAsteroid : T_Asteroid; AAngle : Single) : RVector2f;
begin
  Result[0] := AAsteroid.Pos[0] + 0.95 * AAsteroid.Radius * Cos(AAngle);
  Result[1] := AAsteroid.Pos[1] + 0.95 * AAsteroid.Radius * Sin(AAngle);
end;

function Distance(APoint1, APoint2: RVector2f): Single;
begin
  Result := sqrt(sqr(APoint1[0] - APoint2[0]) + sqr(APoint1[1] - APoint2[1]));
end;

{ T_Unit }

constructor T_Unit.Create;
begin
  MaxTurnUnits := 300;
end;

destructor T_Unit.Destroy;
begin
  SoundServ.Stop(SoundSource);
  inherited Destroy;
end;

procedure T_Unit.DoMoveTo(APos: RVector2f);
var
  xDistance : Single;
  xSpeed : Single;
  xAngle : Single;
begin
  xDistance := Distance(Pos, APos);
  xSpeed := Min(Speed, xDistance / Scene.dt);
  xAngle := arctan2((APos[1] - Pos[1]) , (APos[0] - Pos[0]));
  Pos[0] := Pos[0] + xSpeed * Cos(xAngle) * Scene.dt;
  Pos[1] := Pos[1] + xSpeed * Sin(xAngle) * Scene.dt;
end;

procedure T_Unit.Update;
begin
  if Live < LastLive then
    UnderAttack := True
  else
    UnderAttack := False;
  LastLive := Live;
end;

function T_Unit.IsTurnComplete: boolean;
begin
  Result := TurnUnits >= MaxTurnUnits;
end;

procedure T_Unit.InitTurn;
begin
  TurnUnits := 0;
end;

{ T_WarderUnit }

constructor T_WarderUnit.Create;
var
  xAsteroid : T_Asteroid;
  xAngle : Single;
begin
  inherited;
  Angle := 200*pi/180;
  Speed := 10;
  LengthLook := WARDER_LENGTH_LOOK;
  AngleLook := WARDER_ANGLE_LOOK;
  Status := ST_ATTACK_ASTEROID;
  AttackDistance := 10;
  Radius := 4;
  repeat
    Pos[0] := 500 - Random * 1000;
    Pos[1] := 500 - Random * 1000;
  until FindNearAsteroid = nil;
  xAsteroid := GetDistanationAsteroid;
  if xAsteroid <> nil then
  begin
    xAngle := GetAngleBetweenPos(pos, xAsteroid.pos);
    SetDistanation( xAsteroid, xAngle );
  end;
  Live := 800;
end;

procedure T_WarderUnit.Draw;
var
  q : PGLUquadric;
begin
  glColor3f(1, 0.4, 0);
  if Self = Scene.SelectedWarder then
    glColor3f(1, 0.4, 1);
  if UnderAttack then
    if Random < 0.2 then
      glColor3f(1, 0, 0);
  q := gluNewQuadric;
  glTranslatef( pos[0], pos[1] , 0);
  gluSphere(q, Radius, 12, 12);
  glTranslatef( -pos[0], -pos[1] , 0);
  gluDeleteQuadric(q);

  if (Status = ST_ALERT) and (not IsPlayerVisibleLastTurn) then
    DrawPlayerFantom;
  DrawSector;
end;

procedure T_WarderUnit.DrawSector;
var
  i : integer;
  xPos : RVector2f;
  xAngle : Single;
  xMaxK : integer;
  xMaxDistance : Single;
begin
  glDisable(GL_LIGHTING);
  glColor4f(1, 0.7, 0, 0.2);
  glEnable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_DST_COLOR);
  //glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  glBegin(GL_TRIANGLE_FAN);
  xMaxK := 200;
    glVertex3f(Pos[0], Pos[1], 0);
    for i := -xMaxK to xMaxK do
      begin
        xAngle := Angle + (i / (xMaxK + 1)) * AngleLook * Pi / 180;
        xPos[0] := pos[0] + cos(xAngle) * LengthLook * 0.99;
        xPos[1] := pos[1] + sin(xAngle) * LengthLook * 0.99;
        if not IsPosVisible(xPos, @xMaxDistance) then
          begin
            xPos[0] := pos[0] + cos(xAngle) * xMaxDistance;
            xPos[1] := pos[1] + sin(xAngle) * xMaxDistance;
          end;
        glVertex3f(xPos[0], xPos[1], 0);
      end;
  glEnd;
  {glBegin(GL_TRIANGLES);
  glVertex3d( pos[0], pos[1], 0 );
  glVertex3d( pos[0] + (cos(Angle + (AngleLook*pi/180))*LengthLook) , pos[1] + (sin(Angle + (AngleLook*pi/180))*LengthLook), 0 );
  glVertex3d( pos[0] + (cos(Angle - (AngleLook*pi/180))*LengthLook) , pos[1] + (sin(Angle - (AngleLook*pi/180))*LengthLook), 0 );
  glEnd;                }
  glColor4f(1, 1, 1, 1);
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glEnable(GL_LIGHTING);
end;

procedure T_WarderUnit.DrawPlayerFantom;
var
  q : PGLUquadric;
begin
  glPushMatrix;
  glColor3f(0, 0.4, 0);
  q := gluNewQuadric;
  glTranslatef(LastPlayerPos[0], LastPlayerPos[1], 0);
  glRotatef(-90, 1, 0, 0);
  glRotatef(90 - AsteroidAngle * 180 / Pi, 0, 1, 0);
  glScalef(2, 2, 2);
  Scene.ModelSpider.Draw;
  gluDeleteQuadric(q);
  glColor3f(1, 1, 1);
  glPopMatrix;
end;

procedure T_WarderUnit.Update;
var
  xDistance : Single;
  xAngle : Single;
  xSpeed : Single;
  xDestPos : RVector2f;
  xAsteroid : T_Asteroid;
  xDX : Single;
  xDY : Single;
  xPos : RVector2f;
begin
  inherited;
  if IsPlayerVisible then
    begin
      writeln('Visible' + ' ', random(10));
      if Status <> ST_ALERT then
      begin
        SoundSource := SoundServ.LoadSource('alarm.ogg', SoundSource) ;
        SoundServ.Play(SoundSource);
      end;

      Status := ST_ALERT;
      LastLastPlayerPos := LastPlayerPos;
      LastPlayerPos := Scene.Player.Pos;
      LastPlayerAngle := Scene.Player.AsteroidAngle;
      Angle := GetAngleBetweenPos(Pos, LastPlayerPos);
      IsPlayerVisibleLastTurn := True;
    end
  else
    if IsPlayerVisibleLastTurn then
      begin
        IsPlayerVisibleLastTurn := False;
        xDX := (LastPlayerPos[0] - LastLastPlayerPos[0]);
        xDY := (LastPlayerPos[1] - LastLastPlayerPos[1]);
        LastLastPlayerPos := LastPlayerPos;
        LastPlayerPos[0] := LastPlayerPos[0] + xDX;
        LastPlayerPos[1] := LastPlayerPos[1] + xDY;
        Angle := GetAngleBetweenPos(Pos, LastPlayerPos);;
      end;
  if TurnUnits >= MaxTurnUnits then
    Exit;
  inc(TurnUnits);

  xAsteroid := FindNearAsteroid;
  if xAsteroid <> nil then
    begin
      xAngle := GetAngleBetweenPos(Pos, xAsteroid.Pos);
      xAngle := xAngle + Pi;
      xPos[0] := Pos[0] + 4 * cos(xAngle);
      xPos[1] := Pos[1] + 4 * sin(xAngle);
      DoMoveTo(xPos);
    end;

  Case Status of
    ST_ALERT:
      begin
        AsteroidToMove := nil;
        if ((IsPlayerVisible) and (Distance(Pos, LastPlayerPos) > AttackDistance)) or ((not IsPlayerVisible) and (Distance(Pos, LastPlayerPos) > Radius)) then
          DoMoveTo(LastPlayerPos)
        else
          begin
            if IsPlayerVisible then
//              (if Distance(Pos, Scene.Player.Pos) <= AttackDistance then
                begin
                  DoAttack(Scene.Player);
                  Exit;
                  {Scene.Player.Pos[0] := 0;
                  Scene.Player.Pos[1] := 0;}
                end
            else
              begin
                AsteroidToMove := FindPossiblePlayerAsteroid(LastLastPlayerPos, LastPlayerPos);
                Status := ST_ATTACK_ASTEROID;
              end;
          end;
      end;
    ST_ATTACK_ASTEROID:
      begin
        if AsteroidToMove = nil then
          AsteroidToMove := GetDistanationAsteroid;
        DoMoveTo(AsteroidToMove.Pos);
        Angle := GetAngleBetweenPos(Pos, AsteroidToMove.Pos);

        xDistance := Distance(Pos, AsteroidToMove.Pos) - AsteroidToMove.Radius;
        if xDistance <= AttackDistance then
          begin
            Status := ST_EAT_ASTEROID;
          end;
      end;
    ST_EAT_ASTEROID:
      begin
        if AsteroidToMove <> nil then
          begin
            if SoundServ.GetStateSource(SoundSource) <> $1012 then
            begin
              SoundSource := SoundServ.LoadSource('eat.ogg', SoundSource);
              SoundServ.Play(SoundSource);
            end;
            Dec(AsteroidToMove.Live);
            if AsteroidToMove.Live <= 0 then
              begin
                if Scene.Player.Asteroid = AsteroidToMove then
                  begin
                    Scene.ReloadScene;
                    Exit;
                  end;
                Scene.Asteroids.Remove(AsteroidToMove);
                AsteroidToMove := nil;;
              end;
          end;
        if AsteroidToMove = nil then
          begin
            AsteroidToMove := GetDistanationAsteroid;
            Status := ST_ATTACK_ASTEROID;
          end
      end;
  end;
end;

procedure T_WarderUnit.DoAttack(AUnit: T_PlayerUnit);
begin
  if Random < SuperPowerChance then
    Scene.Player.QuerySuperPower;
  if Scene.Player.SuperPower then
    begin
      TurnUnits := TurnUnits + SuperPowerStrength;
    end;
  Scene.Player.SuperPower := False;
  Dec(AUnit.Live);
  if AUnit.Live <= 0 then
    begin
      Scene.ReloadScene;
      Exit;
    end;
end;

function T_WarderUnit.FindPossiblePlayerAsteroid(APos1, APos2: RVector2f): T_Asteroid;
var
  i : integer;
  xAsteroid : T_Asteroid;
  xAngle : Single;
begin
  Result := nil;
  xAngle := GetAngleBetweenPos(APos1, APos2);
  for i := 0 to Scene.Asteroids.Count - 1 do
    begin
      xAsteroid := Scene.Asteroids[i];
      if (Abs(AngleToAroundZero(GetAngleBetweenPos(LastPlayerPos, xAsteroid.Pos) - xAngle)) < 2 * Pi / 180) then
        begin
          if Result = nil then
            Result := xAsteroid
          else
            if Distance(xAsteroid.Pos, LastPlayerPos) < Distance(Result.Pos, LastPlayerPos) then
              begin
                Result := xAsteroid;
              end;
        end;
    end;
end;

function T_WarderUnit.IsPosVisible(APos: RVector2f; pMaxVisibleDistance : PSingle = nil): boolean;
var
  xAnglePlayer: Single;
  xAngleAsteroid, xAngleTangent: Single;
  xPosTangent : RVector2f;
  xAsteroid: T_Asteroid;
  xDistance : Single;
  xAsteroidDistance : Single;
  i : integer;
begin
  Result := false;
  xDistance := Distance(Pos, APos);
  if pMaxVisibleDistance <> nil then
    pMaxVisibleDistance^ := xDistance;
  if xDistance > LengthLook then
    Exit;
  xAnglePlayer := AngleToAroundZero(GetAngleBetweenPos(Pos, APos));
  if Abs(AngleToAroundZero(xAnglePlayer - Angle)) < (AngleLook*pi/180) then
  //if (xAnglePlayer > (Angle - (AngleLook*pi/180))) and (xAnglePlayer < (Angle + (AngleLook*pi/180))) then
  begin
    for i := 0 to Scene.Asteroids.Count -1 do
    begin
      xAsteroid := Scene.Asteroids[i];
      if Distance(pos, xAsteroid.Pos) < xDistance then
      begin
        xAngleAsteroid := GetAngleBetweenPos(pos, xAsteroid.Pos);
        xAsteroidDistance := Distance(Pos, xAsteroid.Pos);

        xPosTangent[0] := xAsteroid.Pos[0] + cos(xAngleAsteroid + 270 * pi/180);
        xPosTangent[1] := xAsteroid.Pos[1] + sin(xAngleAsteroid + 270 * pi/180);
        xAngleTangent := GetAngleBetweenPos( pos, xPosTangent );
        xAngleTangent := xAngleAsteroid - xAngleTangent;
        xAngleTangent := Abs(AngleToAroundZero(arctan(xAsteroid.Radius / xAsteroidDistance)));
        //if Abs(xAnglePlayer - xAngleAsteroid) < xAngleTangent;
        if Abs(AngleToAroundZero(xAnglePlayer - xAngleAsteroid)) < xAngleTangent then
//        if ( xAnglePlayer > xAngleAsteroid + xAngleTangent ) or ( xAnglePlayer < xAngleAsteroid - xAngleTangent ) then
        begin
          if pMaxVisibleDistance = nil then
            exit
          else
            begin
              if pMaxVisibleDistance^ >= Distance(pos, xAsteroid.Pos) then
                begin
                  pMaxVisibleDistance^ := Distance(pos, xAsteroid.Pos);
                end;
            end;
        end;
      end;
    end;
    if (pMaxVisibleDistance <> nil) and (pMaxVisibleDistance^ < xDistance) then
      Result := False
    else
      result := true;
   // WriteLn('PlayerIsVisible' + FloatToStr(Pos[0]));
  end;
end;

function T_WarderUnit.IsPlayerVisible: boolean;
begin
  Result := IsPosVisible(Scene.Player.Pos);
end;

procedure T_WarderUnit.SetDistanation(ATarget: T_asteroid; AAngle: single);
begin
  AsteroidToMove := ATarget;
  Status := ST_ATTACK_ASTEROID;
end;

function T_WarderUnit.GetDistanationAsteroid: T_Asteroid;
var
  i : Integer;
  xAsteroid : T_Asteroid;
  xDistance, xMinDistance : Single;
begin
  Result := nil;
  xMinDistance := 0;
  For i :=0 to Scene.Asteroids.Count -1 do
  begin
    xAsteroid := Scene.Asteroids[i];
    xDistance := Distance(Pos, xAsteroid.Pos);
    if (xDistance < xMinDistance) or (xMinDistance = 0) then
    begin
      xMinDistance := xDistance;
      indexAsteroid := i;
      Result := Scene.Asteroids[i];
    end;
  end;
end;

{ T_PlayerUnit }

constructor T_PlayerUnit.Create;
begin
  inherited;
  Live := 400;
  Speed := 50;
  MaxDistance := 100;
  EndJump := true;
end;

procedure T_PlayerUnit.Update;
var
  xDistance : Single;
  xAngle : Single;
  xSpeed : Single;
  xDestPos : RVector2f;
  xNearAsteroid : T_Asteroid;

begin
  inherited;
  SuperPowerQueryTime := SuperPowerQueryTime + Scene.dt;
  if SuperPowerQueryTime > MaxSuperPowerQueryTime then
    Scene.SuperPowerControl.Visible := False;
  if (Scene.SuperPowerControl.Visible) and (Window.Keys[mk_SPACE]) then
    begin
      Scene.SuperPowerControl.Visible := False;
      SuperPower := True;
    end;

  if TurnUnits >= MaxTurnUnits then
    Exit;

  Case Status of
    ST_PLANTED :
      if Asteroid <> nil then
      begin
        Pos := GetPosOnAsteroid(Asteroid, AsteroidAngle);
        if Window.Keys[MK_RETURN] then
          TurnUnits := MaxTurnUnits;
      end;
    ST_MOVE :
      begin
        inc(TurnUnits);
        xDestPos := GetPosOnAsteroid(AsteroidToMove, AsteroidToMoveAngle);
        if (Asteroid <> nil) then
          begin
            Scene.ModelSpider.StartAnimation('Move', True);
            if SoundServ.GetStateSource(SoundSource) <> $1012 then
            begin
              SoundSource := SoundServ.LoadSource('go.ogg', SoundSource) ;
              SoundServ.Play(SoundSource);
              EndGo := true;
            end;
            if Asteroid = AsteroidToMove then
              xAngle := AngleToAroundZero(AsteroidToMoveAngle)
            else
              xAngle := GetAngleBetweenPos(Asteroid.Pos, AsteroidToMove.Pos);

            if Abs(AngleToAroundZero(xAngle - AsteroidAngle)) > 10 * Pi/180 then
              begin
                AsteroidAngle := AsteroidAngle + Sign(AngleToAroundZero(xAngle - AsteroidAngle)) * 45 * Pi/180 * Scene.dt;
                Pos := GetPosOnAsteroid(Asteroid, AsteroidAngle);
                Exit;
              end;
            if (Asteroid = AsteroidToMove) then
              begin
                AsteroidToMove := nil;
                Status := ST_PLANTED;
                Scene.ModelSpider.StartAnimation('Wait', True);
                if EndGo then
                  SoundServ.Stop(SoundSource)
                else
                  EndGo := true;
                Exit;
              end;
          end;

        Asteroid := nil;

        Scene.ModelSpider.StartAnimation('Jump', True);
        if EndJump then
        begin
          endJump := false;
          EndGo := false;
          SoundServ.Stop(SoundSource);
          SoundSource := SoundServ.LoadSource('jump.ogg', SoundSource) ;
          SoundServ.Play(SoundSource);
        end;
        xDestPos := GetPosOnAsteroid(AsteroidToMove, AngleToAroundZero(AsteroidAngle + Pi));
        DoMoveTo(xDestPos);
        xNearAsteroid := FindNearAsteroid;
        if xNearAsteroid <> nil then
          begin
            endJump := true;
            SoundSource := SoundServ.LoadSource('jumpend.ogg', SoundSource) ;
            SoundServ.Play(SoundSource);
            Asteroid := xNearAsteroid;
            AsteroidAngle := GetAngleBetweenPos(xNearAsteroid.Pos, Pos);
          end;

{        if Distance(Pos, xDestPos) < 1e-3 then
          begin
            Status := ST_MOVE;
            AsteroidAngle := AsteroidAngle + Pi;
            Asteroid := AsteroidToMove;
            Scene.ModelSpider.StartAnimation('Move', True);
          end;}
      end;
    ST_ATTACK_WARDER:
      begin
        inc(TurnUnits);
        xDestPos := WarderToAttack.Pos;
        if (WarderToAttack <> nil) then
          begin
            if Asteroid <> nil then
              begin
                Scene.ModelSpider.StartAnimation('Move', True);
                xAngle := GetAngleBetweenPos(Asteroid.Pos, WarderToAttack.Pos);
                if Abs(AngleToAroundZero(xAngle - AsteroidAngle)) > 10 * Pi/180 then
                  begin
                    AsteroidAngle := AsteroidAngle + Sign(AngleToAroundZero(xAngle - AsteroidAngle)) * 45 * Pi/180 * Scene.dt;
                    Pos := GetPosOnAsteroid(Asteroid, AsteroidAngle);
                    Exit;
                  end;
                AsteroidToMove := Asteroid;
              end;

            Asteroid := nil;
            if Distance(Pos, WarderToAttack.Pos) <= WarderToAttack.Radius then
              begin
                AsteroidAngle := AngleToAroundZero(GetAngleBetweenPos(Pos, WarderToAttack.Pos) + Pi);
                Scene.ModelSpider.StartAnimation('Attack', True);
                DoAttack(WarderToAttack);
                Exit;
              end;
            Scene.ModelSpider.StartAnimation('Jump', True);
            xDestPos := WarderToAttack.Pos;
            DoMoveTo(xDestPos);
            xNearAsteroid := FindNearAsteroid;
            if xNearAsteroid <> nil then
              begin
                Asteroid := xNearAsteroid;
                AsteroidAngle := GetAngleBetweenPos(xNearAsteroid.Pos, Pos);
              end;
          end;

      end;
  end;
end;

function T_Unit.FindNearAsteroid: T_Asteroid;
var
  xAsteroid : T_Asteroid;
  i : integer;
  xDistance : Single;
begin
  Result := nil;
  for i := 0 to Scene.Asteroids.Count - 1 do
    begin
      xAsteroid := Scene.Asteroids[i];
      if Distance(xAsteroid.Pos, Pos) - xAsteroid.Radius <= Radius + 1e-3 then
        begin
          Result := xAsteroid;
          Exit;
        end;
    end;
end;

procedure T_PlayerUnit.Draw;
var
  q : PGLUquadric;
begin
  glPushMatrix;

  glColor3f(0, 1, 0);
  if UnderAttack then
    if Random < 0.2 then
      glColor3f(1, 0, 0);
  q := gluNewQuadric;
  glTranslatef(Pos[0], Pos[1], 0);
  glRotatef(-90, 1, 0, 0);
  glRotatef(90 - AsteroidAngle * 180 / Pi, 0, 1, 0);
  if TurnUnits < MaxTurnUnits then
    Scene.ModelSpider.CalcAnimation(Scene.ft);
  glScalef(2, 2, 2);
  glEnable(GL_LIGHT2);
  Scene.ModelSpider.Draw;
  glDisable(GL_LIGHT2);
  glRotatef(90, 1, 0, 0);
//  gluSphere(q, 2, 12, 12);
  glTranslatef(-Pos[0], -Pos[1], 0);
  gluDeleteQuadric(q);
  glColor3f(1, 1, 1);
  glPopMatrix;
  if Scene.PlayerTurn then
    DrawMaxDistanceCircle;
end;

procedure T_PlayerUnit.DoAttack(AWarder: T_WarderUnit);
begin
  if AWarder <> nil then
    begin
      if SoundServ.GetStateSource(SoundSource) <> $1012 then
        begin
          SoundSource := SoundServ.LoadSource('udar.ogg', SoundSource);
          SoundServ.Play(SoundSource);

        end;

      if Random < SuperPowerChance then
        QuerySuperPower;
      if SuperPower then
        Dec(AWarder.Live, SuperPowerStrength)
      else
        Dec(AWarder.Live);
      SuperPower := False;
      if AWarder.Live <= 0 then
        begin
          Scene.Warders.Remove(AWarder);
          AWarder.Free;
          WarderToAttack := nil;
          AsteroidAngle := AngleToAroundZero(AsteroidAngle + 90);
          Status := ST_MOVE;
        end;
    end;
end;

procedure T_PlayerUnit.DrawMaxDistanceCircle;
var
  i : integer;
  xMaxPoints : integer;
begin
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
  glBegin(GL_LINE_LOOP);
  xMaxPoints := 100;
  for i := 0 to xMaxPoints - 1 do
    begin
        glVertex3f(Pos[0] + MaxDistance * Cos(2 * Pi * i / (xMaxPoints - 1)), Pos[1] + MaxDistance * Sin(2 * Pi * i / (xMaxPoints - 1)), 0);
    end;
  glVertex3f(Pos[0] + MaxDistance * Cos(0), Pos[1] + MaxDistance * Sin(0), 0);
  glEnd;

  glEnable(GL_BLEND);
//  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBlendFunc(GL_SRC_ALPHA, GL_DST_COLOR);
  glColor4f(1, 1, 1, 0.0);
  glBegin(GL_TRIANGLE_FAN);
  glVertex3f(Pos[0], Pos[1], 0);
  for i := 0 to xMaxPoints - 1 do
    begin
        glVertex3f(Pos[0] + MaxDistance * Cos(2 * Pi * i / (xMaxPoints - 1)), Pos[1] + MaxDistance * Sin(2 * Pi * i / (xMaxPoints - 1)), 0);
    end;
  glVertex3f(Pos[0] + MaxDistance * Cos(0), Pos[1] + MaxDistance * Sin(0), 0);
  glEnd;
  glDisable(GL_BLEND);
  glColor4f(1, 1, 1, 1);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
end;

procedure T_PlayerUnit.SetDestination(ATarget: T_Asteroid; AAngle: Single);
var
  xPos : RVector2f;
begin
  xPos := GetPosOnAsteroid(ATarget, AAngle);
  if Distance(Pos, xPos) > MaxDistance then
    Exit;
  AsteroidToMove := ATarget;
  AsteroidToMoveAngle := AAngle;
  Status := ST_MOVE;

end;

procedure T_PlayerUnit.AttackWarder(AWarder: T_WarderUnit);
begin
  Status := ST_ATTACK_WARDER;
  WarderToAttack := AWarder;
end;

procedure T_PlayerUnit.QuerySuperPower;
begin
  Scene.SuperPowerControl.Visible := True;
  SuperPowerQueryTime := 0;
  //Scene.SuperPowerControl.SetTexture('');
end;

{ T_Asteroid }

constructor T_Asteroid.Create;
begin
  inherited Create;
  Angle := random * 360;
  AngleSpeed := 10 - random * 20;
end;

procedure T_Asteroid.Update;
begin
  inherited Update;
  Angle := Angle + Scene.dt * AngleSpeed;
end;

procedure T_Asteroid.Draw;
var
  q : PGLUquadric;
  xColor : Single;
begin
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  xColor := 0.2 + 1 * Live / MaxLive;
  if Selected then
    glColor4f(0, 0, xColor, xColor)
  else
    glColor4f(xColor, xColor, xColor, xColor);
  if UnderAttack then
    if Random < 0.2 then
      glColor4f(xColor, 0, 0, 1);
  q := gluNewQuadric;
  TextureManager.SetTexture(Scene.texAsteroid);

  gluQuadricTexture(q, GL_TRUE);
  gluQuadricOrientation(q, GLU_OUTSIDE);
  glTranslatef(Pos[0], Pos[1], 0);
  glRotatef(90, 0, 1, 0);
  glRotatef(Angle, 1, 0, 0);
  gluSphere(q, Radius, 12, 12);
  glRotatef(-Angle, 1, 0, 0);
  glRotatef(-90, 0, 1, 0);
  glTranslatef(-Pos[0], -Pos[1], 0);
  gluDeleteQuadric(q);
  TextureManager.UnSetTexture;
  glDisable(GL_BLEND);
  glColor4f(1, 1, 1, 1);
end;

{ T_Space_Stealth }

function T_Space_Stealth.GetRealXY(AMouseX, AMouseY: Single): RVector2f;
begin
  Result[0] := ((AMouseX - Window.Width / 2) * Scale + OffsetX);
  Result[1] := ({Window.Height - }(AMouseY - Window.Height / 2) * Scale+ OffsetY);
end;

procedure T_Space_Stealth.GenerateAsteroidsSpheric(ACount: integer);
var
  i : integer;
  xAsteroid : T_Asteroid;
  xAngle : Single;
  xDistance : Single;
begin
  while ACount > 0 do
    begin
      xAsteroid := T_Asteroid.Create;
      repeat
        xDistance := random * 500;
        xAsteroid.Radius := 4 + random * 10;
        xAngle := Random * 2 * Pi;
        xAsteroid.Pos[0] := xDistance * Cos(xAngle);
        xAsteroid.Pos[1] := xDistance * Sin(xAngle);
        xAsteroid.MaxLive := ASTEROID_LIVE * Round(xAsteroid.Radius);
        xAsteroid.Live := xAsteroid.MaxLive;
      until IsPlaceFree(xAsteroid.Pos, xAsteroid.Radius);
      Asteroids.Add(xAsteroid);
      Dec(ACount);
    end;
end;

function T_Space_Stealth.IsPlaceFree(APos: RVector2f; ARadius: Single): boolean;
var
  i : integer;
  xAsteroid : T_Asteroid;
  xDistance : Single;
begin
  Result := True;
  for i := 0 to Asteroids.Count - 1 do
    begin
      xAsteroid := Asteroids[i];
      xDistance := Distance(xAsteroid.Pos, APos);
      if xDistance <= (ARadius + xAsteroid.Radius) * 1.3 then
        begin
          Result := False;
          Exit;
        end;
    end;
end;

procedure T_Space_Stealth.DrawFantom;
var
  xPos : RVector2f;
  q : PGLUquadric;
begin
  xPos := GetPosOnAsteroid(SelectedAsteroid, SelectedAsteroidAngle);
  glColor3f(0, 0.5, 0);
  q := gluNewQuadric;
  glTranslatef(xPos[0], xPos[1], 0);
  gluSphere(q, 2, 12, 12);
  glTranslatef(-xPos[0], -xPos[1], 0);
  gluDeleteQuadric(q);
  glColor3f(1, 1, 1);
end;


procedure T_Space_Stealth.OnChar(AChar: String);
begin
  ReloadScene;
end;

procedure T_Space_Stealth.InitScene;
var
  i : integer;
  xIni : TIniFile;
begin
  inherited InitScene;
  Shader := T_Shader.Create('Shader' + PathDelim + 'simple');
  Shader.Use;
  SoundServer_Activate;
  SoundSource :=  SoundServ.LoadSource('background.ogg', nil, true);
  if SoundSource <> nil then
    SoundServ.Play(SoundSource);

  Scene := Self;
  Scale := 1;
  texAsteroid := TextureManager.LoadTexture('data' + DirectorySeparator + 'asteroid.tga');
  texFon := TextureManager.LoadTexture('data' + DirectorySeparator + 'space_fon.tga');
  ModelSpider := T_Mesh.Create;
  ModelSpider.LoadFromFile('data' + DirectorySeparator + 'model.txt');
  ModelSpider.StartAnimation('Move', True);
  ModelSpider.CalcAnimation(0);
  OffsetX := 0;
  OffsetY := 0;
  Asteroids := TList.Create;
  GenerateAsteroidsSpheric(100);
  Player := T_PlayerUnit.Create;
{  T_Asteroid(Asteroids[0]).Pos[0] := 0;
  T_Asteroid(Asteroids[0]).Pos[1] := 0;}
  Player.Asteroid := Asteroids[0];
  Player.AsteroidAngle := Random * 2 * Pi;
  Warders := TList.Create;
  for i := 1 to 10 do
    Warders.Add(T_WarderUnit.Create);
  MapControl := TS_Control.Create;
  MapControl.Anchor := tsa_Center;
  MapControl.Width := Window.Width;
  MapControl.Height := Window.Height;
  //MapControl.Visible:= False;
  Window.Controls.Add(MapControl);
  MapControl.RecalcPos;
  MapControl.OnMouseDownProc := OnMapMouseDown;
  MapControl.OnMouseMoveProc := OnMapMouseMove;
  MapControl.OnMouseWheelProc := OnMapMouseWheel;

  SuperPowerControl := TS_Control.Create;
  SuperPowerControl.Anchor := tsa_Bottom;
  SuperPowerControl.Width := 200;
  SuperPowerControl.Height := 100;
  SuperPowerControl.BaseY := -50;
  SuperPowerControl.RecalcPos;
  SuperPowerControl.SetTexture('data' + DirectorySeparator +'btn_space.tga');
  SuperPowerControl.Visible := False;
  Window.Controls.Add(SuperPowerControl);
//  MapControl.AddChild(SuperPowerControl);
  PlayerTurn := True;
  Player.Update;
  Player.TurnUnits := Player.MaxTurnUnits;
  for i := 0 to Warders.Count - 1 do
    begin
      if T_WarderUnit(Warders[i]).IsPlayerVisible then
        begin
          ReloadScene;
          Exit;
        end;
    end;
  MoveCamera(Player.Pos[0], Player.Pos[1], 0.25);
end;

procedure T_Space_Stealth.UpdateScene;
var
  i : integer;
  xWarderTurnIncomplete : boolean;
  xdx : Single;
begin
  inherited UpdateScene;

  if DoMoveCamera then
    begin
      xDX := TargetCameraX - OffsetX;
      xDX := Min(CameraSpeed, Abs(xDX)) * Sign(xDX);

      OffsetX := OffsetX + xDX * dt;

      xDX := TargetCameraY - OffsetY;
      xDX := Min(CameraSpeed, Abs(xDX)) * Sign(xDX);
      OffsetY := OffsetY + xDX * dt;

      xDX := TargetScale - Scale;
      xDX := Min(CameraSpeed * 0.01, Abs(xDX)) * Sign(xDX);
      Scale := Scale + xDX * dt;

      if Abs(TargetCameraX - OffsetX) < 10 then
        if Abs(TargetCameraY - OffsetY) < 10 then
          if Abs(TargetScale - Scale) < 1 then
            DoMoveCamera := False;
    end;

  if Window.Keys[MK_ESCAPE] then
    begin
      Window.Terminated := True;
      Exit;
    end;

  if Window.Keys[MK_1] then
    begin
      ReloadScene;
      Exit;
    end;

  if PlayerTurn then
    begin
      if Player.IsTurnComplete then
        begin
          for i := 0 to Warders.Count - 1 do
            T_WarderUnit(Warders[i]).InitTurn;
          PlayerTurn := False;
        end;
    end
  else
    begin
      xWarderTurnIncomplete := False;
      for i := 0 to Warders.Count - 1 do
        xWarderTurnIncomplete := xWarderTurnIncomplete or (not T_WarderUnit(Warders[i]).IsTurnComplete);
      if not xWarderTurnIncomplete then
        begin
          Player.InitTurn;
          PlayerTurn := True;
          MoveCamera(Player.Pos[0], Player.Pos[1], 0.25);
        end;
    end;

  Player.Update;
  for i := 0 to Warders.Count - 1 do
    T_WarderUnit(Warders[i]).Update;
  for i := 0 to Asteroids.Count - 1 do
    T_Asteroid(Asteroids[i]).Update;
end;

procedure T_Space_Stealth.DrawScene;
var
  i : integer;
  xAsteroid : T_Asteroid;
  q : PGLUquadric;
  xMousePos : RVector2f;
begin
  inherited DrawScene;
  glGetError;
  Shader.Use;
  glEnable(GL_COLOR_MATERIAL);
  glClearColor(0, 0, 0, 1);
  glClearDepth(1.0);
  glDepthRange(0, 1);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glFrontFace(GL_CCW);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  glOrtho(-Window.Width * Scale / 2 + OffsetX, Window.Width * Scale/ 2 + OffsetX, -Window.Height * Scale/ 2 + OffsetY, Window.Height * Scale/ 2 + OffsetY, -200, 200);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  DrawFon;
  Shader.DeUse;
  //Exit;
  glColor4f(1, 1, 1, 1);

  SetLight;


{  glEnable(GL_BLEND);
//  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBlendFunc(GL_SRC_ALPHA, GL_DST_COLOR);
  glColor4f(0, 0, 0, 0.5);
  glDisable(GL_LIGHTING);
  glBegin(GL_QUADS);
    glVertex3f(-1000, -1000, 0);
    glVertex3f(1000, -1000, 0);
    glVertex3f(1000, 1000, 0);
    glVertex3f(-1000, 1000, 0);
  glEnd;
  glEnable(GL_LIGHTING);
  glDisable(GL_BLEND);
  glColor4f(1, 1, 1, 1);}
  for i := 0 to Warders.Count - 1 do
    T_WarderUnit(Warders[i]).Draw;

  Player.Draw;
  for i := 0 to Asteroids.Count - 1 do
    begin
      xAsteroid := Asteroids[i];
      xAsteroid.Selected := SelectedAsteroid = xAsteroid;
      xAsteroid.Draw;
    end;
  if SelectedAsteroid <> nil then
    DrawFantom;
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  MapControl.Visible:= False;
  Window.DrawControls;
  MapControl.Visible:= True;

end;

procedure T_Space_Stealth.FreeScene;
var
  i : integer;
  xAsteroid : T_Asteroid;
begin
  Shader.Free;
  Window.Controls.Remove(MapControl);
  MapControl.Free;
  for i := 0 to Asteroids.Count - 1 do
    begin
      xAsteroid := Asteroids[i];
      xAsteroid.Free;
    end;
  Asteroids.Free;
  Player.Free;
  for i := 0 to Warders.Count - 1 do
    T_WarderUnit(Warders[i]).Free;
  Warders.Free;
  ModelSpider.Free;
  SoundServ.Stop(SoundSource);
  inherited FreeScene;
end;

procedure T_Space_Stealth.ReloadScene;
begin
  FreeScene;
  InitScene;
end;

procedure T_Space_Stealth.SetLight;
var
  xL : array[0..3] of Single;
begin
  glEnable(GL_LIGHTING);
  //glEnable(GL_LIGHT0);
  glEnable(GL_LIGHT1);

  xL[0] := 0.5;
  xL[1] := 0.5;
  xL[2] := 0.5;
  xL[3] := 0;
  glLightfv(GL_LIGHT1, GL_POSITION, @xL[0]);

  xL[0] := 1;
  xL[1] := 1;
  xL[2] := 1;
  xL[3] := 1;
  glLightfv(GL_LIGHT1, GL_DIFFUSE, @xL[0]);

  xL[0] := 4;
  xL[1] := 4;
  xL[2] := 4;
  xL[3] := 1;
  glLightfv(GL_LIGHT2, GL_AMBIENT, @xL[0]);

  xL[0] := 0;
  xL[1] := 0;
  xL[2] := 1;
  xL[3] := 0;
  glLightfv(GL_LIGHT2, GL_POSITION, @xL[0]);
end;

procedure T_Space_Stealth.DrawFon;
var
  k : Single;
  Arr: array[1..4, 1..2] of GLFloat;
  TexArr: array[1..8] of GLFloat;
  xBuf : GLuint;
begin
  k := 1;
  Arr[1][1] := -Window.Width * Scale * k / 2  + OffsetX;
  Arr[1][2] := -Window.Height * Scale * k / 2 + OffsetY;
  Arr[2][1] := Window.Width * Scale * k  / 2 + OffsetX;
  Arr[2][2] := -Window.Height * Scale * k / 2 + OffsetY;
  Arr[3][1] := Window.Width * Scale * k  / 2 + OffsetX;
  Arr[3][2] := Window.Height * Scale * k / 2 + OffsetY;
  Arr[4][1] := -Window.Width * Scale * k  / 2 + OffsetX;
  Arr[4][2] := Window.Height * Scale * k / 2 + OffsetY;

  TexArr[1] := 0;
  TexArr[2] := 0;
  TexArr[3] := 1;
  TexArr[4] := 0;
  TexArr[5] := 1;
  TexArr[6] := 1;
  TexArr[7] := 0;
  TexArr[8] := 1;


  glActiveTexture(GL_TEXTURE0);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texFon);


  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glVertexPointer(2, GL_FLOAT, 0, @Arr[1][1]);

  glTexCoordPointer(2, GL_FLOAT, 0, @TexArr[1]);
  glDrawArrays(GL_QUADS, 0, 4);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
  TextureManager.UnSetTexture;
  Exit;

  k := 1;
  glDisable(GL_LIGHTING);
  TextureManager.SetTexture(texFon);
  glBegin(GL_QUADS);
   glTexCoord2f(0, 0);
    glVertex3f(-Window.Width * Scale * k / 2  + OffsetX, -Window.Height * Scale * k / 2 + OffsetY, 0);
   glTexCoord2f(1, 0);
    glVertex3f(Window.Width * Scale * k  / 2 + OffsetX, -Window.Height * Scale * k / 2 + OffsetY, 0);
   glTexCoord2f(1, 1);
    glVertex3f(Window.Width * Scale * k  / 2 + OffsetX, Window.Height * Scale * k / 2 + OffsetY, 0);
   glTexCoord2f(0, 1);
    glVertex3f(-Window.Width * Scale * k  / 2 + OffsetX, Window.Height * Scale * k / 2 + OffsetY, 0);
  glEnd;
  TextureManager.UnSetTexture;
  glEnable(GL_LIGHTING);

end;

procedure T_Space_Stealth.OnMapMouseDown(AX, AY: Integer; AButton: T_MouseButton
  );
begin
  Case AButton of
    mbLeft:
      begin
        OffsetXDown:= Window.Mouse.X;
        OffsetYDown:= Window.Mouse.Y;
      end;
    mbRight:
      begin
        if SelectedAsteroid <> nil then
          begin
            Player.SetDestination(SelectedAsteroid, SelectedAsteroidAngle);
          end;
        if SelectedWarder <> nil then
          Player.AttackWarder(SelectedWarder);
      end;
  end;
end;

procedure T_Space_Stealth.OnMapMouseMove(AX, AY: Integer);
var
  xMousePos : RVector2f;
  i : integer;
  xAsteroid : T_Asteroid;
  xWarder : T_WarderUnit;
begin
  xMousePos := GetRealXY(Window.Mouse.X, Window.Height - Window.Mouse.Y);
  SelectedAsteroid := nil;
  for i := 0 to Asteroids.Count - 1 do
    begin
      xAsteroid := Asteroids[i];
      if Distance(xAsteroid.Pos, xMousePos) < xAsteroid.Radius * 1.05 then
        begin
          SelectedAsteroid := xAsteroid;
          SelectedAsteroidAngle := GetAngleBetweenPos(SelectedAsteroid.Pos, xMousePos);
        end;
    end;

  SelectedWarder := nil;
  for i := 0 to Warders.Count - 1 do
    begin
      xWarder := Warders[i];
      if Distance(xWarder.Pos, xMousePos) < xWarder.Radius * 1.05 then
        begin
          SelectedWarder := xWarder;
        end;
    end;

  if Window.Mouse.LMB then
  begin
    OffsetX := OffsetX + (OffsetXDown - AX) * Scale;
    OffsetY := OffsetY - (OffsetYDown - AY) * Scale;
    OffsetXDown := AX;
    OffsetYDown := AY;
  end;
end;

procedure T_Space_Stealth.OnMapMouseWheel(AValue: Integer);
begin
  //if not (scale + (0.25 * AValue) < 0.25) then
    //scale := scale - (0.25 * AValue);
    scale := scale * (1 - AValue * 0.1);
    scale := Max(0.025, Scale);
    Scale := Min(4, Scale);
end;

procedure T_Space_Stealth.MoveCamera(AX, AY, AScale: Single);
begin
  DoMoveCamera := True;
  TargetCameraX := AX;
  TargetCameraY := AY;
  TargetScale := AScale;
end;

end.

