unit _bmd;

{$mode delphi}

interface

uses
  Classes, SysUtils, _usual;

type
  T_Bone = class
    Matrix : RMatrix16f;
    Parent : T_Bone;
    Childs : TObjectList; // of T_Bone
  end;

  T_Armature = class
    Bones : TObjectList; // of T_Bone;
    Animations : TObjectList;
  end;

implementation

end.

