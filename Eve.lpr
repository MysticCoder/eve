program Eve;

{$mode delphi}{$H+}

  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}

uses
  {$IFDEF WINDOWS}
    windows,
  {$ENDIF}

  {$IFDEF LINUX}
    cthreads,
  {$ENDIF}

  gl, zlib, _window, _tga, _textures, {_scene_rpg,} _model, {_audio,} _usual, {_zlib,}
  sysutils, {_scene_cosmo, _scene_shooter, _keys, _space_stealth,}
  {_space_stealth_scenes,} inifiles, _font, {_nano, _deep, _scene_arena_shooter,}
  _scene_apoc, _apoc_map, _apoc_unit, _timer, _apoc_unit_controller, _apoc_net,
  _timing, _bmd, _control_edit, _apoc_lobby, _aspect, _town, _scene_explore;

var
  //xSample : _audio.PSoundSample;
  x : pointer;
  a : integer;
  xIni : TIniFile;
begin
  //Randomize;
  {$IFDEF WINDOWS}
  AllocConsole;
  {$ENDIF}
  try
  if (ParamStr(1) = '-s') then
    Window := WindowClass.CreateConsoleMode
  else
    begin
      Window := WindowClass.Create;
      xIni := TIniFile.Create('settings.ini');

      Window.Width := xIni.ReadInteger('main', 'width', 300);//Round(0.8 * Window.ScreenWidth);
      Window.Height:= xIni.ReadInteger('main', 'height', 300);;//Round(0.8 * Window.ScreenHeight);
      Window.Width := 800;
      Window.Height := 600;
      xIni.WriteInteger('main', 'width', Window.Width);
      xIni.WriteInteger('main', 'height', Window.Height);
      xIni.Free;

      Window.Left := (Window.ScreenWidth - Window.Width) div 2;
      Window.Top := (Window.ScreenHeight - Window.Height) div 2;
    end;
  Window.ChangeScene(T_Scene_Explore.Create);
  //GenerateTown;
  Window.Run;
  except
    on E:Exception do
      begin
        writeln(E.ClassName + ' ' + E.Message);
        readln;
      end;
  end;
end.
// В ОС Windows используется QueryPerfomanceCounter, в Linux gettimeofday

