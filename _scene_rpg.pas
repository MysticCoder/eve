unit _scene_rpg;

{$mode delphi}

interface

uses
 {$IFDEF WINDOWS}
 windows,
 {$ENDIF}
 _window, _textures, gl, glu, glext, math, sysutils, _tga, _usual, _model, classes, _font;

type

  RPosition = RVector3f;

  { T_Graphic }
  T_Unit = class;
  T_Graphic = class;
  T_Physic = class;
  T_Logic = class;

  T_GraphicClass = Class of T_Graphic;
  T_PhysicClass = Class of T_Physic;
  T_LogicClass = Class of T_Logic;

  T_Graphic = class
    Parent : T_Unit;
    Model : T_Mesh;
    procedure Draw; virtual;
  end;

  { T_Physic }

  T_Physic = class
    Parent : T_Unit;
    Pos : RPosition;
    PosEnd : RPosition;
    Angle : Single;
    AngleEnd : Single;
    AngleSpeed : Single;
    Speed : Single;
    procedure Update; virtual;
  end;

  { T_Logic }

  T_Logic = class
    Parent : T_Unit;
    BaseHP : Single;   //Str
    BaseAS : Single;   //Agi
    BaseDmg : Single;  //Str
    BaseMP : Single;   //Int
    BaseArmor : Single; //Agi
    BaseMArmor : Single; //Agi
    BaseSpeed : Single; // Agi
    BaseStr : Single; // влияет на HP, Dmg
    BaseAgi : Single; // влияет на AS, Armor, Speed
    BaseInt : Single; // Влияет на MP, MArmor
    procedure Update; virtual;
  end;

  { T_Unit }

  T_Unit = class
    Graphic : T_Graphic;
    Physic : T_Physic;
    Logic : T_Logic;
    constructor Construct(AGraphic : T_GraphicClass; APhysic : T_PhysicClass; ALogic : T_LogicClass); overload;
    constructor Construct(AGraphic : T_Graphic; APhysic : T_Physic; ALogic : T_Logic); overload;
    procedure Update; virtual;
    procedure Draw; virtual;

  end;

  RMapCell = record
    Height : Single;
    Normal : array[0..2] of Single;
    Sprite : integer;
    Passable : boolean;
  end;

  { T_Map }

  T_Map = class
    Map : array of array of RMapCell;
    MaxHeight : Single;
    Width, Height : integer;
    VertexData : Pointer;
    NormalData : Pointer;
    TexData : Pointer;
    texLand : Cardinal;
    WaterVData : Pointer;
    constructor Create(AWidth, AHeight : integer);
    procedure LoadFromFile(AFileName : string; AHeight : Single);
    procedure Load3DTex;
    procedure PrepareVBO;
    procedure PrepareWater;
    procedure DrawWater;
    procedure Draw;
  end;

  { T_Scene_Game }

  T_Scene_Game = class(T_Scene)
    Map : T_Map;
    Player : T_Unit;
    Mobs : TList; // of T_Unit;
    MapControl : TS_Control;
    a : boolean;
    CamX,
    CamY,
    CamZ : real;
    CamAngleX : real;
    CamAngleZ : real;
    Model : T_Mesh;
    FightMode : boolean;
    FightUnit : T_Unit;
    C_Attack : TS_Control;
    Font : T_Font;
    procedure GetRealXYZ(AX, AY : integer; out AOutX : Single; out AOutY : Single; out AOutZ : Single);
    procedure OnMapMouseDown(AX, AY : integer; AButton : T_MouseButton);
    procedure OnMapMouseMove(AX, AY : integer);
    procedure OnMapMouseWheel(AValue : integer);
    procedure OnChar(AChar : AnsiString); override;
    procedure SetCamera;
    procedure InitScene; override;
    procedure UpdateScene; override;
    procedure DrawScene; override;
    procedure DrawFight;
    procedure BeginFight(AUnit : T_Unit);
  end;

  { T_Scene_Logo }

  T_Scene_Logo = class(T_Scene)
    LogoTex : Cardinal;
    LogoControl : TS_Control;
    X : TS_Control;
    procedure InitScene; override;
    procedure DrawScene; override;
    procedure UpdateScene; override;
    procedure FreeScene; override;
    procedure OnChar(AChar : String); override;
  end;

  { T_Scene_MainMenu }

  T_Scene_MainMenu = class(T_Scene)
    Menu : TS_Control;
    btnStart,
    btnExit : TS_Control;
    procedure InitScene; override;
    procedure DrawScene; override;
    procedure UpdateScene; override;
    procedure FreeScene; override;
    procedure OnStartClick(AX, AY : integer; AButton : T_MouseButton);
    procedure OnExitClick(AX, AY : integer; AButton : T_MouseButton);
  end;

  { T_SceneGame }


function GetDistance(APos1, APos2 : RPosition) : Single;
function GetMinAngle(AAngle1, AAngle2 : Single) : Single;

const
  MaxPassableHeight = 0.4;
var
  Scene_Game : T_Scene_Game;
implementation

function GetMinAngle(AAngle1, AAngle2 : Single) : Single;
begin
  Result := AAngle2 - AAngle1;
  while Result > Pi do
    Result := Result - 2 * Pi;
  while Result < -Pi do
    Result := Result + 2 * Pi;
end;

function GetDistance(APos1, APos2 : RPosition) : Single;
var
  i : integer;
begin
  Result := 0;
  for i := Low(APos1) to High(APos1) do
    Result := Result + Sqr(APos1[i] - APos2[i]);
  Result := Sqrt(Result);
end;

{ T_Logic }

procedure T_Logic.Update;
begin

end;

{ T_Unit }

constructor T_Unit.Construct(AGraphic: T_GraphicClass; APhysic: T_PhysicClass;
  ALogic: T_LogicClass);
begin
  Graphic := AGraphic.Create;
  Logic := ALogic.Create;
  Physic := APhysic.Create;
  Graphic.Parent := Self;
  Logic.Parent := Self;
  Physic.Parent := Self;
end;

constructor T_Unit.Construct(AGraphic: T_Graphic; APhysic: T_Physic;
  ALogic: T_Logic);
begin
  Graphic := AGraphic;
  Physic := APhysic;
  Logic := ALogic;
  Graphic.Parent := Self;
  Logic.Parent := Self;
  Physic.Parent := Self;
end;

procedure T_Unit.Update;
begin
  if Assigned(Logic) then
    Logic.Update;
  if Assigned(Physic) then
    Physic.Update;
end;

procedure T_Unit.Draw;
begin
  if Assigned(Graphic) then
    Graphic.Draw;
end;

{ T_Graphic }

procedure T_Graphic.Draw;
var
  xQuad : PGLUquadric;
begin
  if Model <> nil then
    begin
      if Scene_Game.FightMode then
        begin
          Model.CalcAnimation(Scene_Game.dt);
          Model.Draw;
          Exit;
        end;
      glTranslatef(Parent.Physic.Pos[0], Parent.Physic.Pos[1], Parent.Physic.Pos[2]);
      Model.CalcAnimation(Scene_Game.dt);
      glRotatef(Parent.Physic.Angle * 180 / Pi, 0, 0, 1);
      Model.Draw;
      glRotatef(-Parent.Physic.Angle * 180 / Pi, 0, 0, 1);
      glTranslatef(-Parent.Physic.Pos[0], -Parent.Physic.Pos[1], -Parent.Physic.Pos[2]);
      Exit;
    end;
  glDisable(GL_TEXTURE_2D);
  xQuad := gluNewQuadric;
  glTranslatef(Parent.Physic.Pos[0], Parent.Physic.Pos[1], 0);
  gluSphere(xQuad, 0.5, 16, 16);
  glTranslatef(-Parent.Physic.Pos[0], -Parent.Physic.Pos[1], 0);
  gluDeleteQuadric(xQuad);
end;

{ T_Physic }

procedure T_Physic.Update;
var
  xAngle : Single;
  xPos : RPosition;
  xDistance : Single;
begin
  xDistance := GetDistance(Pos, PosEnd);
  if xDistance > 0 then
    AngleEnd := arctan2(PosEnd[1] - Pos[1], PosEnd[0] - Pos[0]);
  xAngle := GetMinAngle(Angle, AngleEnd);
  if xAngle <> 0 then
    xAngle := Sign(xAngle) * Min(Abs(xAngle), AngleSpeed * Scene_Game.dt);
  Angle := Angle + xAngle;
  if xDistance > 0 then
      begin
        xPos[0] := Pos[0] + cos(Angle) * Min(Speed, xDistance) * Scene_Game.dt;
        xPos[1] := Pos[1] + sin(Angle) * Min(Speed, xDistance) * Scene_Game.dt;
        xPos[2] := Scene_Game.Map.Map[trunc(xPos[0])][trunc(xPos[1])].Height;// * Scene_Game.Map.MaxHeight;
        if Abs(xPos[2] - Pos[2]) < MaxPassableHeight then
          Pos := xPos;
      end;
end;

{ T_Scene_Game }

procedure T_Scene_Game.GetRealXYZ(AX, AY: integer; out AOutX: Single; out
  AOutY: Single; out AOutZ: Single);
var
  xDepth : Single;
  xProjection : array[0..15] of Double;
  xModelView : array[0..15] of Double;
  xViewPort : array[0..3] of Integer;
  xX, xY, xZ : Double;
  xTexture : Cardinal;
begin
  AY := Window.Height - AY; // координата Y снизу вверх
  glGenTextures(1, @xTexture);
  glBindTexture(GL_TEXTURE_2D, xTexture);
  //glTexImage2D(GL_TEXTURE_2D, 0, gl_);
  //glCreateShader();
  //glCopyTexImage2D(GL_TEXTURE_2D, 0, );
  glReadPixels(AX, AY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT,  @xDepth); // находим глубину
  //glCopyTexImage2D();
  glGetDoublev(GL_PROJECTION_MATRIX, @xProjection[0]);  // заполняем матрицы проекции, модельвью и вьюпорта
  glGetDoublev(GL_MODELVIEW_MATRIX, @xModelView[0]);
  glGetIntegerv(GL_VIEWPORT, @xViewPort[0]);
  gluUnProject(AX, AY, xDepth, @xModelView[0], @xProjection[0], @xViewPort[0], @xX, @xY, @xZ); // находим мировые координаты куда кликнули
  AOutX := xX;
  AOutY := xY;
  AOutZ := xZ;
end;

procedure T_Scene_Game.OnMapMouseDown(AX, AY: integer; AButton: T_MouseButton);
var
  xZ : Single;
begin
  if AButton = mbRight then
    begin
  //    CamX:= 0;
  //    CamY:= 0;
    end;
  if Player = nil then
    Exit;
  if AButton = mbLeft then
    begin
      a := True; // вырубаем SwapBuffer
  //    DrawScene;     // рисуем сцену
      a := False;
      GetRealXYZ(AX, AY, Player.Physic.PosEnd[0], Player.Physic.PosEnd[1], xZ);
    end;
end;

procedure T_Scene_Game.OnMapMouseMove(AX, AY: integer);
var
  xDistance : real;
  xAngle : real;
begin
  if Window.Mouse.LMB then
    begin
      xDistance := sqrt(sqr(Window.Mouse.dx) + sqr(Window.Mouse.dy));
      xAngle := arctan2(Window.Mouse.dy, Window.Mouse.dx);

      xAngle := CamAngleZ * Pi / 180;
      CamX := CamX - Window.Mouse.dy * sin(xAngle) + Window.Mouse.dx * cos(xAngle);
      CamY := CamY - Window.Mouse.dy * cos(xAngle) - Window.Mouse.dx * sin(xAngle);


      //CamX := CamX + Window.Mouse.dx * cos(xAngle) + Window.Mouse.dy * sin(xAngle);
      //CamY := CamY - Window.Mouse.dy * cos(xAngle) - Window.Mouse.dx * sin(xAngle);

      //writeln(cos(CamAngleZ))
{      CamX := CamX + Window.Mouse.dx * cos(CamAngleZ * Pi / 180);
//      CamY := CamY - Window.Mouse.dx * sin(CamAngleZ);
      CamY := CamY + Window.Mouse.dy * sin(CamAngleZ * Pi / 180);
//      CamY := CamY - Window.Mouse.dy * cos(CamAngleZ);
}    end;
  if Window.Mouse.RMB then
    begin
      CamAngleZ := CamAngleZ + Window.Mouse.dx;
      CamAngleX := CamAngleX + Window.Mouse.dy;
    end;
end;

procedure T_Scene_Game.OnMapMouseWheel(AValue: integer);
begin
  CamZ := CamZ + (1 * AValue);
end;

procedure T_Scene_Game.OnChar(AChar: AnsiString);
begin
  if AChar = 'Ф'#0 then
    begin
      Player.Physic.PosEnd[0] := 5;
      Player.Physic.PosEnd[1] := 5;
    end;
end;

procedure T_Scene_Game.SetCamera;
begin
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  gluPerspective(45, Window.Width / Window.Height, 1, 500);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  CamX := Player.Physic.Pos[0];
  CamY := Player.Physic.Pos[1];
  //CamZ := 1 + Map.Map[trunc(CamX)][trunc(CamY)].Height;
  glTranslatef(0, 0, -CamZ);
  glRotatef(CamAngleX, 1, 0, 0);
  glRotatef(CamAngleZ, 0, 0, 1);
  glTranslatef(-CamX, -CamY, -10);
{  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  //gluOrtho2D(-50, 50, -50, 50);
  gluPerspective(45, Window.Width / Window.Height, 0.01, 100);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  //Translatef(0, 0, -40);
  glRotatef(-Player.Physic.Angle * 180 / Pi + 90, 0, 0, 1);
  gluLookAt(Player.Physic.Pos[0], Player.Physic.Pos[1], 40, Player.Physic.Pos[0], Player.Physic.Pos[1], 0, 0, 1, 1);
  Angle := Angle + 0.05;
  //glPolygonMode(GL_FRONT, GL_LINE);}
end;


procedure T_Scene_Game.InitScene;
begin
  inherited InitScene;
  Font := T_Font.Create;
  Font.LoadFromFile('data' + PathDelim + 'out');
  Scene_Game := Self;
  Map := T_Map.Create(100, 100);
  MapControl := TS_Control.Create;
  MapControl.Anchor := tsa_Center;
  MapControl.Width := Window.Width;
  MapControl.Height := Window.Height;
  Window.Controls.Add(MapControl);
  MapControl.RecalcPos;
  MapControl.OnMouseDownProc := OnMapMouseDown;
  MapControl.OnMouseMoveProc := OnMapMouseMove;
  MapControl.OnMouseWheelProc := OnMapMouseWheel;
  //MapControl.Visible := False;

  C_Attack := TS_Control.Create;
  C_Attack.Anchor := tsa_LeftBottom;
  C_Attack.Width := 128;
  C_Attack.Height := 40;
  C_Attack.BaseX := 10;
  C_Attack.BaseY := -100;
  C_Attack.SetTexture('data' + PathDelim + 'attack.tga');
  Window.Controls.Add(C_Attack);
  C_Attack.RecalcPos;

  Player := T_Unit.Construct(T_Graphic, T_Physic, T_Logic);
  Player.Graphic.Model := T_Mesh.Create;
  Player.Graphic.Model.LoadFromFile('data' + PathDelim + 'model.txt');
  Player.Graphic.Model.StartAnimation('run', True);
  Player.Graphic.Model.CalcAnimation(Scene_Game.dt);
  Player.Physic.Pos[0] := 190;
  Player.Physic.Pos[1] := 150;
  Player.Physic.Pos[2] := (Map.Map[190][150].Height);
  Player.Physic.PosEnd[0] := 190;
  Player.Physic.PosEnd[1] := 150;
  Player.Physic.AngleSpeed := 1000 *Pi/10;
  Player.Physic.Speed := 15;
  CamZ := 40;
  Model := T_Mesh.Create;
  Model.LoadFromFile('data' + PathDelim + 'model.txt');
  Model.StartAnimation('run', True);
  Model.CalcAnimation(dt);
  //FightMode := True;
end;

procedure T_Scene_Game.UpdateScene;
begin
  inherited UpdateScene;
  writeln(Player.Physic.Pos[0]:3:3, ' ', Player.Physic.Pos[1]:3:3, ' ', Player.Physic.Pos[2]:3:3);
  Player.Update;
  Model.CalcAnimation(dt);
end;

procedure T_Scene_Game.DrawScene;
var
  l1 : array[0..3] of single;
begin
  if Window.Mouse.RMB then
    glUseProgram(Window.sp)
  else
    glUseProgram(0);
  inherited DrawScene;
  if FightMode then
    begin
      DrawFight;
      Exit;
    end;
  glColor3f(1, 1, 1);
  glClearColor(0.5, 0.5, 0.5, 1);
  glClearDepth(1.0);
  glDepthRange(0, 1);
  glEnable(GL_COLOR_MATERIAL);
  glDepthFunc(GL_LESS);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_ALPHA_TEST);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);

  glEnable(GL_LIGHTING);
  glDisable(GL_LIGHT0);
 // glEnable(GL_LIGHT0);
  glEnable(GL_LIGHT1);
  l1[0] := 1;
  l1[1] := 1;
  l1[2] := 1;
  l1[3] := 1;
  glLightfv(GL_LIGHT1, GL_DIFFUSE, @l1[0]);
  l1[0] := 1;
  l1[1] := 1;
  l1[2] := 1;
  l1[3] := 0;
  glLightfv(GL_LIGHT1, GL_POSITION, @l1[0]);
  //glShadeModel(GL_SMOOTH);
  glEnable(GL_PERSPECTIVE_CORRECTION_HINT);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  //glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
  //glEnable(GL_POLYGON_SMOOTH);
  //glFrontFace(GL_FRONT);
  glDisable(GL_BLEND);
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  SetCamera;
  glLightfv(GL_LIGHT1, GL_POSITION, @l1[0]);
  //glNormal3f(1,1,1);
  //Model.Draw;
  glPushMatrix;
  Player.Draw;
  glPopMatrix;
  Map.Draw;
  glDisable(GL_LIGHTING);
  //Window.DrawControls;
  //Font.OutText2D(100, 100, ' Привет Мир!! :-)');
end;

procedure T_Scene_Game.DrawFight;
begin
  glColor3f(1, 1, 1);
  glClearColor(0, 0, 0, 1);
  glClearDepth(1.0);
  glDepthRange(0, 1);
  glEnable(GL_COLOR_MATERIAL);
  glDepthFunc(GL_LESS);
  glDisable(GL_CULL_FACE);
  //glEnable(GL_CULL_FACE);
  //glCullFace(GL_BACK);

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_ALPHA_TEST);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  gluPerspective(45, Window.Width / Window.Height, 1, 500);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;

  gluLookAt(0, -55, 0, 0, 0, 0, 0, 0, 1);
  glColor4f(1, 1, 1, 1);
  Player.Graphic.Model.AnimLooped:= True;
  Player.Graphic.Draw;
  Window.DrawControls;
end;

procedure T_Scene_Game.BeginFight(AUnit: T_Unit);
begin
  FightMode := True;
  FightUnit := AUnit;
end;

{ T_Map }

constructor T_Map.Create(AWidth, AHeight: integer);
begin
  //Exit;
  Width := AWidth;
  Height := AHeight;
  //SetLength(Map, AWidth, AHeight);
  LoadFromFile('data' + PathDelim + 'land2', 20);
end;

procedure T_Map.LoadFromFile(AFileName: string; AHeight: Single);
var
  xTGA : TGA_Header;
  i, j : integer;
  xPixel : PCardinal;
begin
  MaxHeight := AHeight;
  xTGA := LoadTGA(AFileName + '.tga');
  SetLength(Map, xTGA.Width , xTGA.Height);
  Width := xTGA.Width;
  Height := xTGA.Height;
  xPixel := xTGA.Data;
  for i := 0 to xTGA.Width - 1 do
    for j := 0 to xTGA.Height - 1 do
      begin
        xPixel := Pointer(PtrUint(xTGA.Data) + i * 4 + j * xTGA.Width * 4);
        Map[i][j].Height := (xPixel^ and $FFFFFF)/$FFFFFF * AHeight; //(GetBValue(xPixel^) / $FF) * AHeight;
        //inc(xPixel);
        //writeln(Map[i][j].Height:3:3);
      end;
  FreeMem(xTGA.Data);

  xTGA := LoadTGA(AFileName + 'n.tga');
  for i := 0 to xTGA.Width - 1 do
    for j := 0 to xTGA.Height - 1 do
      begin
        xPixel := Pointer(PtrUInt(xTGA.Data) + i * 4 + j * xTGA.Width * 4);
        Map[i][j].Normal[0] := -1 + 2 * GetRValue(xPixel^) / 255; //(GetBValue(xPixel^) / $FF) * AHeight;
        Map[i][j].Normal[1] := -1 + 2 * GetGValue(xPixel^) / 255; //(GetBValue(xPixel^) / $FF) * AHeight;
        Map[i][j].Normal[2] := -1 + 2 * GetBValue(xPixel^) / 255; //(GetBValue(xPixel^) / $FF) * AHeight;
        //inc(xPixel);
        //writeln(Map[i][j].Height:3:3);
      end;
  FreeMem(xTGA.Data);
  PrepareVBO;
  Load3DTex;
  PrepareWater;
end;

procedure T_Map.Load3DTex;
var
  xTGA : TGA_Header;
  xMem : PGLvoid;
begin
  xTGA := LoadTGA('data' + PathDelim + 'landtex1.tga');
  xMem := GetMem(3 * xTGA.Width * xTGA.Height * 4 * 100);
  Move(xTGA.Data^, xMem^, xTGA.Width * xTGA.Height * 4);
  Freemem(xTGA.Data);

  xTGA := LoadTGA('data' + PathDelim + 'landtex2.tga');
  Move(xTGA.Data^, Pointer(PtrUint(xMem) + (xTGA.Width * xTGA.Height * 4))^, xTGA.Width * xTGA.Height * 4);
  Freemem(xTGA.Data);

  xTGA := LoadTGA('data' + PathDelim + 'landtex3.tga');
  Move(xTGA.Data^, Pointer(PtrUint(xMem) + (xTGA.Width * xTGA.Height * 4) * 2)^, xTGA.Width * xTGA.Height * 4);
  Freemem(xTGA.Data);

  glGenTextures(1, @TexLand);
  glBindTexture(gl_texture_3d, texLand);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 0);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  //glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri (GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, xTGA.Width, xTGA.Height, 3, 0, GL_RGBA, GL_UNSIGNED_BYTE, xMem);
  Freemem(xMem);
end;

procedure T_Map.PrepareVBO;
var
  i, j : integer;
  xVPointer : PSingle;
  xNPointer : PSingle;
  xTPointer : PSingle;
  var
    xFactor : real;
begin
  VertexData := GetMem(Width * Height * 4 * 3 * SizeOf(Single));
  NormalData := GetMem(Width * Height * 4 * 3 * SizeOf(Single));
  TexData := GetMem(Width * Height * 4 * 3 * SizeOf(Single));
  xVPointer := VertexData;
  xNPointer := NormalData;
  xTPointer := TexData;
  for i := 0 to Width - 2 do
    for j := 0 to Height - 2 do
      begin
        AddDataf(xVPointer, [i, j, Map[i][j].Height]);
        AddDataf(xVPointer, [i + 1, j, Map[i + 1][j].Height]);
        AddDataf(xVPointer, [i + 1, j + 1, Map[i + 1][j + 1].Height]);
        AddDataf(xVPointer, [i, j + 1, Map[i][j + 1].Height]);

        AddDataf(xNPointer, [Map[i][j].Normal[0], Map[i][j].Normal[1], Map[i][j].Normal[2]]);
        AddDataf(xNPointer, [Map[i+1][j].Normal[0], Map[i+1][j].Normal[1], Map[i+1][j].Normal[2]]);
        AddDataf(xNPointer, [Map[i+1][j+1].Normal[0], Map[i+1][j+1].Normal[1], Map[i+1][j+1].Normal[2]]);
        AddDataf(xNPointer, [Map[i][j+1].Normal[0], Map[i][j+1].Normal[1], Map[i][j+1].Normal[2]]);

        xFactor := sin((i + j) * Pi / 100);
        xFactor := xFactor + 1;
        xFactor := xFactor / 2;
        xFactor := 1;
 {               _AddData(xTPointer, [0, 0, 0]);
                _AddData(xTPointer, [1, 0, 0.5]);
                _AddData(xTPointer, [1, 1, 1]);
                _AddData(xTPointer, [0, 1, 0.5]);
}
{        _AddData(xTPointer, [random, random, xFactor * Map[i][j].Height / MaxHeight]);
        _AddData(xTPointer, [random, random, xFactor * Map[i + 1][j].Height / MaxHeight]);
        _AddData(xTPointer, [random, random, xFactor * Map[i + 1][j + 1].Height / MaxHeight]);
        _AddData(xTPointer, [random, random, xFactor * Map[i][j + 1].Height / MaxHeight]);
}        AddDataf(xTPointer, [0, 0, xFactor * Map[i][j].Height / MaxHeight]);
        AddDataf(xTPointer, [1, 0, xFactor * Map[i + 1][j].Height / MaxHeight]);
        AddDataf(xTPointer, [1, 1, xFactor * Map[i + 1][j + 1].Height / MaxHeight]);
        AddDataf(xTPointer, [0, 1, xFactor * Map[i][j + 1].Height / MaxHeight]);
      end;
end;

procedure T_Map.PrepareWater;
  var
    i, j : integer;
    xVPointer : PSingle;
    var
      xFactor : real;
      xWaterHeight : Single;
  begin
    xWaterHeight := 5;
    WaterVData := GetMem(Width * Height * 4 * 3 * SizeOf(Single));
    xVPointer := WaterVData;
    for i := 0 to Width - 2 do
      for j := 0 to Height - 2 do
        begin
          AddDataf(xVPointer, [i, j, xWaterHeight]);
          AddDataf(xVPointer, [i + 1, j, xWaterHeight]);
          AddDataf(xVPointer, [i + 1, j + 1, xWaterHeight]);
          AddDataf(xVPointer, [i, j + 1, xWaterHeight]);
        end;
end;

procedure T_Map.DrawWater;
var
  i, j : integer;
  xIndex : integer;
begin
  glColor4f(0, 0, 1, 0.5);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_ALPHA_TEST);
  glEnableClientState(GL_VERTEX_ARRAY);
  glNormal3f(0,0,1);
  glVertexPointer(3, GL_FLOAT, 0, WaterVData);
  glDrawArrays(GL_QUADS, 0, (Width - 0) * (Height - 0) * 4);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisable(GL_BLEND);
  glColor4f(1, 1, 1, 1);
end;

procedure T_Map.Draw;
var
  i, j : integer;
  xIndex : integer;
begin
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  glVertexPointer(3, GL_FLOAT, 0, VertexData);
  glNormalPointer(GL_FLOAT, 0, NormalData);
  glTexCoordPointer(3, GL_FLOAT, 0, TexData);

  glEnable(GL_TEXTURE_3D);
  glBindTexture(GL_TEXTURE_3D, texLand);
  glDrawArrays(GL_QUADS, 0, (Width - 0) * (Height - 0) * 4);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_NORMAL_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisable(GL_TEXTURE_3D);
  DrawWater;
  Exit;
//  glBindTexture(GL_TEXTURE_2D, 1);
  glBegin(GL_QUADS);
  //glVertexPointer(;
  for i := 0 to Width - 2 do
    for j := 0 to Height - 2 do
      begin
{                glTexCoord2f(0, 0);
                  glVertex3f(i, j, 0);
                  glTexCoord2f(1, 0);
                  glVertex3f(i + 1, j , 0);
                  glTexCoord2f(1, 1);
                  glVertex3f(i + 1, j + 1, 0);
                  glTexCoord2f(0, 1);
                  glVertex3f(i, j + 1, 0);}
        glTexCoord2f(0, 0);
          glVertex3f(i - 0.5, j - 0.5, Map[i][j].Height);
          glTexCoord2f(1, 0);
          glVertex3f(i + 0.5, j - 0.5, Map[i][j].Height);
          glTexCoord2f(1, 1);
          glVertex3f(i + 0.5, j + 0.5, Map[i][j].Height);
          glTexCoord2f(0, 1);
          glVertex3f(i - 0.5, j + 0.5, Map[i][j].Height);
      end;
  glEnd;
end;

{ T_SceneGame }


procedure T_Scene_MainMenu.InitScene;
begin
  inherited InitScene;
  Menu := TS_Control.Create;
  Menu.Anchor := tsa_Center;
  Menu.Width := 400;
  Menu.Height := 200;
  Window.Controls.Add(Menu);

  btnStart := TS_Control.Create;
  btnStart.Width := 200;
  btnStart.Height := 60;
  btnStart.Anchor := tsa_Top;
  btnStart.BaseY := 10;

  btnExit := TS_Control.Create;
  btnExit.Width := 200;
  btnExit.Height := 60;
  btnExit.Anchor := tsa_Top;
  btnExit.BaseY := 110;

  Menu.AddChild(btnStart);
  Menu.AddChild(btnExit);
  Menu.SetTexture('data/logo.tga');
  btnStart.SetTexture('data/start.tga');
  btnExit.SetTexture('data/logo.tga');
  btnStart.OnMouseDownProc := onStartClick;
  btnExit.OnMouseDownProc := OnExitClick;
end;

procedure T_Scene_MainMenu.DrawScene;
begin
  inherited DrawScene;
  glClearColor(0, 0, 0, 1);
  glClearDepth(1.0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);

  Menu.RecalcPos;
  Window.DrawControls;
  //Window.SwapBuffers;
end;

procedure T_Scene_MainMenu.UpdateScene;
begin
  inherited UpdateScene;
end;

procedure T_Scene_MainMenu.FreeScene;
begin
  inherited FreeScene;
  Window.FreeControls;
end;

procedure T_Scene_MainMenu.OnStartClick(AX, AY: integer; AButton: T_MouseButton
  );
begin
  Window.DelayedChangeScene(T_Scene_Game.Create);
end;

procedure T_Scene_MainMenu.OnExitClick(AX, AY: integer; AButton: T_MouseButton);
begin
  Window.Terminated := True;
end;

{ T_Scene_Logo }

procedure T_Scene_Logo.InitScene;
begin
  inherited InitScene;
  LogoControl := TS_Control.Create;
  LogoControl.Anchor := tsa_Center;
  LogoControl.Width := 400;
  LogoControl.Height := 400;
  LogoControl.BaseX := 0;//-LogoControl.Width div 2;
  LogoControl.BaseY := 0;//-LogoControl.Height div 2;
  LogoControl.SetTexture('data\logo.tga');
  Window.Controls.Add(LogoControl);

  X := TS_Control.Create;
  X.Anchor := tsa_Center;
  X.Width := Window.Width;
  X.Height := Window.Height;
  X.SetTexture('data\moroz.tga', False);
  Window.Controls.Add(X);
  //LogoTex := TS_TextureManager.LoadTexture('data\logo.tga');
{  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  //gluPerspective(45, Window.Width / Window.Height, -1, 1);
  glOrtho(-1, 1, -1, 1, -1, 1);
}
end;

procedure T_Scene_Logo.DrawScene;
begin
  inherited DrawScene;
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
{  glEnable(GL_TEXTURE_2D);
  glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex2f(-1, -1);
    glTexCoord2f(1, 0);
    glVertex2f(-1 + 2/Window.Aspect, -1);
    glTexCoord2f(1, 1);
    glVertex2f(-1 + 2/Window.Aspect, 1);
    glTexCoord2f(0, 1);
    glVertex2f(-1, 1);
  glEnd;
  glDisable(GL_TEXTURE_2D);
  }
  Window.DrawControls;
  //Window.SwapBuffers;
end;

var
  xByte : Byte;
procedure T_Scene_Logo.UpdateScene;
var
  pTexture : P_Texture;
  i, j : integer;
  pPixel : PCardinal;
  s : string;
begin
  xByte := xByte + 1;
  pTexture := TextureManager.Textures[1];
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, pTexture^.TextureID);
  for i := 0 to pTexture^.Width - 1 do
    for j := 0 to pTexture^.Height - 1 do
      begin
        pPixel := Pointer(Cardinal(pTexture^.TextureData) + pTexture^.Width * j * 4 + i * 4);
        pPixel^ := Random($FFFFFFFF);
        //pPixel^ := (pPixel^ and $FFFFFF) or (xByte shl 24);
      end;
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, pTexture^.Width, pTexture^.Height, GL_RGBA, GL_UNSIGNED_BYTE, pTexture^.TextureData);
  inherited UpdateScene;
  LogoControl.RecalcPos;
end;

procedure T_Scene_Logo.FreeScene;
begin
  inherited FreeScene;
  Window.Controls.Remove(X);
  X.Free;
  Window.Controls.Remove(LogoControl);
  LogoControl.Free;
end;

procedure T_Scene_Logo.OnChar(AChar: String);
begin
  inherited OnChar(AChar);
  Window.ChangeScene(T_Scene_MainMenu.Create);
end;

end.
