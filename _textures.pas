unit _textures;

{$mode delphi}

interface

uses
  gl, glext, _tga, Classes, sysutils,  _usual, lazutf8;
type

  { T_Font }
  T_FontChar = record
    Char : AnsiString;
    X,Y : integer;
    Width, Height : integer;
    TexCoord : array[0..3] of RVector2f;
  end;
  P_FontChar = ^T_FontChar;


  { TS_TextureManager }
  R_Texture = record
    FileName : string;
    TextureID : Cardinal;
    Subscribers : integer;
    Width,
    Height : integer;
    TextureData : Pointer;
  end;

  P_Texture = ^R_Texture;

  TS_TextureManager = class
    Textures : TList; //of TS_Texture
    constructor Create;
    procedure SetTexture(ATextureID : Cardinal);
    procedure UnSetTexture;
    function LoadTextureFromMem(AWidth, AHeight : integer; AMem : Pointer; AFileName : AnsiString = '') : Cardinal;
    function LoadTexture(AFileName : string; ADoFreeData : boolean = True) : Cardinal;
  end;

var
  TextureManager : TS_TextureManager;
implementation
  uses _Window;

constructor TS_TextureManager.Create;
begin
  Textures := TList.Create;

end;

procedure TS_TextureManager.SetTexture(ATextureID: Cardinal);
begin
  glEnable(GL_TEXTURE_2D);
  //glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, ATextureID);
  //glUniform1f(glGetUniformLocation(Window.sp, 'tex0'), 0);
  //glUniform1f(glGetUniformLocation(Window.sp, 'tex0'), 0);
  //glActiveTexture(GL_TEXTURE0);
end;

procedure TS_TextureManager.UnSetTexture;
begin
  glBindTexture(GL_TEXTURE_2D, 0);
  //glDisable(GL_TEXTURE_2D);
end;

function TS_TextureManager.LoadTextureFromMem(AWidth, AHeight: integer;
  AMem: Pointer; AFileName: AnsiString): Cardinal;
var
  pTexture : P_Texture;
begin
  glGenTextures(1, @Result);
  glBindTexture(GL_TEXTURE_2D, Result);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, AWidth, AHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, AMem);
  New(pTexture);
  pTexture^.Subscribers := 1;
  pTexture^.FileName := AFileName;
  pTexture^.TextureID := Result;
  pTexture^.Width := AWidth;
  pTexture^.Height := AHeight;
  pTexture^.TextureData := nil;
  Textures.Add(pTexture);
end;

{ TS_TextureManager }
function TS_TextureManager.LoadTexture(AFileName: string; ADoFreeData: boolean
  ): Cardinal;
var
  xTGA : TGA_Header;
  i : integer;
  pTexture : P_Texture;
begin
  Result := 0;
  for i := 0 to Textures.Count - 1 do
    begin
      pTexture := Textures[i];
      if AnsiUpperCase(pTexture^.FileName) = AnsiUpperCase(AFileName) then
        begin
          Result := pTexture^.TextureID;
          Inc(pTexture^.Subscribers);
          Exit;
        end;
    end;
  if not FileExists(AFileName) then
    Exit;
  xTGA := LoadTGA(AFileName);
  Result := LoadTextureFromMem(xTGA.Width, xTGA.Height, xTGA.Data, AFileName);

  if ADoFreeData then
    Dispose(xTGA.Data)
  else
    pTexture^.TextureData := xTGA.Data;
end;

end.

