unit _nano;

{$mode delphi}

interface

uses
  Classes, SysUtils, Sockets;
type
  R_Address = record
    IP : ShortString;
    ProxyIP : ShortString;
    ProxyPort : Word;
    Port : Word;
    ID : integer;
  end;


  T_NanoConnector = class;

  { T_Nano }

  T_Nano = class
    Connector : T_NanoConnector;
    constructor Create(AConnector : T_NanoConnector = nil);
  end;

  { T_NanoConnector }

  T_NanoConnector = class(T_Nano)
    Clients : TList; // of T_Nano;
    Address : R_Address;
    Sock : Tsocket;
    TrID : Cardinal;
    DoTerminateThread : boolean;
    constructor Create(APort : Word = 10001);
    //procedure AddClient(AClient : T_Nano);
  end;

implementation

{ T_NanoConnector }
function NanoConnectorThread(pConnector : Pointer) : ptrint;
var
  xConnector : T_NanoConnector;
begin
  xConnector := pConnector;
  while not xConnector.DoTerminateThread do
    begin
      Sleep(0);
      //fprecvfrom();
      //fpsendto();
    end;
end;

constructor T_Nano.Create(AConnector: T_NanoConnector);
begin
  Connector := AConnector;
end;

constructor T_NanoConnector.Create(APort: Word);
var
  addr : TSockAddr;
begin
  inherited Create;
  Clients := TList.Create;
  Sock := fpsocket(AF_INET, SOCK_DGRAM, PF_INET);
  FillChar(addr, SizeOf(Addr), 0);
  addr.sin_family := AF_INET;
  addr.sin_addr.s_addr := INADDR_ANY;
  addr.sin_port := APort;
  fpbind(Sock, @addr, SizeOf(addr));
  TrID := BeginThread(NanoConnectorThread, Self);
end;

end.
