unit _scene_shooter;

{$mode delphi}

interface

uses
  Classes, SysUtils, math, _window, _usual, _textures, gl, glu, _keys;

type

  { T_Scene_Shooter }
  T_Unit = class;

  { T_PhysicBase }

  T_PhysicBase = class
    Parent : T_Unit;
    Pos : RVector3f;
    Angle : Single;
    Speed : Single;
    procedure Update; virtual;
  end;

  { T_LogicBase }

  T_LogicBase = class
    Parent : T_Unit;
    procedure Update; virtual;
  end;

  { T_GraphicBase }

  T_GraphicBase = class
    Parent : T_Unit;
    procedure Draw; virtual;
  end;

  T_LogicClass = class of T_LogicBase;
  T_PhysicClass = class of T_PhysicBase;
  T_GraphicClass = class of T_GraphicBase;

  { T_Physic }

  T_Physic = class(T_PhysicBase)
    DoMove : Boolean;
    AngleMove : Single;
    procedure MoveDirection(AAngle : Single);
    procedure Update; override;
  end;

  { T_PhysicBullet }

  T_PhysicBullet = class(T_Physic)
    EndPos : RVector3f;
    procedure Update; override;
  end;

  { T_Logic }

  T_Logic = class(T_LogicBase)
    wasd : array[0..3] of Boolean;
    procedure Update; override;
    procedure ShootTo(APos : RVector3f);
  end;

  { T_Graphic }

  T_Graphic = class(T_GraphicBase)
    procedure Draw; override;
  end;

  T_Weapon = class
    Cooldown : Single;
    //procedure ShootTo();
  end;
  { T_Unit }

  T_Unit = class
    Physic : T_PhysicBase;
    Logic : T_LogicBase;
    Graphic : T_GraphicBase;
    procedure Update;
    constructor Construct(ALogic : T_LogicBase; APhysic : T_PhysicBase; AGraphic : T_GraphicBase); overload;
    constructor Construct(ALogic : T_LogicClass; APhysic : T_PhysicClass; AGraphic : T_GraphicClass); overload;
  end;

  T_MapCell = record
    Passable : boolean;
    Tex : Cardinal;
  end;

  { T_Map }

  T_Map = class
    Width,
    Height : integer;
    CellWidth,
    CellHeight : integer;
    Cells : array of array of T_MapCell;
    procedure GenerateMap1;
    procedure Draw;
  end;

  T_Scene_Shooter = class(T_Scene)
    Units : TList; // of T_Unit
    Map : T_Map;
    Control : TS_Control;
    CamPos : RVector3f;
    CamAngle : RVector3f;
    function GetRealXY : RVector3f;
    procedure SetCamera;
    procedure PreDraw;
    procedure DrawUnits;
    procedure OnMouseMove(AX, AY : integer);
    procedure OnMouseWheel(AValue : integer);
    procedure OnMouseDown(AX, AY : integer; AButton : T_MouseButton);
    procedure InitScene; override;
    procedure UpdateScene; override;
    procedure DrawScene; override;
    procedure FreeScene; override;
  end;

var
  Scene : T_Scene_Shooter;
implementation

{ T_LogicBase }

procedure T_LogicBase.Update;
begin

end;

{ T_PhysicBase }

procedure T_PhysicBase.Update;
begin

end;

{ T_GraphicBase }

procedure T_GraphicBase.Draw;
begin

end;

{ T_PhysicBullet }

procedure T_PhysicBullet.Update;
var
  xDistance : Single;
begin
  AngleMove := arctan2(EndPos[1] - Pos[1], EndPos[0] - Pos[0]);
  DoMove := True;
  xDistance := GetDistance(Pos, EndPos);
  if xDistance < Speed * Scene.dt then
    Speed := xDistance / Scene.dt;
  inherited Update;
end;

{ T_Physic }

procedure T_Physic.MoveDirection(AAngle: Single);
begin
  Pos[0] := Pos[0] + Cos(AAngle) * Speed * Scene.dt;
  Pos[1] := Pos[1] + Sin(AAngle) * Speed * Scene.dt;
end;

procedure T_Physic.Update;
begin
  inherited Update;
  if DoMove then
    MoveDirection(AngleMove);
end;


{ T_Logic }

procedure T_Logic.Update;
var
  x2 : Single;
  xAngle : Single;
begin
  x2 := sqrt(0.5);
  wasd[0] := Window.Keys[MK_W];
  wasd[1] := Window.Keys[MK_A];
  wasd[2] := Window.Keys[MK_S];
  wasd[3] := Window.Keys[MK_D];
  xAngle := Pi / 2 + Scene.CamAngle[2] * Pi / 180;
  T_Physic(Parent.Physic).DoMove := True;
  with T_Physic(Parent.Physic) do
  if wasd[0] and wasd[1] then
    AngleMove := xAngle + Pi/4
  else
    if wasd[0] and wasd[3] then
      AngleMove := xAngle - Pi/4
    else
      if wasd[2] and wasd[1] then
        AngleMove := xAngle + 3 * Pi / 4
      else
        if wasd[2] and wasd[3] then
          AngleMove := xAngle - 3 * Pi / 4
        else
          if wasd[0] then
            AngleMove := xAngle
          else
            if wasd[1] then
              AngleMove := xAngle + Pi/2
            else
              if wasd[2] then
                AngleMove := xAngle + Pi
              else
                if wasd[3] then
                  AngleMove := xAngle - Pi/2
                else
                  DoMove := False;
end;

procedure T_Logic.ShootTo(APos: RVector3f);
var
  xBullet : T_Unit;
begin
  xBullet := T_Unit.Create;
  xBullet.Graphic := T_Graphic.Create;
  xBullet.Physic := T_PhysicBullet.Create;
  xBullet.Physic.Pos := Parent.Physic.Pos;
  xBullet.Physic.Speed := 200;
  T_PhysicBullet(xBullet.Physic).EndPos := APos;
  xBullet.Logic := T_Logic.Create;

  xBullet.Graphic.Parent := xBullet;
  xBullet.Physic.Parent := xBullet;
  xBullet.Logic.Parent := xBullet;
  Scene.Units.Add(xBullet);
end;

{ T_Unit }

procedure T_Unit.Update;
begin
  Logic.Update;
  Physic.Update;
end;

constructor T_Unit.Construct(ALogic: T_LogicBase; APhysic: T_PhysicBase;
  AGraphic: T_GraphicBase);
begin
  Logic := ALogic;
  Logic.Parent := Self;
  Physic := APhysic;
  Physic.Parent := Self;
  Graphic := AGraphic;
  Graphic.Parent := Self;
end;

constructor T_Unit.Construct(ALogic: T_LogicClass; APhysic: T_PhysicClass;
  AGraphic: T_GraphicClass);
begin
  Logic := ALogic.Create;
  Logic.Parent := Self;
  Physic := APhysic.Create;
  Physic.Parent := Self;
  Graphic := AGraphic.Create;
  Graphic.Parent := Self;
end;


{ T_Graphic }

procedure T_Graphic.Draw;
var
  q : PGLUquadric;
begin
  glColor3f(0, 1, 0);
  q := gluNewQuadric;
  with Parent.Physic do
    glTranslatef(Pos[0], Pos[1], Pos[2]);

  gluSphere(q, 0.5, 16, 16);

  with Parent.Physic do
    glTranslatef(-Pos[0], -Pos[1], -Pos[2]);
  gluDeleteQuadric(q);
  glColor3f(1, 1, 1);
end;


{ T_Map }

procedure T_Map.GenerateMap1;
var
  i, j : integer;
begin
  Width := 100;
  Height := 100;
  CellWidth := 1;
  CellHeight := 1;
  SetLength(Cells, Width, Height);
  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
      begin
        Cells[i][j].Passable := True;
        Cells[i][j].Tex := TextureManager.LoadTexture(Path_Data + 'earth.tga');
      end;
end;

procedure T_Map.Draw;
var
  i, j : integer;
begin
  {glBegin(GL_QUADS);
    glVertex3f(0, 0, 0);
    glVertex3f(100, 0, 0);
    glVertex3f(100, 100, 0);
    glVertex3f(0, 100, 0);
  glEnd;
  exit;}
  TextureManager.SetTexture(Cells[0][0].Tex);
  glBegin(GL_QUADS);
  for i := 0 to Width - 1 do
    for j := 0 to Height - 1 do
      begin
          glTexCoord2f(0, 0);
          glVertex3f(i, j, 0);
          glTexCoord2f(1, 0);
          glVertex3f(i + 1, j, 0);
          glTexCoord2f(1, 1);
          glVertex3f(i + 1, j + 1, 0);
          glTexCoord2f(0, 1);
          glVertex3f(i, j + 1, 0);
      end;
  glEnd;
end;

{ T_Scene_Shooter }

function T_Scene_Shooter.GetRealXY: RVector3f;
var
  xModel : array[0..15] of GLDouble;
  xProj : array[0..15] of GLDouble;
  xView : array[0..3] of Integer;
  xX, xY, xZ : GLDouble;
  x1, y1, z1 : GLDouble;
  x2, y2, z2 : GLDouble;
  k : GLDouble;
begin
  glGetDoublev(GL_MODELVIEW_MATRIX, @xModel[0]);
  glGetDoublev(GL_PROJECTION_MATRIX, @xProj[0]);
  glGetIntegerv(GL_VIEWPORT, @xView[0]);
  Log(gluErrorString(glGetError), True);

  gluUnProject(Window.Mouse.X, Window.Height - Window.Mouse.Y, 0, @xModel[0], @xProj[0], @xView[0], @x1, @y1, @z1);
  Log(gluErrorString(glGetError), True);
  gluUnProject(Window.Mouse.X, Window.Height - Window.Mouse.Y, GLDouble(1.0), @xModel[0], @xProj[0], @xView[0], @x2, @y2, @z2);
  Log(gluErrorString(glGetError), True);
  Log(FloatToStr(x1), True);
  k := z1 / (z1 - z2);
  Result[0] := Lerp(x1, x2, k);
  Result[1] := Lerp(y1, y2, k);
  Result[2] := Lerp(z1, z2, k);
end;

procedure T_Scene_Shooter.SetCamera;
begin
  CamPos[0] := T_Unit(Units[0]).Physic.Pos[0];
  CamPos[1] := T_Unit(Units[0]).Physic.Pos[1];
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  gluPerspective(90, Window.Width / Window.Height, 1, 500);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  glRotatef(-CamAngle[0], 1, 0, 0);
  glRotatef(-CamAngle[1], 0, 1, 0);
  glRotatef(-CamAngle[2], 0, 0, 1);
  glTranslatef(-CamPos[0], -CamPos[1], -CamPos[2]);
  //gluLookAt(Map.Width / 2, Map.Height / 2, 50, Map.Width / 2, Map.Height / 2, 0, 0, 1, 0);
end;

procedure T_Scene_Shooter.PreDraw;
begin
  glClearColor(0, 0, 0, 1);
  glClearDepth(1);
  glDepthRange(0, 1);
  glDepthFunc(GL_LESS);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
end;

procedure T_Scene_Shooter.DrawUnits;
var
  i : integer;
begin
  for i := 0 to Units.Count - 1 do
    T_Unit(Units[i]).Graphic.Draw;
end;

procedure T_Scene_Shooter.OnMouseMove(AX, AY: integer);
begin
  if Window.Mouse.RMB then
    begin
      CamAngle[0] := CamAngle[0] - Window.Mouse.dy;
      if CamAngle[0] < 0 then
        CamAngle[0] := 0;
      CamAngle[2] := CamAngle[2] - Window.Mouse.dx;
    end;
end;

procedure T_Scene_Shooter.OnMouseWheel(AValue: integer);
begin
  CamPos[2] := CamPos[2] + AValue;
end;

procedure T_Scene_Shooter.OnMouseDown(AX, AY: integer; AButton: T_MouseButton);
begin
  if AButton = mbLeft then
    begin
      T_Logic(T_Unit(Units[0]).Logic).ShootTo(GetRealXY);
    end;
end;

procedure T_Scene_Shooter.InitScene;
var
  xUnit : T_Unit;
begin
  inherited InitScene;
  Scene := Self;
  Map := T_Map.Create;
  Map.GenerateMap1;
  Units := TList.Create;
  xUnit := T_Unit.Create;
  xUnit.Graphic := T_Graphic.Create;
  xUnit.Logic := T_Logic.Create;
  xUnit.Physic := T_Physic.Create;
  xUnit.Graphic.Parent := xUnit;
  xUnit.Logic.Parent := xUnit;
  xUnit.Physic.Parent := xUnit;
  xUnit.Physic.Speed := 5;
  Units.Add(xUnit);

  CamPos[0] := Map.Width / 2;
  CamPos[1] := Map.Height / 2;
  CamPos[2] := 50;

  Control := TS_Control.Create;
  Control.Anchor := tsa_Center;
  Control.Width := Window.Width;
  Control.Height := Window.Height;
  Control.RecalcPos;

  Control.OnMouseMoveProc := OnMouseMove;
  Control.OnMouseWheelProc := OnMouseWheel;
  Control.OnMouseDownProc := OnMouseDown;
  Window.Controls.Add(Control);
end;

procedure T_Scene_Shooter.UpdateScene;
var
  i : integer;
begin
  inherited UpdateScene;
  for i := 0 to Units.Count - 1 do
    T_Unit(Units[i]).Update;
end;

procedure T_Scene_Shooter.DrawScene;
begin
  inherited DrawScene;
  PreDraw;
  SetCamera;
  Map.Draw;
  DrawUnits;
end;

procedure T_Scene_Shooter.FreeScene;
begin
  inherited FreeScene;
end;

end.

