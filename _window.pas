unit _window;

{$mode delphi}{$H+}

interface
  uses
    SysUtils, Classes, gl, glext, _textures, inifiles, _font, _usual, math, _keys, LazUTF8;
type
  TS_Anchor = (tsa_LeftTop, tsa_Top, tsa_RightTop, tsa_Left, tsa_Center, tsa_Right, tsa_LeftBottom, tsa_Bottom, tsa_RightBottom);

  pAnchor = ^TS_Anchor;

  { TS_Control }
  T_MouseButton = (mbLeft, mbMiddle, mbRight);

  { T_Shader }

  T_Shader = class
    Programm : GLuint;
    vShader : GLuint;
    fShader : GLuint;
    texSampler : GLInt;
    constructor Create(AShaderPath: AnsiString);
    procedure Use;
    procedure DeUse;
    destructor Destroy; override;
  end;

  TInternalType = (it_Integer, it_Cardinal, it_Boolean, it_String, it_Anchor);

  { TS_Internal }

  TS_Internal = class
    Name : AnsiString;
    ValueInt : PInteger;
    ValueCardinal : PCardinal;
    ValueBool : PBoolean;
    ValueString : PAnsiString;
    ValueAnchor : PAnchor;
    _type : TInternalType;
    function GetString : AnsiString;
    procedure SetValue(AValue : AnsiString);
  end;

  T_MessageType = (tmt_MouseDown, tmt_MouseUp, tmt_MouseMove, tmt_MouseWheel, tmt_UniChar);

  T_Message = record
    _type : T_MessageType;
    X,
    Y : integer;
    MouseWheel : integer;
    MButton : T_MouseButton;
    UniChar : AnsiString;
    KeyCode : Word;
  end;

  TS_Control = class
    private
    public
      X, Y : integer;
      Internals : TObjectList;
      Color : Cardinal;
      FocusColor : Cardinal;
      DoDrawFocus : Boolean;
      Texture : Cardinal;
      Anchor : TS_Anchor;
      BaseWidth,
      BaseHeight : integer;
      WidthPercentOfParent : Integer;
      HeightPercentOfParent : Integer;
      Width, Height : integer;
      BaseX : integer;
      BaseY : integer;
      ParentName : AnsiString;
      Parent : TS_Control;
      Childs : TList;
      Visible : boolean;
      Font : T_Font;
      Name : AnsiString;
      DoDrawCaption : boolean;
      Caption : AnsiString;
      isFocused : boolean;
      IsDoDraw : boolean;
      isCreatedByParent : boolean;
      OnMouseDownProc : procedure(AX, AY : integer; AButton : T_MouseButton) of object;
      OnMouseUpProc : procedure(AX, AY : integer; AButton : T_MouseButton) of object;
      OnMouseMoveProc : procedure(AX, AY : integer; AControl : TS_Control = nil) of object;
      OnMouseWheelProc : procedure(AValue : integer) of object;
      OnSetFocus : procedure of object;
      OnReleaseFocus : procedure of object;
    constructor Create; virtual;
    destructor Destroy; override;
    procedure InitInternals; virtual;
    function SaveInternalsToMem : TMemoryStream;
    procedure LoadInternalsFromMem(AStream : TMemoryStream);
    procedure RegisterInternal(AName : AnsiString; AType : TInternalType; AVal : Pointer);
    procedure AfterConstruction;override;
    procedure RecalcPos; virtual;
    procedure RecalcPosChilds; virtual;
    procedure Draw;
    procedure DoDraw; virtual;
    procedure DrawFocus; virtual;
    procedure DrawChilds; virtual;
    procedure DrawCaption; virtual;
    procedure BringToFrontChild(AControl : TS_Control);
    procedure SetFocus;
    procedure ReleaseFocus;
    procedure Hide;
    procedure Show;
    procedure SetTexture(ATextureName : string; ADoFreeData : boolean = True);
    procedure ProcessMessage(AMessage : T_Message);
    procedure OnMouseDown(AX, AY : integer; AButton : T_MouseButton); virtual;
    procedure OnMouseUp(AX, AY : integer; AButton : T_MouseButton); virtual;
    procedure OnMouseMove(AX, AY : integer); virtual;
    procedure OnMouseWheel(AValue : integer); virtual;
    procedure OnUniChar(AChar : AnsiString; AKeyCode : Word); virtual;
    procedure AddChild(AChild : TS_Control);
  end;

  TS_ControlClass = class of TS_Control;
  { TS_List }

  TS_VertScrollBar = class;

  TS_List = class(TS_Control)
    OffsetY : Single;
    Strings : TStringList;
    ItemHeight : integer;
    SelIndex : integer;
    VertScrollBar : TS_VertScrollBar;
    OnItemMouseDownProc : procedure(AItemIndex : integer) of object;
    constructor Create; override;
    destructor Destroy; override;
    procedure OnVertPosChange(ANewPos : Single);
    procedure DoDraw; override;
    procedure DrawSelected;
    procedure RecalcPos; override;
    procedure OnMouseDown(AX, AY : integer; AButton : T_MouseButton); override;
    function OnItemMouseDown(AItemIndex : integer) : boolean; virtual;
  end;

  { TS_VertScrollBar }

  TS_VertScrollBar = class(TS_Control)
    Position : Single;
    Max : Single;
    TextureUp,
    TextureMid,
    TextureDown : Cardinal;
    bUp,
    bDown,
    bMid : TS_Control;
    OnPosChangeProc : procedure(ANewPos : Single) of object;
    constructor Create; override;
    procedure RecalcPos; override;
    procedure DoDraw; override;
    procedure OnMouseMove(AX, AY : integer); override;
    procedure OnUpClick(AX, AY : integer; AButton : T_MouseButton);
    procedure OnDownClick(AX, AY : integer; AButton : T_MouseButton);
  end;

  { TS_Edit }

  TS_Edit = class(TS_Control)
    OnUniCharProc : procedure(AChar : AnsiString; AKeyCode : Word) of object;
    procedure OnUniChar(AChar : AnsiString; AKeyCode : Word); override;
    procedure DoDraw; override;
  end;

  { T_Scene }

  T_Scene = class
    dt : Single;
    ft : Single;
    LastDrawedTime : DWord;
    UPS : integer;
    NextUpdate : Single;
    MaxFrameSkips : integer;
    DoDrawOnlyAfterUpdate : boolean;
    procedure InitScene; virtual;
    procedure UpdateScene; virtual;
    procedure DrawScene; virtual;
    procedure FreeScene; virtual;
    procedure OnChar(AChar : AnsiString); virtual;
  end;

  { TS_Window }

  R_Mouse = record
    X, Y : integer;
    dx, dy : Single;
    LMB,
    MMB,
    RMB : boolean;
  end;

  TS_Window = class
  private
  protected
    FHeight: integer;
    FLeft: integer;
    FTop: integer;
    FWidth: integer;
    FCaption : string;
    procedure SetCaption(AValue : string); virtual; abstract;
    procedure SetHeight(AValue: integer); virtual;
    procedure SetLeft(AValue: integer); virtual; abstract;
    procedure SetTop(AValue: integer); virtual; abstract;
    procedure SetWidth(AValue: integer); virtual;
    function GetScreenWidth : integer; virtual; abstract;
    function GetScreenHeight : integer; virtual; abstract;
    function GetAspect : real;
  public
    MainControl : TS_Control;
    FocusedControl : TS_Control;
    Handle : Cardinal;
    GLDC : Cardinal;
    Keys : array[0..65535] of Boolean;
    OnChar : procedure(AChar : string) of object;
    Scene : T_Scene;
    DelayedScene : T_Scene;
    Terminated : boolean;
    Mouse  : R_Mouse;
    FPS : integer;
    FPSTimer : Single;
    FPSCounter : integer;
    VS,
    FS : Cardinal;
    sp : Cardinal;
    IsConsoleMode : boolean;
    Font : T_Font;
    procedure ChangeScene(AScene : T_Scene); virtual;
    procedure DelayedChangeScene(AScene : T_Scene); virtual;
    procedure ProcessEvents; virtual; abstract;
    procedure Run; virtual;
    procedure SwapBuffers; virtual; abstract;
    procedure DrawControls; virtual;
    procedure FreeControls; virtual;
    constructor Create; virtual;
    constructor CreateConsoleMode; virtual;
    procedure AfterConstruction; override;
    procedure LoadControlsFromFile(AFileName : AnsiString);
    function FindControlByName(AControlName : AnsiString) : TS_Control;
  published
    property ScreenWidth : integer read GetScreenWidth;
    property ScreenHeight : integer read GetScreenHeight;
    property Width : integer read FWidth write SetWidth;
    property Height : integer read FHeight write SetHeight;
    property Left : integer read FLeft write SetLeft;
    property Top : integer read FTop write SetTop;
    property Aspect : real read GetAspect;
    property Caption : string read FCaption write SetCaption;
  end;

  T_WindowClass = class of TS_Window;
function PointInRect(AX, AY : real; ALeft, ATop, ARight, ABottom : real) : boolean;

const
  TextureVertScrollBarUp = 'data' + PathDelim + 'controls' + PathDelim + 'ScrollBarUp.tga';
  TextureVertScrollBarMid = 'data' + PathDelim + 'controls' + PathDelim + 'ScrollBarMid.tga';
  TextureVertScrollBarDown = 'data' + PathDelim + 'controls' + PathDelim + 'ScrollBarDown.tga';
var
  Window : TS_Window;
  WindowClass : T_WindowClass;
  gl_texture_3d : integer = $806F;
var
  glTexImage3D : procedure (target: GLenum; level: GLint; internalformat: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; border: GLint; format: GLenum; _type: GLenum; const pixels: PGLvoid); {$IFDEF WINDOWS}stdcall; {$ELSE}cdecl; {$ENDIF}
implementation
  uses
    {$IFDEF WINDOWS}
    _window_win;
    {$ENDIF}
    {$IFDEF LINUX}
    _window_linux;
    {$ENDIF}

function PointInRect(AX, AY : real; ALeft, ATop, ARight, ABottom : real) : boolean;
begin
  Result := (AX >= ALeft) and (AX <= ARight) and (AY >= ATop) and (AY <= ABottom);
end;

{ TS_Internal }

function TS_Internal.GetString: AnsiString;
begin
  Case _type of
    it_Boolean:
        if ValueBool^ then
          Result := 'True'
        else
          Result := 'False';
    it_Cardinal:
        Result := IntToStr(ValueCardinal^);
    it_Integer:
        Result := IntToStr(ValueInt^);
    it_String:
        Result := ValueString^;
    it_Anchor:
        Case ValueAnchor^ of
          tsa_LeftTop:
              Result := 'tsa_LeftTop';
          tsa_Top:
              Result := 'tsa_Top';
          tsa_RightTop:
              Result := 'tsa_RightTop';
          tsa_Left:
              Result := 'tsa_Left';
          tsa_Center:
              Result := 'tsa_Center';
          tsa_Right:
              Result := 'tsa_Right';
          tsa_LeftBottom:
              Result := 'tsa_LeftBottom';
          tsa_Bottom:
              Result := 'tsa_Bottom';
          tsa_RightBottom:
              Result := 'tsa_RightBottom';
        end;
  end;
end;

procedure TS_Internal.SetValue(AValue: AnsiString);
begin
  try
  Case _type of
    it_String:
        ValueString^ := AValue;
    it_Integer:
          ValueInt^ := StrToInt(AValue);
    it_Cardinal:
        ValueCardinal^ := StrToInt64(AValue);
    it_Boolean:
        if AnsiUpperCase(AValue) = 'FALSE' then
          ValueBool^ := False
        else
          ValueBool^ := True;
    it_Anchor:
        if AValue = 'tsa_LeftTop' then
          ValueAnchor^ := tsa_LeftTop
        else
          if AValue = 'tsa_Top' then
            ValueAnchor^ := tsa_Top
          else
            if AValue = 'tsa_RightTop' then
              ValueAnchor^ := tsa_RightTop
            else
              if AValue = 'tsa_Left' then
                ValueAnchor^ := tsa_Left
              else
                if AValue = 'tsa_Center' then
                  ValueAnchor^ := tsa_Center
                else
                  if AValue = 'tsa_Right' then
                    ValueAnchor^ := tsa_Right
                  else
                    if AValue = 'tsa_LeftBottom' then
                      ValueAnchor^ := tsa_LeftBottom
                    else
                      if AValue = 'tsa_Bottom' then
                        ValueAnchor^ := tsa_Bottom
                      else
                        if AValue = 'tsa_RightBottom' then
                          ValueAnchor^ := tsa_RightBottom;
  end;

  except
  end;
end;

{ TS_Edit }

procedure TS_Edit.OnUniChar(AChar: AnsiString; AKeyCode: Word);
var
  xLen : integer;
begin
  inherited OnUniChar(AChar, AKeyCode);
  if not isFocused then
    Exit;
  try
  if (AKeyCode = MK_BACKSPACE) then
    begin
      if Length(Caption) <= 0 then
        Exit;
      xLen := UTF8Length(Caption);
      Caption := UTF8LeftStr(Caption, xLen - 1);
      Exit;
    end;
  if AKeyCode <> MK_RETURN then
    if (Length(AChar) > 0) and (UTF8CharacterLength(@AChar[1]) > 0) then
      Caption := Caption + AChar;
  finally
    if Assigned(OnUniCharProc) then
      OnUniCharProc(AChar, AKeyCode);
  end;
end;


procedure TS_Edit.DoDraw;
begin
  inherited DoDraw;
  Font.DrawCharsXY(Caption, X, Y);
end;

{ TS_VertScrollBar }

constructor TS_VertScrollBar.Create;
begin
  inherited Create;
  //OnMouseMoveProc := OnMouseMove;
  bUp := TS_Control.Create;
  bUp.BaseWidth := BaseWidth;
  bUp.BaseHeight := 10;
  bUp.Anchor := tsa_Top;
  bUp.SetTexture(TextureVertScrollBarUp);
  bUp.OnMouseDownProc := OnUpClick;
  bUp.isCreatedByParent := True;
  AddChild(bUp);

  bDown := TS_Control.Create;
  bDown.BaseWidth := BaseWidth;
  bDown.BaseHeight := 10;
  bDown.Anchor := tsa_Bottom;
  bDown.SetTexture(TextureVertScrollBarDown);
  bDown.OnMouseDownProc := OnDownClick;
  bDown.isCreatedByParent := True;
  AddChild(bDown);

  bMid := TS_Control.Create;
  bMid.BaseWidth := BaseWidth;
  bMid.BaseHeight := 10;
  bMid.Anchor := tsa_Top;
  bMid.SetTexture(TextureVertScrollBarMid);
  bMid.isCreatedByParent := True;
  AddChild(bMid);
  RecalcPos;

end;

procedure TS_VertScrollBar.RecalcPos;
var
  xHeight : integer;
begin
  Anchor := tsa_Right;
  BaseWidth := 10;
  bUp.BaseWidth := 10;
  bDown.BaseWidth := 10;
  bMid.BaseWidth := 10;
  xHeight := Height - bUp.Height - bDown.Height - bMid.Height;
  bMid.BaseY := Round(bUp.Height + (Position/Max) * xHeight);
  if Parent <> nil then
    BaseHeight := Parent.Height;
  inherited RecalcPos;
end;

procedure TS_VertScrollBar.DoDraw;
var
  xHeight : integer;
  xY : integer;
begin
  inherited DoDraw;
{  xHeight := 10;
  TextureManager.SetTexture(TextureUp);
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0);
  glVertex2f(X, Y);
  glTexCoord2f(0, 1);
  glVertex2f(X, Y + xHeight);
  glTexCoord2f(1, 1);
  glVertex2f(X + Width, Y + xHeight);
  glTexCoord2f(1, 0);
  glVertex2f(X + Width, Y);
  glEnd;

  xY := Y + xHeight + Round((Position/Max)*(Height-3*xHeight));
  TextureManager.SetTexture(TextureMid);
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0);
  glVertex2f(X, xY);
  glTexCoord2f(0, 1);
  glVertex2f(X, xY + xHeight);
  glTexCoord2f(1, 1);
  glVertex2f(X + Width, xY + xHeight);
  glTexCoord2f(1, 0);
  glVertex2f(X + Width, xY);
  glEnd;

  TextureManager.SetTexture(TextureDown);
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0);
  glVertex2f(X, Y + Height - xHeight);
  glTexCoord2f(0, 1);
  glVertex2f(X, Y + Height);
  glTexCoord2f(1, 1);
  glVertex2f(X + Width, Y + Height);
  glTexCoord2f(1, 0);
  glVertex2f(X + Width, Y + Height - xHeight);
  glEnd;
  TextureManager.UnSetTexture;
}end;

procedure TS_VertScrollBar.OnMouseMove(AX, AY: integer);
var
  xY : Single;
  xHeight : Single;
begin
  if Window.Mouse.LMB then
    begin
      AY := AY - Y;
      if (AY < bUp.Height) or (Y + AY > bDown.Y-bDown.Height) then
        Exit;
      xY := AY - bUp.Height;
      xHeight := Height - bUp.Height - bDown.Height - bMid.Height;
      Position := Max * xY/xHeight;
      if Assigned(OnPosChangeProc) then
        OnPosChangeProc(Position);
      RecalcPos;
    end;
end;


procedure TS_VertScrollBar.OnUpClick(AX, AY: integer; AButton: T_MouseButton);
begin
  Position := Math.Max(0, Position - 1);
  if Assigned(OnPosChangeProc) then
    OnPosChangeProc(Position);
  RecalcPos;
end;

procedure TS_VertScrollBar.OnDownClick(AX, AY: integer; AButton: T_MouseButton);
begin
  Position := Math.Min(Max, Position + 1);
  if Assigned(OnPosChangeProc) then
    OnPosChangeProc(Position);
  RecalcPos;
end;

{ TS_List }

constructor TS_List.Create;
begin
  inherited Create;
  Strings := TStringList.Create;
  ItemHeight := 16;
  VertScrollBar := TS_VertScrollBar.Create;
  AddChild(VertScrollBar);
  VertScrollBar.isCreatedByParent := True;
  VertScrollBar.Max := 10;
  VertScrollBar.OnPosChangeProc := OnVertPosChange;
end;

destructor TS_List.Destroy;
begin
  inherited Destroy;
end;

procedure TS_List.OnVertPosChange(ANewPos: Single);
begin
  OffsetY := ANewPos;
end;

procedure TS_List.DoDraw;
var
  i : integer;
  xY : Single;
begin
  RecalcPos;
  inherited DoDraw;
  for i := 0 to Strings.Count - 1 do
    begin
      xY := i * ItemHeight - OffsetY;
      if (xY) > Height then
        break;
      if i = SelIndex then
        DrawSelected;
    Font.DrawCharsXY(Strings[i], X, Y + xY);
    end;
end;

procedure TS_List.DrawSelected;
begin
  glColor4ubv(@FocusColor);
  glBegin(GL_LINE_LOOP);
    glVertex2f(X, -OffsetY + Y + SelIndex * ItemHeight);
    glVertex2f(X + Width, -OffsetY + Y + SelIndex * ItemHeight);
    glVertex2f(X + Width, -OffsetY + Y + (SelIndex + 1) * ItemHeight);
    glVertex2f(X, -OffsetY + Y + (SelIndex + 1) * ItemHeight);
  glEnd;
  glColor4ubv(@Color);
end;

procedure TS_List.RecalcPos;
begin
  VertScrollBar.Max := ItemHeight * Strings.Count - Height;
  inherited RecalcPos;
end;

procedure TS_List.OnMouseDown(AX, AY: integer; AButton: T_MouseButton);
var
  xItemIndex : integer;
begin
  inherited OnMouseDown(AX, AY, AButton);
  AY := Round(AY + OffsetY);
  xItemIndex := ((AY - Y) div ItemHeight);
  if xItemIndex <= Strings.Count - 1 then
    OnItemMouseDown(xItemIndex);
end;

function TS_List.OnItemMouseDown(AItemIndex: integer): boolean;
begin
  SelIndex := AItemIndex;
  if Assigned(OnItemMouseDownProc) then
    OnItemMouseDownProc(AItemIndex);
end;

{ T_Shader }

constructor T_Shader.Create(AShaderPath : AnsiString);
var
  M : TMemoryStream;
begin
  Programm := glCreateProgram;
  M := TMemoryStream.Create;

  M.LoadFromFile(AShaderPath + '.v.txt');
  M.Position := M.Size;
  M.WriteByte(0);
  vShader := glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vShader, 1, @M.Memory, nil);
  glCompileShader(vShader);
  ShaderLog('vertex:'#13#10, vShader);

  M.LoadFromFile(AShaderPath + '.f.txt');
  M.Position := M.Size;
  M.WriteByte(0);
  fShader := glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fShader, 1, @M.Memory, nil);
  glCompileShader(fShader);
  ShaderLog('fragment:'#13#10, fShader);

  glAttachShader(Programm, vShader);
  glAttachShader(Programm, fShader);

  glLinkProgram(Programm);
  glUseProgram(Programm);
  texSampler := glGetUniformLocation(Programm, 'tex');
  glUniform1i(texSampler, 0);
end;

procedure T_Shader.Use;
begin
  glUseProgram(Programm);
end;

procedure T_Shader.DeUse;
begin
  glUseProgram(0);
end;

destructor T_Shader.Destroy;
begin
  glUseProgram(0);
  glDeleteProgram(Programm);
  glDeleteShader(vShader);
  glDeleteShader(fShader);
  inherited Destroy;
end;

{ TS_Control }

constructor TS_Control.Create;
begin
  InitInternals;
  Color := $FFFFFFFF;
  FocusColor := $A0A0A0FF;
  Childs := TList.Create;
  Visible := True;
  IsDoDraw := True;
  if Window <> nil then
    Font := Window.Font;
end;

destructor TS_Control.Destroy;
var
  i : integer;
begin
  if Parent <> nil then
    Parent.Childs.Remove(Self);
  ReleaseFocus;
  for i := Childs.Count - 1 downto 0 do
    TS_Control(Childs[i]).Free;
  Childs.Free;
  inherited Destroy;
end;

procedure TS_Control.InitInternals;
begin
  Internals := TObjectList.create(True);
  RegisterInternal('Color', it_Cardinal, @Color);
  RegisterInternal('FocusColor', it_Cardinal, @FocusColor);
  RegisterInternal('DoDrawFocus', it_Boolean, @DoDrawFocus);
  RegisterInternal('Anchor', it_Anchor, @Anchor);
  RegisterInternal('BaseWidth', it_Integer, @BaseWidth);
  RegisterInternal('BaseHeight', it_Integer, @BaseHeight);
  RegisterInternal('WidthPercentOfParent', it_Integer, @WidthPercentOfParent);
  RegisterInternal('HeightPercentOfParent', it_Integer, @HeightPercentOfParent);
  RegisterInternal('BaseX', it_Integer, @BaseX);
  RegisterInternal('BaseY', it_Integer, @BaseY);
  RegisterInternal('Visible', it_Boolean, @Visible);
  RegisterInternal('Name', it_String, @Name);
  RegisterInternal('DoDrawCaption', it_Boolean, @DoDrawCaption);
  RegisterInternal('Caption', it_String, @Caption);
  RegisterInternal('IsFocused', it_Boolean, @IsFocused);
  RegisterInternal('ParentName', it_String, @ParentName);
end;

function TS_Control.SaveInternalsToMem: TMemoryStream;
var
  i : integer;
  xInternal : TS_Internal;
begin
  Result := TMemoryStream.Create;
  WritelnToStream(Result, '-----------');
  WritelnToStream(Result, IntToStr(Internals.Count));
  for i := 0 to Internals.Count - 1 do
    begin
      xInternal := TS_Internal(Internals[i]);
      WritelnToStream(Result, xInternal.Name);
      WritelnToStream(Result, xInternal.GetString);
    end;
end;

procedure TS_Control.LoadInternalsFromMem(AStream: TMemoryStream);
var
  i : integer;
  j : integer;
  xInternal : TS_Internal;
  xCount : Integer;
  xInternName : AnsiString;
  xInternValue : AnsiString;
  xControl : TS_Control;
begin
  ReadlnFromStream(AStream);
  xCount := StrToInt(ReadlnFromStream(AStream));
  //xCount := AStream.ReadWord;
  for i := 0 to xCount - 1 do
    begin
      xInternName := ReadlnFromStream(AStream);
      xInternValue := ReadlnFromStream(AStream);
      //xInternName := AStream.ReadAnsiString;
      //xInternValue := AStream.ReadAnsiString;
      for j := 0 to Internals.Count - 1 do
        begin
          xInternal := TS_Internal(Internals[j]);
          if xInternal.Name = xInternName then
            begin
              if AnsiUpperCase(xInternal.Name) = AnsiUpperCase('ParentName') then
                begin
                  xControl := Window.FindControlByName(xInternValue);
                  if xControl <> nil then
                    begin
                      if Parent <> nil then
                        Parent.Childs.Remove(Self);
                      xControl.AddChild(Self);
                    end
                  else
                    Window.MainControl.AddChild(Self);
                end;
              xInternal.SetValue(xInternValue);
              break;
            end;
        end;
    end;
end;

procedure TS_Control.RegisterInternal(AName: AnsiString; AType: TInternalType;
  AVal: Pointer);
var
  xInternal : TS_Internal;
begin
  xInternal := TS_Internal.Create;
  xInternal.Name := AName;
  case AType of
    it_Boolean:
        xInternal.ValueBool := AVal;
    it_Cardinal:
        xInternal.ValueCardinal := AVal;
    it_Integer:
        xInternal.ValueInt := AVal;
    it_String:
        xInternal.ValueString := AVal;
    it_Anchor:
        xInternal.ValueAnchor := AVal;
  end;
  xInternal._type := AType;
  Internals.Add(xInternal);
end;

procedure TS_Control.AfterConstruction;
begin
end;

procedure TS_Control.RecalcPos;
var
  xParentX,
  xParentY,
  xParentWidth,
  xParentHeight : integer;
begin
  if Window = nil then
    Exit;
  if (Parent = nil) then
    begin
      xParentX := 0;
      xParentY := 0;
      xParentWidth := Window.Width;
      xParentHeight := Window.Height;
    end
  else
    begin
      xParentX := Parent.X;
      xParentY := Parent.Y;
      xParentWidth := Parent.Width;
      xParentHeight := Parent.Height;
    end;
  Width := BaseWidth + Round(xParentWidth * WidthPercentOfParent/100);
  Height := BaseHeight + Round(xParentHeight * HeightPercentOfParent/100);
  case Anchor of
    tsa_Left, tsa_LeftTop, tsa_LeftBottom:
        X := xParentX + BaseX;
    tsa_Right, tsa_RightTop, tsa_RightBottom:
        X := xParentX + xParentWidth + BaseX - Width;
    tsa_Top, tsa_Center, tsa_Bottom:
        X := xParentX + (xParentWidth div 2) + BaseX - (Width div 2);
  end;

  case Anchor of
    tsa_LeftTop, tsa_Top, tsa_RightTop:
        Y := xParentY + BaseY;
    tsa_Left, tsa_Center, tsa_Right:
        Y := xParentY + (xParentHeight div 2) + BaseY - (Height div 2);
    tsa_LeftBottom, tsa_Bottom, tsa_RightBottom:
        Y := xParentY + xParentHeight + BaseY - Height;
  end;

  RecalcPosChilds;
end;

procedure TS_Control.RecalcPosChilds;
var
  i : integer;
begin
  for i := 0 to Childs.Count - 1 do
    TS_Control(Childs[i]).RecalcPos;
end;

procedure TS_Control.Draw;
begin
  if not Visible then
    Exit;
  glEnable(GL_SCISSOR_TEST);
  glScissor(X, Window.Height-Y-Height, Width, Height);
  if IsDoDraw then
    DoDraw;
  glDisable(GL_SCISSOR_TEST);
  DrawChilds;
end;

procedure TS_Control.DoDraw;
var
  i : integer;
begin

  glColor4ubv(@Color);
  if Texture <> 0 then
    begin
      glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, Texture);
    end;
  glBegin(GL_QUADS);
    glTexCoord2f(0, 1);
    glVertex2i(X, Y);
    glTexCoord2f(1, 1);
    glVertex2i(X + Width, Y);
    glTexCoord2f(1, 0);
    glVertex2i(X + Width, Y + Height);
    glTexCoord2f(0, 0);
    glVertex2i(X, Y + Height);
  glEnd;
  glDisable(GL_TEXTURE_2D);
  if (isFocused and DoDrawFocus) then
    DrawFocus;
  if DoDrawCaption then
    DrawCaption;
end;

procedure TS_Control.DrawFocus;
begin
  glColor4ubv(@FocusColor);
  glBegin(GL_LINE_LOOP);
    glTexCoord2f(0, 1);
    glVertex2i(X, Y);
    glTexCoord2f(1, 1);
    glVertex2i(X + Width, Y);
    glTexCoord2f(1, 0);
    glVertex2i(X + Width, Y + Height);
    glTexCoord2f(0, 0);
    glVertex2i(X, Y + Height);
  glEnd;
  glColor4f(1, 1, 1, 1);
end;

procedure TS_Control.DrawChilds;
var
  i : integer;
begin
  for i := 0 to Childs.Count - 1 do
    begin
      TS_Control(Childs[i]).Draw;
    end;
end;

procedure TS_Control.DrawCaption;
begin
  if Font <> nil then
    Font.DrawCharsXY(Caption, X + 2, Y + 2);
end;

procedure TS_Control.BringToFrontChild(AControl: TS_Control);
begin
  if Parent <> nil then
    Parent.BringToFrontChild(Self);
  Childs.Remove(AControl);
  Childs.Add(AControl);
end;

procedure TS_Control.SetFocus;
begin
  if Window.FocusedControl <> nil then
    Window.FocusedControl.ReleaseFocus;
  isFocused := True;
  Window.FocusedControl := Self;
  if Parent <> nil then
    Parent.BringToFrontChild(Self);
  if Assigned(OnSetFocus) then
    OnSetFocus;
end;

procedure TS_Control.ReleaseFocus;
begin
  isFocused := False;
  Window.FocusedControl := nil;
  if Assigned(OnReleaseFocus) then
    OnReleaseFocus;
end;

procedure TS_Control.Hide;
begin
  if isFocused then
    ReleaseFocus;
  Visible := False;
end;

procedure TS_Control.Show;
begin
  Visible := True;
end;

procedure TS_Control.SetTexture(ATextureName: string; ADoFreeData: boolean);
begin
  Texture := TextureManager.LoadTexture(ATextureName, ADoFreeData);
end;

procedure TS_Control.ProcessMessage(AMessage: T_Message);
var
  i : integer;
  xControl : TS_Control;
begin
  if (AMessage._type >= tmt_MouseDown) and (AMessage._type <= tmt_MouseWheel) then
    if not Visible then
      Exit;
  for i := Childs.Count - 1 downto 0 do
    begin
      xControl := Childs[i];
      if (AMessage._type >= tmt_MouseDown) and (AMessage._type <= tmt_MouseWheel) then
        if (xControl.Visible and PointInRect(AMessage.X, AMessage.Y, xControl.X, xControl.Y, xControl.X + xControl.Width, xControl.Y + xControl.Height)) then
          begin
            xControl.ProcessMessage(AMessage);
            Exit;
          end
        else
      else
        xControl.ProcessMessage(AMessage);
    end;
  case AMessage._type of
    tmt_MouseDown:
        OnMouseDown(AMessage.X, AMessage.Y, AMessage.MButton);
    tmt_MouseWheel:
        OnMouseWheel(AMessage.MouseWheel);
    tmt_MouseMove:
        OnMouseMove(AMessage.X, AMessage.Y);
    tmt_MouseUp:
        OnMouseUp(AMessage.X, AMessage.Y, AMessage.MButton);
    tmt_UniChar:
        OnUniChar(AMessage.UniChar, AMessage.KeyCode);
  end;
end;

procedure TS_Control.OnMouseDown(AX, AY: integer; AButton: T_MouseButton);
begin
  SetFocus;
 if Assigned(OnMouseDownProc) then
   OnMouseDownProc(AX, AY, AButton);
end;

procedure TS_Control.OnMouseUp(AX, AY: integer; AButton: T_MouseButton);
begin
 if Assigned(OnMouseUpProc) then
   OnMouseUpProc(AX, AY, AButton);
end;

procedure TS_Control.OnMouseMove(AX, AY: integer);
begin
 if Assigned(OnMouseMoveProc) then
   OnMouseMoveProc(AX, AY, Self);
end;

procedure TS_Control.OnMouseWheel(AValue: integer);
begin
 if Assigned(OnMouseWheelProc) then
   OnMouseWheelProc(AValue);
end;

procedure TS_Control.OnUniChar(AChar: AnsiString; AKeyCode: Word);
begin

end;


procedure TS_Control.AddChild(AChild: TS_Control);
begin
  Childs.Add(AChild);
  AChild.Parent := Self;
end;


{ TS_Window }

procedure TS_Window.SetHeight(AValue: integer);
begin
  FHeight := AValue;
  MainControl.BaseHeight := AValue;
  MainControl.RecalcPos;
end;

procedure TS_Window.SetWidth(AValue: integer);
begin
  FWidth := AValue;
  MainControl.BaseWidth := AValue;
  MainControl.RecalcPos;
end;

function TS_Window.GetAspect: real;
begin
  Result := FWidth / FHeight;
end;

procedure TS_Window.ChangeScene(AScene: T_Scene);
begin
  if Assigned(Scene) then
    Scene.FreeScene;
  Scene := AScene;
  if Scene = nil then
    Exit;
  Scene.InitScene;
  Scene.dt := 1 / Scene.UPS;
  Scene.NextUpdate := GetTickCount;
  OnChar := Scene.OnChar;
end;

procedure TS_Window.DelayedChangeScene(AScene: T_Scene);
begin
  DelayedScene := AScene;
end;

procedure TS_Window.Run;
var
  xFrameSkips : integer;
  xUpdated : boolean;
  xTime : DWord;
begin
  while not terminated do
    begin
      Sleep(1);
      if not IsConsoleMode then
        ProcessEvents;

      xFrameSkips := 0;
      xUpdated := False;

      if DelayedScene <> nil then
        begin
          ChangeScene(DelayedScene);
          DelayedScene := nil;
        end;

      if Assigned(Scene) then
        begin
          while (GetTickCount > Scene.NextUpdate) and (xFrameSkips < Scene.MaxFrameSkips) do
            begin
              inc(xFrameSkips);
              Scene.NextUpdate := Scene.NextUpdate + 1000 * Scene.dt;
              Scene.UpdateScene;
              xUpdated := True;
            end;
        end;

      if not IsConsoleMode then
        if Assigned(Scene) then
          if (not Scene.DoDrawOnlyAfterUpdate or xUpdated) then
            begin
              //writeln(Gettickcount);
              Scene.ft := (GetTickCount - Scene.LastDrawedTime) / 1000;
              FPSTimer := FPSTimer + Scene.ft;
              if FPSTimer > 1 then
                begin
                  FPSTimer := 0;
                  FPS := FPSCounter;
                  FPSCounter := 0;
                  writeln('FPS = ', FPS);
                end;
              //writeln(FPSCounter);
              inc(FPSCounter);
              Scene.LastDrawedTime := GetTickCount;
              Scene.DrawScene;
              glFinish;
              SwapBuffers;
            end;

    end;
  if Assigned(Scene) then
    Scene.FreeScene;
end;

procedure TS_Window.DrawControls;
var
  i : integer;
begin
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  glOrtho(0, Width, Height, 0, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  MainControl.Draw;

end;

procedure TS_Window.FreeControls;
begin
  MainControl.Free;
end;




constructor TS_Window.Create;
begin
  TextureManager := TS_TextureManager.Create;
  MainControl := TS_Control.Create;
  MainControl.Anchor := tsa_Center;
  MainControl.IsDoDraw := False;
end;

constructor TS_Window.CreateConsoleMode;
begin
  IsConsoleMode := True;
end;

procedure TS_Window.AfterConstruction;
var
  xVertexShader : PChar;
  xFragmentShader : PChar;
begin
  inherited AfterConstruction;
  if IsConsoleMode = False then
    begin
      Font := T_Font.Create;
      Font.LoadFromFile('font1');
    end;
  exit;Load_GL_version_2_0; exit;
  vs := glCreateShader(GL_VERTEX_SHADER);
  fs := glCreateShader(GL_FRAGMENT_SHADER);
  xVertexShader :=
  'varying vec3 Texcoord; '#13#10 +
  'varying vec4 diffuse,ambient;'#13#10 +
  'varying vec3 normal,lightDir,halfVector;'#13#10 +
  'void main()'#13#10 +
    '{'#13#10 +
    'Texcoord = gl_MultiTexCoord0.xyz;'#13#10 +
    'gl_FrontColor = gl_Color;'#13#10 +
    'gl_Position = ftransform();'#13#10 +


      'normal = gl_NormalMatrix * gl_Normal;'#13#10 +
      'normal = normal;'#13#10 +
//    'normal = normalize(gl_NormalMatrix * gl_Normal);'#13#10 +
    'lightDir = normalize(vec3(gl_LightSource[1].position));'#13#10 +
    'halfVector = normalize(gl_LightSource[1].halfVector.xyz);'#13#10 +
    'diffuse = gl_FrontMaterial.diffuse * gl_LightSource[1].diffuse;'#13#10 +
    'ambient = gl_FrontMaterial.ambient * gl_LightSource[1].ambient;'#13#10 +
    'ambient += gl_LightModel.ambient * gl_FrontMaterial.ambient;'#13#10 +

    '}'#0;
  xFragmentShader :=
  'uniform sampler3D tex0;'#13#10 +
  'varying vec3 Texcoord;'#13#10 +
  'varying vec4 diffuse,ambient;'#13#10 +
  'varying vec3 normal,lightDir,halfVector;'#13#10 +
  'void main()'#13#10 +
  '{'#13#10 +

  'vec3 n,halfV;'#13#10 +
  'float NdotL,NdotHV;'#13#10 +
  'vec4 color = ambient;'#13#10 +

  'n = normalize(normal);'#13#10 +
  'NdotL = max(dot(n,lightDir),0.0);'#13#10 +
  'if (NdotL > 0.0) {'#13#10 +
  '    color += diffuse * NdotL;'#13#10 +
  '    halfV = normalize(halfVector);'#13#10 +
  '    NdotHV = max(dot(n,halfV),0.0);'#13#10 +
  '    color += gl_FrontMaterial.specular *'#13#10 +
  '            gl_LightSource[0].specular *'#13#10 +
  '            pow(NdotHV, gl_FrontMaterial.shininess);'#13#10 +
  '}'#13#10 +
//  'gl_FragColor = texture3D(tex0, Texcoord) * color;'#13#10 +
  'gl_FragColor = gl_Color * color * texture3D(tex0, Texcoord);'#13#10 +
  'gl_FragColor[3] = gl_Color[3];'#13#10 +
  //'gl_FragColor = texture3D(tex0, Texcoord); //gl_FragColor = vec4(0.4,0.4,0.8,1.0);'#13#10 +
  //'gl_FragColor[1] = gl_FragColor[1] * 2;'#13#10 +
  '}'#0;
  try
  glShaderSource(vs, 1, @xVertexShader, nil);
  except
    on E:Exception do
      writeln(E.ClassName + ':' + E.Message);
  end;
  glShaderSource(fs, 1, @xFragmentShader, nil);
  glCompileShader(vs);
  glCompileShader(fs);
  sp := glCreateProgram;
  glAttachShader(sp, vs);
  glAttachShader(sp ,fs);
  glLinkProgram(sp);
  glUseProgram(sp);
  writeln(xVertexShader);
end;

procedure TS_Window.LoadControlsFromFile(AFileName: AnsiString);
var
  i : integer;
  F : TMemoryStream;
  xClassName : AnsiString;
  xControl : TS_Control;
  xParent  : TS_Control;
begin
  F := TMemoryStream.Create;
  F.LoadFromFile(AFileName);
  F.Position := 0;
  while true do
    begin
      if F.Position = F.Size then
        break;
      xClassName := ReadlnFromStream(F);
      if AnsiUpperCase(xClassName) = 'TS_CONTROL' then
        xControl := TS_Control.Create;
      if AnsiUpperCase(xClassName) = 'TS_LIST' then
        xControl := TS_List.Create;
      if AnsiUpperCase(xClassName) = 'TS_EDIT' then
        xControl := TS_Edit.Create;
      if AnsiUpperCase(xClassName) = 'TS_VERTSCROLLBAR' then
        xControl := TS_VertScrollBar.Create;
      xControl.LoadInternalsFromMem(F);
      {if xControl.ParentName <> '' then
        xParent := FindControlByName(xControl.ParentName)
      else
        xParent := Window.MainControl;
      xParent.AddChild(xControl);}
    end;
  F.Free;
end;

function TS_Window.FindControlByName(AControlName: AnsiString): TS_Control;
  function _FindControlByName(AControl : TS_Control) : TS_Control;
  var
    i : integer;
    xControl : TS_Control;
  begin
    Result := nil;
    if AControl.Name = AControlName then
      begin
      Result := AControl;
      Exit;
      end;
    for i := 0 to AControl.Childs.Count - 1 do
      begin
        xControl := TS_Control(AControl.Childs[i]);
        Result := _FindControlByName(xControl);
        if Result <> nil then
          Exit;
      end;
  end;

begin
  Result := _FindControlByName(MainControl);
end;

{ TS_Window }

{ T_Scene }

procedure T_Scene.InitScene;
begin
  UPS := 50;
  MaxFrameSkips := 10;
  DoDrawOnlyAfterUpdate := True;
end;

procedure T_Scene.UpdateScene;
begin

end;

procedure T_Scene.DrawScene;
begin

end;

procedure T_Scene.FreeScene;
begin

end;

procedure T_Scene.OnChar(AChar: AnsiString);
begin

end;

initialization
  {$IFDEF WINDOWS}
  WindowClass := TS_Window_Win;
  {$ENDIF}
  {$IFDEF LINUX}
  WindowClass := TS_Window_Linux;
  {$ENDIF}
end.

